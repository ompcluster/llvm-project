//===---------------- scheduler.cpp - Target-Task scheduler ---------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Scheduler used to manage multiple devices of the same type.
//
//===----------------------------------------------------------------------===//

#include "scheduler.h"
#include "device.h"
#include "private.h"
#include "profiler.h"
#include "rtl.h"

#include "algorithms/basescheduler.h"
#include "algorithms/graph_roundrobin.h"
#include "algorithms/heft.h"
#include "algorithms/roundrobin.h"
#include "ft.h"

// Strings allowed to be passed in the OMPCLUSTER_SCHEDULER environment
// variable.
// NOTE: Update this when adding a new scheduler.
static const char *allowed_schedulers[] = {
    "roundrobin",
    "graph_roundrobin",
    "heft",
};

// Scheduler global state
static BaseScheduler *scheduler;
static bool is_using_heft = true;

// The default scheduler used by the MPI plugin
using DefaultScheduler = HEFTScheduler;

// Scheduler implementation
// =============================================================================

static inline void WarnUnknownScheduler(const std::string &str) {
  std::string message;
  for (const auto &name : allowed_schedulers) {
    message += " ";
    message += name;
  }

  DP("[SCHEDULER] Invalid scheduler OMPCLUSTER_SCHEDULER=%s\n", str.c_str());
  DP("[SCHEDULER] Allowed values are:%s\n", message.c_str());
}

/// Initialize the scheduler.
///
/// \note callers are encoraged to check the scheduler pointer before calling
/// this function, thus avoiding the need to acquire a lock.
void InitScheduler() {
  // Only one thread can try to init the scheduler at a time.
  static std::mutex init_mtx;
  std::unique_lock<std::mutex> init_lk(init_mtx);

  if (scheduler != nullptr) {
    return;
  }

  const char *str = std::getenv("OMPCLUSTER_SCHEDULER");
  std::string sched_str = (str != nullptr) ? str : "";

  if (sched_str == "roundrobin") {
    DP("[SCHEDULER] Creating dynamic round-robin scheduler\n");
    scheduler = new RoundRobinScheduler();
    is_using_heft = false;
  } else if (sched_str == "graph_roundrobin") {
    DP("[SCHEDULER] Creating static round-robin scheduler\n");
    scheduler = new GraphRoundRobinScheduler();
    is_using_heft = false;
  } else if (sched_str == "heft") {
    DP("[SCHEDULER] Creating HEFT scheduler\n");
    scheduler = new HEFTScheduler();
    is_using_heft = true;
  } else {
    if (sched_str != "") {
      WarnUnknownScheduler(sched_str);
    }
    DP("[SCHEDULER] Creating default scheduler\n");
    scheduler = new DefaultScheduler();
  }
}

void DeinitScheduler() {
  if (scheduler != nullptr) {
    DP("[SCHEDULER] Deinit scheduler\n");
    delete scheduler;
  }
}

// Return the list of successor ranks of a given data task if the HEFT scheduler
// is enabled
std::set<int> getDataTaskSuccessors(int32_t task_id) {
  if (is_using_heft) {
    auto heft_scheduler = static_cast<HEFTScheduler *>(scheduler);

    auto succ_set = heft_scheduler->dataTaskSuccessorRanks[task_id];

    return succ_set;
  }

  std::set<int> emtpy_set = {};
  return emtpy_set;
}

int Schedule(ident_t *Loc, int DeviceId) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  PROF_BEGIN(PROF_LVL_ALL, "Before Locking");
  // Do not schedule non-mpi-target devices.
  const RTLInfoTy *DeviceRTL = PM->Devices[DeviceId]->RTL;
  if (!DeviceRTL->IsMPI || DeviceRTL->DisableDataManager) {
    return DeviceId;
  }

  DP("[SCHEDULER] Asked to schedule for %s[%d]\n", DeviceRTL->RTLName.c_str(),
     DeviceId);

  // Create the scheduler if it does not exist yet.
  if (scheduler == nullptr) {
    InitScheduler();
  }

  const int32_t TaskId = ft_handler.TaskGetCurrentTargetID();
  static const bool MustWait = [] {
    const char *str = std::getenv("OMPCLUSTER_BLOCKING_SCHEDULER");
    if (str) {
      return (bool)atoi(str);
    }

    return false;
  }();
  PROF_END(PROF_LVL_ALL);

  PROF_BEGIN(PROF_LVL_USER, "Schedule Task", [&]() -> std::string {
    std::stringstream details;
    details << "{";
    details << "\"task_id\":" << TaskId;
    details << "}";
    return details.str();
  }());

  // Acquire the scheduled device id.
  int ScheduledDevId = -1;
  int OldExecCount = -1;
  std::atomic<int> *ExecCount = nullptr;
  do {
    BaseScheduler::LockGuard SchLk(scheduler);
    ScheduledDevId = scheduler->scheduleTask(TaskId, DeviceRTL);
    ScheduledDevId = ft_handler.DevicesTranslate(ScheduledDevId);
    ExecCount = &PM->Devices[ScheduledDevId]->ExecCount;

    // Set OldExecCount to its expected value for a successfull scheduling.
    OldExecCount = 0;
  } while (MustWait && !ExecCount->compare_exchange_weak(OldExecCount, 1));
  PROF_END(PROF_LVL_USER);

  PROF_BEGIN(PROF_LVL_ALL, "After Locking");
  // Update exec count when scheduler is configured to not be blocking.
  if (!MustWait) {
    (*ExecCount)++;
  }
  assert(*ExecCount > 0);

  if (TaskId != -1) {
    BaseScheduler::LockGuard SchLk(scheduler);
    scheduler->setTaskState(TaskId, TaskState::DISPATCHED);
    scheduler->setTaskSourceLocation(TaskId, Loc);
  }

  DP("[SCHEDULER] Using %s[%d]!\n", DeviceRTL->RTLName.c_str(), ScheduledDevId);
  PROF_END(PROF_LVL_ALL);
  if (is_using_heft) {
    scheduler->dumpTaskGraph();
  }
  return ScheduledDevId;
}

void PinTask(ident_t *Loc, int DeviceId) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);

  // Create the scheduler if it does not exist yet.
  if (scheduler == nullptr) {
    InitScheduler();
  }

  const int32_t TaskId = ft_handler.TaskGetCurrentTargetID();
  if (TaskId != -1) {
    BaseScheduler::LockGuard SchLk(scheduler);
    scheduler->setTaskState(TaskId, TaskState::DISPATCHED);
    scheduler->setTaskSourceLocation(TaskId, Loc);
    scheduler->pinTask(TaskId, DeviceId);
    DP("[SCHEDULER] Pinning task %d at device %d!\n", TaskId, DeviceId);
  }
}

void MarkExecutedTask(int DeviceId) {
  // Non-mpi-target devices were not scheduled.
  DeviceTy &Device = *PM->Devices[DeviceId];
  if (!Device.RTL->IsMPI || Device.RTL->DisableDataManager) {
    return;
  }

  // Update counter of in flight regions.
  Device.ExecCount--;

  const int32_t TaskId = ft_handler.TaskGetCurrentTargetID();
  if (TaskId != -1) {
    // We must have the scheduler initialized up to this point.
    assert(scheduler != nullptr);
    BaseScheduler::LockGuard SchLk(scheduler);
    scheduler->setTaskState(TaskId, DeviceId, TaskState::EXECUTED);
  }
}

void MarkFailedTask(int32_t task_id, const DeviceTy &Device) {
  // Non-mpi-target devices were not scheduled.
  if (!Device.RTL->IsMPI || Device.RTL->DisableDataManager) {
    return;
  }

  // Since we also mark implicitly tasks, this task_id follows the custom_id gen
  // of FT handling
  BaseScheduler::LockGuard SchLk(scheduler);
  scheduler->setTaskState(task_id, TaskState::FAILED);
}

void MarkExecutedPinnedTask(const DeviceTy &Device) {
  // Non-mpi-target devices were not scheduled.
  if (!Device.RTL->IsMPI || Device.RTL->DisableDataManager) {
    return;
  }

  const int32_t TaskId = ft_handler.TaskGetCurrentTargetID();
  if (TaskId != -1) {
    // We must have the scheduler initialized up to this point.
    assert(scheduler != nullptr);
    BaseScheduler::LockGuard SchLk(scheduler);
    scheduler->setTaskState(TaskId, TaskState::EXECUTED);
  }
}

bool HasOutDep(int32_t task_id, int64_t addr) {
  return scheduler->getDep(task_id, addr).dep_type.out;
}

int32_t GetCurrentTargetTaskID() {
  // Init the scheduler for graph data query.
  if (scheduler == nullptr)
    InitScheduler();

  return ft_handler.TaskGetCurrentTargetID();
}

void reScheduleGraph(RTLInfoTy *RTL) {
  if (RTL->IsMPI && !RTL->DisableDataManager)
    scheduler->reScheduleGraph(RTL);
}

const void *GetCurrentOutlinedFnID() {
  if (scheduler == nullptr)
    return nullptr;

  BaseScheduler::LockGuard SchLk(scheduler);

  const int CurrTaskID = __kmpc_target_task_current_task_id();
  TaskScheduleData ScheduleData = scheduler->getTask(CurrTaskID);

  if (!ScheduleData.task)
    return nullptr;

  return ScheduleData.task->target_data.outlined_fn_id;
}
