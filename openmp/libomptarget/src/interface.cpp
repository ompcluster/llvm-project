//===-------- interface.cpp - Target independent OpenMP target RTL --------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Implementation of the interface to be used by Clang during the codegen of a
// target region.
//
//===----------------------------------------------------------------------===//

#include "algorithms/basescheduler.h"
#include "device.h"
#include "ft.h"
#include "omptarget.h"
#include "private.h"
#include "rtl.h"
#include "scheduler.h"

#include <atomic>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <mutex>
#include <set>

////////////////////////////////////////////////////////////////////////////////
/// adds requires flags
EXTERN void __tgt_register_requires(int64_t Flags) {
  TIMESCOPE();
  PM->RTLs.registerRequires(Flags);
}

////////////////////////////////////////////////////////////////////////////////
/// adds a target shared library to the target execution image
EXTERN void __tgt_register_lib(__tgt_bin_desc *Desc) {
  TIMESCOPE();
  std::call_once(PM->RTLs.InitFlag, &RTLsTy::loadRTLs, &PM->RTLs);
  for (auto &RTL : PM->RTLs.AllRTLs) {
    if (RTL.register_lib) {
      if ((*RTL.register_lib)(Desc) != OFFLOAD_SUCCESS) {
        DP("Could not register library with %s", RTL.RTLName.c_str());
      }
    }
  }
  PM->RTLs.registerLib(Desc);

  for (auto &R : PM->RTLs.StartableRTLs) {
    R.start_devices(Desc);
  }
}

////////////////////////////////////////////////////////////////////////////////
/// Initialize all available devices without registering any image
EXTERN void __tgt_init_all_rtls() { PM->RTLs.initAllRTLs(); }

////////////////////////////////////////////////////////////////////////////////
/// unloads a target shared library
EXTERN void __tgt_unregister_lib(__tgt_bin_desc *Desc) {
  TIMESCOPE();
  PM->RTLs.unregisterLib(Desc);
  for (auto &RTL : PM->RTLs.UsedRTLs) {
    if (RTL->unregister_lib) {
      if ((*RTL->unregister_lib)(Desc) != OFFLOAD_SUCCESS) {
        DP("Could not register library with %s", RTL->RTLName.c_str());
      }
    }
  }
}

static int TargetDataBeginBcast(ident_t *Loc, DeviceTy &Device,
                                int64_t DeviceId, int32_t ArgNum,
                                void **ArgsBase, void **Args, int64_t *ArgSizes,
                                int64_t *ArgTypes, map_var_info_t *ArgNames,
                                void **ArgMappers, std::set<int> SuccSet,
                                AsyncInfoTy &AsyncInfo) {
  // Execute it for each MPI device initialized
  int Rc = OFFLOAD_SUCCESS;

  // Add memory addresses that are going to be offloaded to the list of
  // buffers that should be broadcasted to all MPI devices
  Device.register_bcast_ptr(ArgNum, ArgsBase, Args, ArgSizes, ArgTypes,
                            true /*AddToList*/);

  // Build auxiliary array of active devices
  std::vector<int> ActiveDevicesVec;
  ActiveDevicesVec.reserve(ft_handler.DevicesGetActiveNumber());
  const int32_t LastActiveDevice =
      Device.RTL->Idx + ft_handler.DevicesGetActiveNumber();
  for (int i = Device.RTL->Idx; i < LastActiveDevice; i++) {
    ActiveDevicesVec.push_back(i);
  }

  // TODO: currently SuccSet contains the corresponding ranks. Rewrite it to
  // hold devices ID should simplify the section bellow
  for (auto i : SuccSet) {
    assert(i - 1 < ActiveDevicesVec.size());

    // Get device ID from rank number
    int64_t BcastDeviceId =
        ft_handler.DevicesTranslate(ActiveDevicesVec[i - 1]) + Device.RTL->Idx;

    // Initiate all MPI devices.
    if (checkDeviceAndCtors(BcastDeviceId, Loc)) {
      DP("Failed to get device %" PRId64 " ready for broadcast\n", DeviceId);
      return OFFLOAD_FAIL;
    }

    // Parameters for the memory management system.
    // Always register the first memory entry as a proxy (at device 0). For
    // all the other devices, register them as links to the first device.
    DataManagementParam DmParam;
    DmParam.FromDataMap = (BcastDeviceId == Device.RTL->Idx);
    DmParam.UseBcast = true;

    // Send the data to device[BcastDeviceId] using bcast.
    Rc |= targetDataBegin(Loc, *PM->Devices[BcastDeviceId], ArgNum, ArgsBase,
                          Args, ArgSizes, ArgTypes, ArgNames, ArgMappers,
                          DmParam, AsyncInfo);
  }

  // After broadcast, remove item from the list
  Device.register_bcast_ptr(ArgNum, ArgsBase, Args, ArgSizes, ArgTypes,
                            false /*AddToList*/);

  return Rc;
}

/// On MPI, data update/exit clauses always chooses the proxy device. This
/// ensures that the data will be correctly retrieved from the most up-to-date
/// device and that its copies will be remove from the whole cluster.
static DeviceTy &getDataDevice(ident_t *Loc, int64_t &DeviceId) {
  if (PM->Devices[DeviceId]->RTL->IsMPI &&
      !PM->Devices[DeviceId]->RTL->DisableDataManager) {
    DeviceTy &ProxyDevice = *PM->Devices[DeviceId]->ProxyDevice;
    DeviceId = ProxyDevice.DeviceID;
    PinTask(Loc, DeviceId);
    return ProxyDevice;
  }

  return *PM->Devices[DeviceId];
}

/// creates host-to-target data mapping, stores it in the
/// libomptarget.so internal structure (an entry in a stack of data maps)
/// and passes the data to the device.
EXTERN void __tgt_target_data_begin_mapper(ident_t *Loc, int64_t DeviceId,
                                           int32_t ArgNum, void **ArgsBase,
                                           void **Args, int64_t *ArgSizes,
                                           int64_t *ArgTypes,
                                           map_var_info_t *ArgNames,
                                           void **ArgMappers) {
  PROF_SCOPED_WITH_IDENT(PROF_LVL_USER, __FUNCTION__, Loc);
  TIMESCOPE_WITH_IDENT(Loc);
  DP("Entering data begin region for device %" PRId64 " with %d mappings\n",
     DeviceId, ArgNum);

  // If DeviceId equals to RESTART_DEVICE, it means that this is a tasks being
  // restarted
  const bool Restarting = (DeviceId == RESTART_DEVICE) && ft_handler.isActive();
  // Redefine DeviceId in case of restart
  DeviceId = (DeviceId == RESTART_DEVICE) ? OFFLOAD_DEVICE_DEFAULT : DeviceId;

  // FT Handling
  int32_t CustomId;
  if (ft_handler.isActive()) {
    if (!Restarting) {
      CustomId = ft_handler.TaskRegisterArgs(
          InterfaceTaskFunction::itf_tgt_target_data_begin_mapper, Loc,
          DeviceId, nullptr, ArgNum, ArgsBase, Args, ArgSizes, ArgTypes,
          ArgNames, ArgMappers, -1, -1, {});
      // Check if there is any FT procedure pending
      while (ft_handler.RestartCheck() || ft_handler.CPCheck())
        ;
    } else {
      CustomId = ft_handler.TaskGetCurrentTargetID();
    }
  }

  if (checkDeviceAndCtors(DeviceId, Loc)) {
    DP("Not offloading to device %" PRId64 "\n", DeviceId);
    return;
  }

  // TODO: Only schedule MPI devices. Remove when device groups are added.
  if (PM->Devices[DeviceId]->RTL->IsMPI &&
      !PM->Devices[DeviceId]->RTL->DisableDataManager) {
    DeviceId = Schedule(Loc, DeviceId);

    if (checkDeviceAndCtors(DeviceId, Loc)) {
      DP("Not offloading to device %" PRId64 "\n", DeviceId);
      return;
    }
  }

  DeviceTy &Device = *PM->Devices[DeviceId];

  if (ft_handler.isActive()) {
    ft_handler.addDeviceTaskMapEntry(CustomId, Device.DeviceID);
    DP("[FT-Handler] - Executing task %d (tgt data begin)\n", CustomId);
  }

  if (getInfoLevel() & OMP_INFOTYPE_KERNEL_ARGS)
    printKernelArguments(Loc, DeviceId, ArgNum, ArgSizes, ArgTypes, ArgNames,
                         "Entering OpenMP data region");
#ifdef OMPTARGET_DEBUG
  for (int I = 0; I < ArgNum; ++I) {
    DP("Entry %2d: Base=" DPxMOD ", Begin=" DPxMOD ", Size=%" PRId64
       ", Type=0x%" PRIx64 ", Name=%s\n",
       I, DPxPTR(ArgsBase[I]), DPxPTR(Args[I]), ArgSizes[I], ArgTypes[I],
       (ArgNames) ? getNameFromMapping(ArgNames[I]).c_str() : "unknown");
  }
#endif

  // Check if asynchronous data maps should be handled as a broadcast operation
  // to all available devices of the same type as pointed by `DeviceId`. For
  // more information check mpi_manager.cpp from the MPI plugin.
  std::pair<bool, bool> BcastMode = [] {
    const char *EnvStr = getenv("OMPCLUSTER_BCAST_STRATEGY");
    std::string Strategy = EnvStr != nullptr ? EnvStr : "";

    return std::make_pair((Strategy == "p2p" || Strategy == "mpibcast" ||
                           Strategy == "dynamicbcast"),
                          Strategy == "dynamicbcast");
  }();

  static const bool DynBcastEnabled = BcastMode.second;
  static const bool BcastEnabled = BcastMode.first;

  // Only use the broadcast policy when we have a synchronous data map.
  AsyncInfoTy AsyncInfo(Device);
  int Rc = OFFLOAD_FAIL;

  // Performs a broadcast or a sub-broadcast if the data is going to be sent to
  // more than 'BcastMinProcs' ranks
  static const int BcastMinProcs = [] {
    int BcastMinProcs = 3;
    if (const char *EnvStr = std::getenv("OMPCLUSTER_BCAST_MIN_PROC_NUM")) {
      BcastMinProcs = std::stoi(EnvStr);
    }

    assertm(BcastMinProcs >= 1,
            "Minimum number of procs to broadcast must be at least 1");
    return BcastMinProcs;
  }();

  const std::set<int> SuccSet = getDataTaskSuccessors(GetCurrentTargetTaskID());

  // Only detect partial bcasts if DynBcast is enabled. Otherwise only detects
  // if the data is to be moved to every rank
  bool DetectedBcast =
      DynBcastEnabled ? SuccSet.size() >= BcastMinProcs
                      : SuccSet.size() >= ft_handler.DevicesGetActiveNumber();

  // Use broadcast rotine on asyncrhonus data maps only. Only works in
  // combination with the HEFT scheduler
  // TODO : fix TargetDataBeginBcast and remove the false part of the boolean
  const bool UseBcast = false && BcastEnabled && Device.RTL->IsMPI &&
                        !Device.RTL->DisableDataManager &&
                        GetCurrentTargetTaskID() != -1 && DetectedBcast;
  if (UseBcast) {
    Rc = TargetDataBeginBcast(Loc, Device, DeviceId, ArgNum, ArgsBase, Args,
                              ArgSizes, ArgTypes, ArgNames, ArgMappers, SuccSet,
                              AsyncInfo);
  } else {
    // Parameters for the memory management system.
    DataManagementParam DmParam;
    DmParam.FromDataMap = true;

    Rc = targetDataBegin(Loc, Device, ArgNum, ArgsBase, Args, ArgSizes,
                         ArgTypes, ArgNames, ArgMappers, DmParam, AsyncInfo);
  }

  if (Rc == OFFLOAD_SUCCESS)
    Rc = AsyncInfo.synchronize();

  // Avoid handling outcome if Rc is OFFLOAD_EVENT_FAILED
  if (Rc == OFFLOAD_EVENT_FAILED && ft_handler.isActive())
    handleTargetOutcome(Rc == OFFLOAD_EVENT_FAILED, Loc);
  else
    handleTargetOutcome(Rc == OFFLOAD_SUCCESS, Loc);

  if (ft_handler.isActive())
    ft_handler.TaskFinish(CustomId);

  if (Rc == OFFLOAD_SUCCESS)
    MarkExecutedTask(DeviceId);

  if (Rc != OFFLOAD_SUCCESS && ft_handler.isActive())
    MarkFailedTask(CustomId, Device);
}

EXTERN void __tgt_target_data_begin_nowait_mapper(
    ident_t *Loc, int64_t DeviceId, int32_t ArgNum, void **ArgsBase,
    void **Args, int64_t *ArgSizes, int64_t *ArgTypes, map_var_info_t *ArgNames,
    void **ArgMappers, int32_t DepNum, void *DepList, int32_t NoAliasDepNum,
    void *NoAliasDepList) {
  PROF_SCOPED_WITH_IDENT(PROF_LVL_USER, __FUNCTION__, Loc);
  TIMESCOPE_WITH_IDENT(Loc);

  __tgt_target_data_begin_mapper(Loc, DeviceId, ArgNum, ArgsBase, Args,
                                 ArgSizes, ArgTypes, ArgNames, ArgMappers);
}

/// passes data from the target, releases target memory and destroys
/// the host-target mapping (top entry from the stack of data maps)
/// created by the last __tgt_target_data_begin.
EXTERN void __tgt_target_data_end_mapper(ident_t *Loc, int64_t DeviceId,
                                         int32_t ArgNum, void **ArgsBase,
                                         void **Args, int64_t *ArgSizes,
                                         int64_t *ArgTypes,
                                         map_var_info_t *ArgNames,
                                         void **ArgMappers) {
  PROF_SCOPED_WITH_IDENT(PROF_LVL_USER, __FUNCTION__, Loc);
  TIMESCOPE_WITH_IDENT(Loc);
  DP("Entering data end region with %d mappings\n", ArgNum);

  // If DeviceId equals to RESTART_DEVICE, it means that this is a tasks being
  // restarted
  const bool Restarting = (DeviceId == RESTART_DEVICE) && ft_handler.isActive();
  // Redefine DeviceId in case of restart
  DeviceId = (DeviceId == RESTART_DEVICE) ? OFFLOAD_DEVICE_DEFAULT : DeviceId;

  // FT Handling
  int32_t CustomId;
  if (ft_handler.isActive()) {
    if (!Restarting) {
      CustomId = ft_handler.TaskRegisterArgs(
          InterfaceTaskFunction::itf_tgt_target_data_end_mapper, Loc, DeviceId,
          nullptr, ArgNum, ArgsBase, Args, ArgSizes, ArgTypes, ArgNames,
          ArgMappers, -1, -1, {});
      // Check if there is any FT procedure pending
      while (ft_handler.RestartCheck() || ft_handler.CPCheck())
        ;
    } else {
      CustomId = ft_handler.TaskGetCurrentTargetID();
    }
  }

  if (checkDeviceAndCtors(DeviceId, Loc)) {
    DP("Not offloading to device %" PRId64 "\n", DeviceId);
    return;
  }

  PM->RTLsMtx.lock();
  size_t DevicesSize = PM->Devices.size();
  PM->RTLsMtx.unlock();
  if (DevicesSize <= (size_t)DeviceId) {
    DP("Device ID  %" PRId64 " does not have a matching RTL.\n", DeviceId);
    handleTargetOutcome(false, Loc);
    return;
  }

  // On MPI, data exit clauses always chooses the proxy device (first
  // available). This ensures that the data will be correctly retrieved from
  // the most up-to-date device and that its copies will be remove from the
  // whole cluster.
  // TODO: Remove this when device groups are implemented.
  DeviceTy &Device = getDataDevice(Loc, DeviceId);

  if (ft_handler.isActive()) {
    ft_handler.addDeviceTaskMapEntry(CustomId, Device.DeviceID);
    DP("[FT-Handler] - Executing task %d (tgt data end)\n", CustomId);
  }

  if (getInfoLevel() & OMP_INFOTYPE_KERNEL_ARGS)
    printKernelArguments(Loc, DeviceId, ArgNum, ArgSizes, ArgTypes, ArgNames,
                         "Exiting OpenMP data region");
#ifdef OMPTARGET_DEBUG
  for (int I = 0; I < ArgNum; ++I) {
    DP("Entry %2d: Base=" DPxMOD ", Begin=" DPxMOD ", Size=%" PRId64
       ", Type=0x%" PRIx64 ", Name=%s\n",
       I, DPxPTR(ArgsBase[I]), DPxPTR(Args[I]), ArgSizes[I], ArgTypes[I],
       (ArgNames) ? getNameFromMapping(ArgNames[I]).c_str() : "unknown");
  }
#endif

  // Parameters for the memory management system.
  DataManagementParam DmParam;
  AsyncInfoTy AsyncInfo(Device);
  int Rc = targetDataEnd(Loc, Device, ArgNum, ArgsBase, Args, ArgSizes,
                         ArgTypes, ArgNames, ArgMappers, DmParam, AsyncInfo);

  if (Rc == OFFLOAD_SUCCESS)
    Rc = AsyncInfo.synchronize();

  // Avoid handling outcome if Rc is OFFLOAD_EVENT_FAILED
  if (Rc == OFFLOAD_EVENT_FAILED && ft_handler.isActive())
    handleTargetOutcome(Rc == OFFLOAD_EVENT_FAILED, Loc);
  else
    handleTargetOutcome(Rc == OFFLOAD_SUCCESS, Loc);

  if (ft_handler.isActive())
    ft_handler.TaskFinish(CustomId);

  if (Rc == OFFLOAD_SUCCESS)
    MarkExecutedPinnedTask(Device);

  if (Rc != OFFLOAD_SUCCESS && ft_handler.isActive())
    MarkFailedTask(CustomId, Device);
}

EXTERN void __tgt_target_data_end_nowait_mapper(
    ident_t *Loc, int64_t DeviceId, int32_t ArgNum, void **ArgsBase,
    void **Args, int64_t *ArgSizes, int64_t *ArgTypes, map_var_info_t *ArgNames,
    void **ArgMappers, int32_t DepNum, void *DepList, int32_t NoAliasDepNum,
    void *NoAliasDepList) {
  PROF_SCOPED_WITH_IDENT(PROF_LVL_USER, __FUNCTION__, Loc);
  TIMESCOPE_WITH_IDENT(Loc);

  __tgt_target_data_end_mapper(Loc, DeviceId, ArgNum, ArgsBase, Args, ArgSizes,
                               ArgTypes, ArgNames, ArgMappers);
}

EXTERN void __tgt_target_data_update_mapper(ident_t *Loc, int64_t DeviceId,
                                            int32_t ArgNum, void **ArgsBase,
                                            void **Args, int64_t *ArgSizes,
                                            int64_t *ArgTypes,
                                            map_var_info_t *ArgNames,
                                            void **ArgMappers) {
  PROF_SCOPED_WITH_IDENT(PROF_LVL_USER, __FUNCTION__, Loc);
  TIMESCOPE_WITH_IDENT(Loc);
  DP("Entering data update with %d mappings\n", ArgNum);

  // If DeviceId equals to RESTART_DEVICE, it means that this is a tasks being
  // restarted
  const bool Restarting = (DeviceId == RESTART_DEVICE) && ft_handler.isActive();
  // Redefine DeviceId in case of restart
  DeviceId = (DeviceId == RESTART_DEVICE) ? OFFLOAD_DEVICE_DEFAULT : DeviceId;

  // FT Handling
  int32_t CustomId;
  if (ft_handler.isActive()) {
    if (!Restarting) {
      CustomId = ft_handler.TaskRegisterArgs(
          InterfaceTaskFunction::itf_tgt_target_data_update_mapper, Loc,
          DeviceId, nullptr, ArgNum, ArgsBase, Args, ArgSizes, ArgTypes,
          ArgNames, ArgMappers, -1, -1, {});
      // Check if there is any FT procedure pending
      while (ft_handler.RestartCheck() || ft_handler.CPCheck())
        ;
    } else {
      CustomId = ft_handler.TaskGetCurrentTargetID();
    }
  }

  if (checkDeviceAndCtors(DeviceId, Loc)) {
    DP("Not offloading to device %" PRId64 "\n", DeviceId);
    return;
  }

  DeviceTy &Device = getDataDevice(Loc, DeviceId);

  if (ft_handler.isActive()) {
    ft_handler.addDeviceTaskMapEntry(CustomId, Device.DeviceID);
    DP("[FT-Handler] - Executing task %d (tgt data update)\n", CustomId);
  }

  if (getInfoLevel() & OMP_INFOTYPE_KERNEL_ARGS)
    printKernelArguments(Loc, DeviceId, ArgNum, ArgSizes, ArgTypes, ArgNames,
                         "Updating OpenMP data");

  AsyncInfoTy AsyncInfo(Device);
  DataManagementParam DMParam;
  int Rc = targetDataUpdate(Loc, Device, ArgNum, ArgsBase, Args, ArgSizes,
                            ArgTypes, ArgNames, ArgMappers, DMParam, AsyncInfo);

  if (Rc == OFFLOAD_SUCCESS)
    Rc = AsyncInfo.synchronize();

  // Avoid handling outcome if Rc is OFFLOAD_EVENT_FAILED
  if (Rc == OFFLOAD_EVENT_FAILED && ft_handler.isActive())
    handleTargetOutcome(Rc == OFFLOAD_EVENT_FAILED, Loc);
  else
    handleTargetOutcome(Rc == OFFLOAD_SUCCESS, Loc);

  if (ft_handler.isActive())
    ft_handler.TaskFinish(CustomId);

  if (Rc == OFFLOAD_SUCCESS)
    MarkExecutedPinnedTask(Device);

  if (Rc != OFFLOAD_SUCCESS && ft_handler.isActive())
    MarkFailedTask(CustomId, Device);
}

EXTERN void __tgt_target_data_update_nowait_mapper(
    ident_t *Loc, int64_t DeviceId, int32_t ArgNum, void **ArgsBase,
    void **Args, int64_t *ArgSizes, int64_t *ArgTypes, map_var_info_t *ArgNames,
    void **ArgMappers, int32_t DepNum, void *DepList, int32_t NoAliasDepNum,
    void *NoAliasDepList) {
  PROF_SCOPED_WITH_IDENT(PROF_LVL_USER, __FUNCTION__, Loc);
  TIMESCOPE_WITH_IDENT(Loc);

  __tgt_target_data_update_mapper(Loc, DeviceId, ArgNum, ArgsBase, Args,
                                  ArgSizes, ArgTypes, ArgNames, ArgMappers);
}

/// Implements a kernel entry that executes the target region on the specified
/// device.
///
/// \param Loc Source location associated with this target region.
/// \param DeviceId The device to execute this region, -1 indicated the default.
/// \param NumTeams Number of teams to launch the region with, -1 indicates a
///                 non-teams region and 0 indicates it was unspecified.
/// \param ThreadLimit Limit to the number of threads to use in the kernel
///                    launch, 0 indicates it was unspecified.
/// \param HostPtr  The pointer to the host function registered with the kernel.
/// \param Args     All arguments to this kernel launch (see struct definition).
EXTERN int __tgt_target_kernel(ident_t *Loc, int64_t DeviceId, int32_t NumTeams,
                               int32_t ThreadLimit, void *HostPtr,
                               __tgt_kernel_arguments *Args) {
  PROF_SCOPED_WITH_IDENT(PROF_LVL_USER, __FUNCTION__, Loc);
  TIMESCOPE_WITH_IDENT(Loc);
  DP("Entering target region with entry point " DPxMOD " and device Id %" PRId64
     "\n",
     DPxPTR(HostPtr), DeviceId);
  if (Args->Version != 1) {
    DP("Unexpected ABI version: %d\n", Args->Version);
  }

  // If DeviceId equals to RESTART_DEVICE, it means that this is a tasks being
  // restarted
  const bool Restarting = (DeviceId == RESTART_DEVICE) && ft_handler.isActive();
  // Redefine DeviceId in case of restart
  DeviceId = (DeviceId == RESTART_DEVICE) ? OFFLOAD_DEVICE_DEFAULT : DeviceId;

  // FT Handling
  int32_t CustomId;
  if (ft_handler.isActive()) {
    if (!Restarting) {
      CustomId = ft_handler.TaskRegisterArgs(
          InterfaceTaskFunction::itf_tgt_target_kernel, Loc, DeviceId, HostPtr,
          0, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, NumTeams,
          ThreadLimit, *Args);
      // Check if there is any FT procedure pending
      while (ft_handler.RestartCheck() || ft_handler.CPCheck())
        ;
    } else {
      CustomId = ft_handler.TaskGetCurrentTargetID();
    }
  }

  if (checkDeviceAndCtors(DeviceId, Loc)) {
    DP("Not offloading to device %" PRId64 "\n", DeviceId);
    return OMP_TGT_FAIL;
  }

  // TODO: Only schedule MPI devices. Remove when device groups are added.
  if (PM->Devices[DeviceId]->RTL->IsMPI &&
      !PM->Devices[DeviceId]->RTL->DisableDataManager) {
    DeviceId = Schedule(Loc, DeviceId);

    if (checkDeviceAndCtors(DeviceId, Loc)) {
      DP("Not offloading to device %" PRId64 "\n", DeviceId);
      return OMP_TGT_FAIL;
    }
  }

  DeviceTy &Device = *PM->Devices[DeviceId];

  if (ft_handler.isActive()) {
    ft_handler.addDeviceTaskMapEntry(CustomId, Device.DeviceID);
    DP("[FT-Handler] - Executing task %d (tgt target) - Device %d\n", CustomId,
       Device.DeviceID);
  }

  if (getInfoLevel() & OMP_INFOTYPE_KERNEL_ARGS)
    printKernelArguments(Loc, DeviceId, Args->NumArgs, Args->ArgSizes,
                         Args->ArgTypes, Args->ArgNames,
                         "Entering OpenMP kernel");
#ifdef OMPTARGET_DEBUG
  for (int I = 0; I < Args->NumArgs; ++I) {
    DP("Entry %2d: Base=" DPxMOD ", Begin=" DPxMOD ", Size=%" PRId64
       ", Type=0x%" PRIx64 ", Name=%s\n",
       I, DPxPTR(Args->ArgBasePtrs[I]), DPxPTR(Args->ArgPtrs[I]),
       Args->ArgSizes[I], Args->ArgTypes[I],
       (Args->ArgNames) ? getNameFromMapping(Args->ArgNames[I]).c_str()
                        : "unknown");
  }
#endif

  bool IsTeams = NumTeams != -1;
  if (!IsTeams)
    NumTeams = 0;

  const int32_t TaskID =
      PM->Devices[DeviceId]->RTL->IsMPI &&
              !PM->Devices[DeviceId]->RTL->DisableDataManager && !Restarting
          ? CustomId
          : -1;
  AsyncInfoTy AsyncInfo(Device, TaskID);
  int Rc = target(Loc, Device, HostPtr, Args->NumArgs, Args->ArgBasePtrs,
                  Args->ArgPtrs, Args->ArgSizes, Args->ArgTypes, Args->ArgNames,
                  Args->ArgMappers, NumTeams, ThreadLimit, Args->Tripcount,
                  IsTeams, AsyncInfo);
  if (Rc == OFFLOAD_SUCCESS)
    Rc = AsyncInfo.synchronize();

  // Avoid handling outcome if Rc is OFFLOAD_EVENT_FAILED
  if (Rc == OFFLOAD_EVENT_FAILED && ft_handler.isActive()) {
    handleTargetOutcome(Rc == OFFLOAD_EVENT_FAILED, Loc);
    assert(Rc == OFFLOAD_EVENT_FAILED &&
           "__tgt_target_mapper unexpected failure!");
  } else {
    handleTargetOutcome(Rc == OFFLOAD_SUCCESS, Loc);
    assert(Rc == OFFLOAD_SUCCESS && "__tgt_target_mapper unexpected failure!");
  }

  if (ft_handler.isActive())
    ft_handler.TaskFinish(CustomId);

  if (Rc == OFFLOAD_SUCCESS)
    MarkExecutedTask(DeviceId);

  if (Rc != OFFLOAD_SUCCESS && ft_handler.isActive())
    MarkFailedTask(CustomId, Device);

  return OMP_TGT_SUCCESS;
}

EXTERN int __tgt_target_kernel_nowait(
    ident_t *Loc, int64_t DeviceId, int32_t NumTeams, int32_t ThreadLimit,
    void *HostPtr, __tgt_kernel_arguments *Args, int32_t DepNum, void *DepList,
    int32_t NoAliasDepNum, void *NoAliasDepList) {
  PROF_SCOPED_WITH_IDENT(PROF_LVL_USER, __FUNCTION__, Loc);
  TIMESCOPE_WITH_IDENT(Loc);

  return __tgt_target_kernel(Loc, DeviceId, NumTeams, ThreadLimit, HostPtr,
                             Args);
}

// Get the current number of components for a user-defined mapper.
EXTERN int64_t __tgt_mapper_num_components(void *RtMapperHandle) {
  TIMESCOPE();
  auto *MapperComponentsPtr = (struct MapperComponentsTy *)RtMapperHandle;
  int64_t Size = MapperComponentsPtr->Components.size();
  DP("__tgt_mapper_num_components(Handle=" DPxMOD ") returns %" PRId64 "\n",
     DPxPTR(RtMapperHandle), Size);
  return Size;
}

// Push back one component for a user-defined mapper.
EXTERN void __tgt_push_mapper_component(void *RtMapperHandle, void *Base,
                                        void *Begin, int64_t Size, int64_t Type,
                                        void *Name) {
  TIMESCOPE();
  DP("__tgt_push_mapper_component(Handle=" DPxMOD
     ") adds an entry (Base=" DPxMOD ", Begin=" DPxMOD ", Size=%" PRId64
     ", Type=0x%" PRIx64 ", Name=%s).\n",
     DPxPTR(RtMapperHandle), DPxPTR(Base), DPxPTR(Begin), Size, Type,
     (Name) ? getNameFromMapping(Name).c_str() : "unknown");
  auto *MapperComponentsPtr = (struct MapperComponentsTy *)RtMapperHandle;
  MapperComponentsPtr->Components.push_back(
      MapComponentInfoTy(Base, Begin, Size, Type, Name));
}

EXTERN void __tgt_set_info_flag(uint32_t NewInfoLevel) {
  std::atomic<uint32_t> &InfoLevel = getInfoLevelInternal();
  InfoLevel.store(NewInfoLevel);
  for (auto &R : PM->RTLs.AllRTLs) {
    if (R.set_info_flag)
      R.set_info_flag(NewInfoLevel);
  }
}

EXTERN int __tgt_print_device_info(int64_t DeviceId) {
  return PM->Devices[DeviceId]->printDeviceInfo(
      PM->Devices[DeviceId]->RTLDeviceID);
}
