//===-------------------- ft.cpp - Fault Tolerance ------------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Definitions for Fault Tolerance integration between MPI Plugin and Omptarget
//
//===----------------------------------------------------------------------===//

#include "ft.h"

#include "private.h"
#include "profiler.h"
#include "rtl.h"
#include "scheduler.h"
#include <omptarget.h>

#include <algorithm>
#include <queue>
#include <string>
#include <utility>

// Every procedure of this library will be executed on host process, since only
// the host process binds the notification function. It is also valid to notice
// that only one thread execute the FT operations

// Create an instance of Notification Handler accessible by everyone
FTNotificationHandler ft_handler;

static bool using_ft = false;

/// Function that is bound as callback to the FT notification callbacks in the
/// plugin
void FTNotificationCallback(FTNotification notify_handler) {
  switch (notify_handler.notification_id) {
  case FTNotificationID::FAILURE: {
    // The libomptarget devices structures does not map the MPI rank 0
    DP("[FT-Handler] - FAILURE: %d\n", notify_handler.value - 1);
    // Add to the known dead processes
    ft_handler.DevicesHandleFailed(notify_handler.value - 1);
    if (ft_handler.isReferencesSet())
      ft_handler.RestartLaunch(notify_handler.value - 1);
    break;
  }
  case FTNotificationID::FALSE_POSITIVE: {
    DP("[FT-Handler] - FALSE_POSITIVE: %d\n", notify_handler.value);
    ft_handler.DevicesRemoveFailed(notify_handler.value);
    break;
  }
  case FTNotificationID::CHECKPOINT: {
    DP("[FT-Handler] - CHECKPOINT\n");
    if (ft_handler.isReferencesSet())
      ft_handler.CPLaunch();
    break;
  }
  case FTNotificationID::CHECKPOINT_DONE: {
    DP("[FT-Handler] - CHECKPOINT_DONE\n");
    if (ft_handler.isReferencesSet())
      ft_handler.CPFinish(notify_handler.value);
    break;
  }
  }
}

// === General notification procedures
// ===-----------------------------------------------------------------------===
/// Notification class constructor for FT handling on the interface
FTNotificationHandler::FTNotificationHandler() {
  using_ft = false;
  if (char *env_str = getenv("OMPCLUSTER_CP_USEVELOC"))
    if (std::stoi(env_str) == 1)
      using_ft = true;

  if (char *env_str = getenv("OMPCLUSTER_FT_DISABLE"))
    if (std::stoi(env_str) == 1)
      using_ft = false;

  devices.clear();
  restart_failed_tasks.clear();
  restart_task_list.clear();
  device_task_exec_count.clear();
  cp_state = CPState::NOT_RUNNING;
  restart_state = RestartState::COMPLETED;
  execution_state = ExecutionState::RUNNING;
  running_tasks = 0;
  cp_next_version = 0;
  schedule_map = nullptr;
  task_graph = nullptr;
}

void FTNotificationHandler::deactivate() { using_ft = false; }

bool FTNotificationHandler::isActive() { return using_ft; }

void FTNotificationHandler::RTLSetRefs() {
  // Get the first device and proxy device related to the MPI Devices
  for (size_t i = 0; i < PM->Devices.size(); i++) {
    if (PM->Devices[i]->RTL->IsMPI &&
        !PM->Devices[i]->RTL->DisableDataManager) {
      first_device_idx = i;
      proxy_device = PM->Devices[i]->ProxyDevice;
      RTL = PM->Devices[i]->RTL;
      break;
    }
  }
}

// === Tasking and mapping related procedures
// ===-----------------------------------------------------------------------===
/// Sets an reference to the schedule map used by the scheduler and the task
/// graph used by OpenMP
void FTNotificationHandler::setGraphRefs(ScheduleMap *sch_map,
                                         kmp_target_task_map_t *graph,
                                         BaseScheduler *sch) {
  if (!isActive())
    return;

  std::lock_guard<std::mutex> lock(internal_mtx);
  schedule_map = sch_map;
  task_graph = graph;
  scheduler = sch;
  for (int32_t idx = 0; idx < task_graph->num_tasks; idx++) {
    kmp_target_task_t task = task_graph->tasks[idx];
    // Mark the task with highest id. Used to avoid looking at tasks with
    // non-valid id at the restart operation
    if (task.task_id > max_task_id)
      max_task_id = task.task_id;
  }

  thread_task_map.resize(__kmpc_global_num_threads(nullptr), INVALID_TASK);
}

/// Verifies if the references to the schedule map and taskgraph are set. This
/// avoid calls to the FT Handler functions when no graphs are being used.
bool FTNotificationHandler::isReferencesSet() {
  return (task_graph != nullptr && schedule_map != nullptr);
}

/// Adds an entry the the device_task_map, representing that task with id \p
/// task_id is executing on device \p device_id
void FTNotificationHandler::addDeviceTaskMapEntry(int32_t task_id,
                                                  int32_t device_id) {
  std::lock_guard<std::mutex> lock(internal_mtx);
  if (device_id >= 0)
    device_task_exec_count[device_id]++;
  device_task_map[task_id] = device_id;
  running_tasks++;
}

/// Removes the entry from the device_task related to the task with id
/// \p task_id
void FTNotificationHandler::removeDeviceTaskMapEntry(int32_t task_id) {
  bool ready_to_cp = false;
  int32_t device_id = -1;
  {
    std::lock_guard<std::mutex> lock(internal_mtx);
    device_id = device_task_map[task_id];
    device_task_map.erase(task_id);
    if (device_id >= 0) {
      device_task_exec_count[device_id]--;
      if (device_task_exec_count[device_id] == 0 &&
          cp_state == CPState::RUNNING &&
          cp_new.dev_state[device_id + 1] == CPDevState::NONE) {
        DP("[FT-Handler] - Rank %d is ready for CP\n", device_id + 1);
        ready_to_cp = true;
      }
    }
  }
  if (ready_to_cp) {
    std::lock_guard<std::mutex> lock(cp_queue_mtx);
    cp_queue.push(device_id + 1);
    DP("[FT-Handler] - Added device %d to CP queue\n", device_id + 1);
  }
}

/// Adds the task with id equal to \p task_id to the restart_failed_tasks
/// mapping
void FTNotificationHandler::addFailedTask(int32_t task_id) {
  std::lock_guard<std::mutex> lock(internal_mtx);
  auto pos = std::find(restart_failed_tasks.begin(), restart_failed_tasks.end(),
                       task_id);
  if (pos == restart_failed_tasks.end()) {
    restart_failed_tasks.push_back(task_id);
  }
}

// === Devices related procedures
// ===-----------------------------------------------------------------------===
/// Constructs the device state vector
void FTNotificationHandler::DevicesSetNumber(int32_t number_of_devices) {
  std::lock_guard<std::mutex> lock(devices_mtx);
  devices.resize(number_of_devices, DeviceState::ALIVE);
  device_task_exec_count.resize(number_of_devices, 0);
}

/// Marks the device with id equal to \p device_id as DEAD in the map devices
void FTNotificationHandler::DevicesAddFailed(int device_id) {
  std::lock_guard<std::mutex> lock(devices_mtx);
  // This map contains only worker devices
  devices[device_id] = DeviceState::DEAD;
  devices_failed_count++;
}

/// Marks the device with id equal to \p device_id as ALIVE in the map of
/// devices (occurs when a false-positive failure is detected)
void FTNotificationHandler::DevicesRemoveFailed(int device_id) {
  // Remove from known dead processes
  std::lock_guard<std::mutex> lock(devices_mtx);
  devices[device_id] = DeviceState::ALIVE;
  devices_failed_count--;
}

/// Looks at the if the device with id equal to \p device_id was executing any
/// task, if so, add the tasks to the restart_failed_tasks map
void FTNotificationHandler::DevicesHandleFailed(int device_id) {
  std::lock_guard<std::mutex> lock(internal_mtx);
  for (const auto entry : device_task_map) {
    if (entry.second == device_id) {
      restart_failed_tasks.push_back(entry.first);
      DP("[FT-Handler] - Failed device (%d) was executing task %d\n", device_id,
         entry.first);
    }
  }
}

/// Returns the state of te device with id equals to \p device_id, if the device
/// is up, the state will be ALIVE, otherwise, the state will be DEAD
DeviceState FTNotificationHandler::DevicesCheck(int device_id) {
  std::lock_guard<std::mutex> lock(devices_mtx);
  if (device_id == -1)
    return DeviceState::DEAD;
  return devices[device_id];
}

/// Returns the current number of active devices
int32_t FTNotificationHandler::DevicesGetActiveNumber() {
  if (!isActive())
    return devices.size();

  std::lock_guard<std::mutex> lock(devices_mtx);
  int32_t size = devices.size() - devices_failed_count;
  return size;
}

/// Translates the \p device_id into an actual device rank that is alive
int32_t FTNotificationHandler::DevicesTranslate(int device_id) {
  if (!isActive())
    return device_id;

  std::lock_guard<std::mutex> lock(devices_mtx);
  int alive_device_id = device_id;
  int alive_device_count = -1;
  for (std::size_t i = 0; i < devices.size(); i++) {
    if (devices[i] == DeviceState::ALIVE) {
      alive_device_count++;
      if (alive_device_count == device_id) {
        alive_device_id = i;
        break;
      }
    }
  }
  return alive_device_id;
}

// === Checkpointing related procedures
// ===-----------------------------------------------------------------------===
/// Discovers in which device the most recent version of the buffer is located
/// and register it to be saved in that device in the future
void FTNotificationHandler::CPEvalBuffer(uintptr_t hst_ptr, int64_t ptr_size) {
  uintptr_t tgt_ptr = hst_ptr;
  int32_t cp_device_id = 0;

  // Look for mappings for this ptr
  DeviceTy::HDTTMapAccessorTy ProxyMapAccessor =
      proxy_device->HostDataToTargetMap.getExclusiveAccessor();
  LookupResult lr =
      proxy_device->lookupMapping(ProxyMapAccessor, (void *)hst_ptr, ptr_size);

  if (lr.Exists()) {
    std::lock_guard<decltype(*lr.Entry)> proxy_entry_map_lock(*lr.Entry);
    std::lock_guard<decltype(*lr.Entry->getActiveEntries().back())>
        link_entry_map_lock(*lr.Entry->getActiveEntries().back());

    cp_device_id =
        lr.Entry->getActiveEntries().back()->getOwner()->DeviceID + 1;
    tgt_ptr = lr.Entry->getActiveEntries().back()->TgtPtrBegin;
  }

  // Add to the save map
  if (cp_new.ptr_map[hst_ptr].cp_device_id != -1) {
    if (cp_new.ptr_map[hst_ptr].cp_device_id != cp_device_id) {
      DP("[FT-Handler] - Updating save map entry for buffer (hst: " DPxMOD
         ") from device %d (tgt: " DPxMOD ") to device %d (tgt: " DPxMOD " \n",
         DPxPTR(hst_ptr), cp_new.ptr_map[hst_ptr].cp_device_id,
         DPxPTR(cp_new.ptr_map[hst_ptr].tgt_ptr), cp_device_id,
         DPxPTR(tgt_ptr));
    } else {
      return;
    }
  }
  cp_new.ptr_map[hst_ptr].cp_device_id = cp_device_id;
  cp_new.ptr_map[hst_ptr].tgt_ptr = tgt_ptr;
  cp_new.dev_map[cp_device_id].ptrs.push_back(&cp_new.ptr_map[hst_ptr]);

  DP("[FT-Handler] - Registering pointer hst " DPxMOD " (tgt " DPxMOD
     ") with size %ld in device %d\n",
     DPxPTR(hst_ptr), DPxPTR(tgt_ptr), ptr_size, cp_device_id);
}

void FTNotificationHandler::CPEvalTask(int32_t task_id) {
  if (task_id < 0)
    return;

  // Skip if this task started after CP setup
  if (cp_new.executing_tasks.find(task_id) == cp_new.executing_tasks.end())
    return;

  DP("[FT-Handler] - Evaluating finished task (ID: %d)\n", task_id);
  const kmp_target_task_t *task = schedule_map->at(task_id).task;

  for (int32_t i = 0; i < task->target_data.num_args; i++) {
    if ((task->target_data.arg_types[i] & OMP_TGT_MAPTYPE_LITERAL) ||
        (task->target_data.arg_types[i] & OMP_TGT_MAPTYPE_PRIVATE))
      continue;

    uintptr_t hst_ptr = (uintptr_t)task->target_data.args_base[i];
    if (cp_new.ptr_map.find(hst_ptr) != cp_new.ptr_map.end()) {
      DP("[FT-Handler] - Task: %d finished using buffer " DPxMOD
         " evaluating the buffer\n",
         task_id, DPxPTR((void *)hst_ptr));
      CPEvalBuffer(hst_ptr, cp_new.ptr_map[hst_ptr].ptr_size);
    }
  }
  cp_new.executing_tasks[task_id] = true;

  // If all tasks are done, host can start checkpointing
  if (std::all_of(cp_new.executing_tasks.begin(), cp_new.executing_tasks.end(),
                  [&](const auto &entry) { return entry.second == true; })) {
    if (cp_new.dev_state[0] == CPDevState::NONE) {
      DP("[FT-Handler] - Rank 0 is ready for CP.\n");
      std::lock_guard<std::mutex> lock_queue(cp_queue_mtx);
      cp_queue.push(0);
      DP("[FT-Handler] - Rank 0 added to CP queue\n");
    }
  }
}

/// Select which pointers will be saved by the CP
void FTNotificationHandler::CPSelectPtrs() {
  cp_new.ptr_map.clear();
  cp_new.executing_tasks.clear();

  // Define all buffers the CP will save
  for (int32_t task_idx = 0; task_idx < task_graph->num_tasks; task_idx++) {
    const kmp_target_task_t task = task_graph->tasks[task_idx];

    // SAVED tasks are checkpointed tasks, so we ignore them
    if (schedule_map->at(task.task_id).state == TaskState::SAVED)
      continue;

    // EXECUTED tasks will be SAVED tasks after the checkpoint, so we ignore
    if (schedule_map->at(task.task_id).state == TaskState::EXECUTED) {
      cp_new.saved_tasks.push_back(task.task_id);
      continue;
    }

    // DISPATCHED tasks will also be SAVED tasks after the checkpoint, ignore
    if (schedule_map->at(task.task_id).state == TaskState::DISPATCHED) {
      cp_new.saved_tasks.push_back(task.task_id);
      cp_new.executing_tasks[task.task_id] = false;
      continue;
    }

    for (int32_t i = 0; i < task.target_data.num_args; i++) {
      // Ignore private variables and arrays - there is no mapping for them.
      if ((task.target_data.arg_types[i] & OMP_TGT_MAPTYPE_LITERAL) ||
          (task.target_data.arg_types[i] & OMP_TGT_MAPTYPE_PRIVATE))
        continue;

      uintptr_t hst_ptr = (uintptr_t)task.target_data.args_base[i];
      // Only register the pointer if not registered.
      if (cp_new.ptr_map.find(hst_ptr) == cp_new.ptr_map.end()) {
        cp_new.ptr_map[hst_ptr] = {
            .hst_ptr = hst_ptr,
            .tgt_ptr = (uintptr_t) nullptr,
            .ptr_size = task.target_data.arg_sizes[i],
            .cp_buffer_id = -1,
            .cp_device_id = -1,
        };
      } else {
        // Some target tasks will have a 0 size argument for some pointer
        // because of the implicitly mapping. We need to capture the size
        // from the data begin tasks
        if (cp_new.ptr_map[hst_ptr].ptr_size < task.target_data.arg_sizes[i])
          cp_new.ptr_map[hst_ptr].ptr_size = task.target_data.arg_sizes[i];
      }
    }
  }

  for (auto entry : cp_new.ptr_map)
    CPEvalBuffer(entry.first, entry.second.ptr_size);
}

/// Executes the Checkpoint in the given \p device_id device
void FTNotificationHandler::CPExec(int32_t device_id) {
  if (cp_new.canceled || cp_state == CPState::NOT_RUNNING)
    return;

  cp_new.dev_map[device_id].cp_version = cp_next_version;
  DP("[FT-Handler] - Checkpointing device %d (version %d)\n", device_id,
     cp_new.dev_map[device_id].cp_version);

  if (device_id != 0 && DevicesCheck(device_id - 1) == DeviceState::DEAD) {
    DP("[FT-Handler] - Skipping checkpoint request for failed device: %d\n",
       device_id);
    return;
  }

  PROF_SCOPED(PROF_LVL_ALL, "Checkpointing Device", [&]() -> std::string {
    std::stringstream details;
    details << "{";
    details << "\"device_id\":" << device_id;
    details << "}";
    return details.str();
  }());

  DeviceTy &device =
      (device_id == 0) ? *proxy_device : *PM->Devices[device_id - 1];
  DeviceTy::HDTTMapAccessorTy DeviceMapAccessor =
      device.HostDataToTargetMap.getExclusiveAccessor();

  // Avoid registering deleted buffers
  int32_t curr_buffer_id = 0;

  if (device_id == 0) {
    for (auto ptr : cp_new.dev_map[device_id].ptrs) {
      cp_new.dev_map[device_id].ptr_sizes.push_back(ptr->ptr_size);
      cp_new.dev_map[device_id].tgt_ptrs.push_back(ptr->hst_ptr);
      ptr->cp_buffer_id = curr_buffer_id++;
    }
  } else {
    for (auto ptr : cp_new.dev_map[device_id].ptrs) {
      LookupResult lr = device.lookupMapping(
          DeviceMapAccessor, (void *)ptr->hst_ptr, ptr->ptr_size);
      if (lr.Exists()) {
        cp_new.dev_map[device_id].ptr_sizes.push_back(ptr->ptr_size);
        cp_new.dev_map[device_id].tgt_ptrs.push_back(ptr->tgt_ptr);
        ptr->cp_buffer_id = curr_buffer_id++;
      }
    }
  }

  cp_new.dev_map[device_id].num_ptrs = curr_buffer_id;

  __tgt_async_info AsyncInfo;
  RTL->register_cp_ptrs_async(device_id - 1,
                              cp_new.dev_map[device_id].tgt_ptrs.data(),
                              cp_new.dev_map[device_id].ptr_sizes.data(),
                              cp_new.dev_map[device_id].num_ptrs, &AsyncInfo);
  RTL->request_checkpoint_async(device_id - 1 /* host is -1 here */,
                                &cp_new.dev_map[device_id].cp_version,
                                &AsyncInfo);
  RTL->synchronize(device_id, &AsyncInfo);

  cp_new.dev_map[device_id].first_id = 0;
  cp_new.dev_map[device_id].last_id = curr_buffer_id - 1;
}

/// Cancels a checkpoint in progress
void FTNotificationHandler::CPCancel() {
  DP("[FT-Handler] - Canceling Checkpoint\n");
  std::lock_guard<std::mutex> lock(cp_queue_mtx);
  cp_queue = std::queue<int32_t>();
  cp_new.canceled = true;
  int cp_version = -2; // Fake checkpoint call
  RTL->request_checkpoint(-1 /* host is -1 here */, &cp_version);
}

/// Executes all the procedures related to saving a checkpoint
void FTNotificationHandler::CPLaunch() {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  // This is one of two main FT functions, to interact with the rest of the
  // agnostic layer, we first acquire the scheduler mutex. This will prevent any
  // task of changing its state before the end of the CP Setup, also preventing
  // tasks from starting after we start the setup.
  BaseScheduler::LockGuard lock_scheduler(scheduler);

  // Create function to request the save of the checkpoint
  const int32_t dev_number = RTL->NumberOfDevices + 1;
  // This vector hold the devices that has no ptrs to save, meaning the
  // CHECKPOINT thread will call the CP for those devices
  std::vector<int32_t> skipped_devices;
  // Execution state needs to be RUNNING. Otherwise, cancels CP at this moment.
  // TODO: In future, add a way to enable checkpoint for when there are still
  // failed tasks to run
  if (execution_state != ExecutionState::RUNNING ||
      restart_state == RestartState::IN_PROGRESS) {
    CPCancel();
    return;
  }

  PROF_SCOPED(PROF_LVL_ALL, "Checkpoint Setup");
  execution_state = ExecutionState::CHECKPOINTING;
  cp_state = CPState::RUNNING;
  cp_new.dev_state.resize(dev_number, CPDevState::NONE);
  cp_new.dev_map.clear();
  cp_new.dev_map.resize(dev_number);

  CPSelectPtrs();

  {
    // Host can only be ready if there is no more tasks running, because
    // executing tasks can bring data back to the host, meaning we need to save
    // them in the host. We are only skipping the host if there is no tasks
    // executing in the CP SETUP.
    if (cp_new.executing_tasks.empty()) {
      if (cp_new.dev_map[0].ptrs.size() > 0) {
        DP("[FT-Handler] - Rank 0 is ready for CP\n");
        std::lock_guard<std::mutex> lock_queue(cp_queue_mtx);
        cp_queue.push(0);
      } else {
        // Mark as done because the checkpoint is empty
        std::lock_guard<std::mutex> lock_queue(cp_internal_mtx);
        cp_new.dev_state[0] = CPDevState::DONE;
        skipped_devices.push_back(0);
      }
    }
    // Workers
    for (size_t i = 0; i < devices.size(); i++) {
      bool is_device_free = false;
      {
        std::lock_guard<std::mutex> lock_internal(internal_mtx);
        if (device_task_exec_count[i] == 0)
          is_device_free = true;
      }
      if (DevicesCheck(i) == DeviceState::ALIVE && is_device_free) {
        if (cp_new.dev_map[i + 1].ptrs.size() > 0) {
          DP("[FT-Handler] - Rank %lu is ready for CP\n", i + 1);
          std::lock_guard<std::mutex> lock_queue(cp_queue_mtx);
          cp_queue.push(i + 1);
        } else {
          // Mark as done because the checkpoint is empty
          std::lock_guard<std::mutex> lock_queue(cp_internal_mtx);
          cp_new.dev_state[i + 1] = CPDevState::DONE;
          skipped_devices.push_back(i + 1);
        }
      }
    }
  }

  // Finish procedure and free interface threads
  DP("[FT-Handler] - Ending Checkpoint Request\n");
  std::unique_lock<std::mutex> fti_lock(ft_interface_mtx);
  execution_state = ExecutionState::RUNNING;
  ft_interface_cv.notify_all();
  fti_lock.unlock();

  // Now we can execute the skipped devices in parallel with the other
  // tasks/CPs
  {
    PROF_SCOPED(PROF_LVL_ALL, "Checkpoint Skipped Devices");
    for (size_t i = 0; i < skipped_devices.size(); i++)
      CPExec(skipped_devices[i]);
  }
}

/// Marks the tasks that were saved and frees the thread that is waiting for the
/// checkpoint to finish. The \p version brings the result from failures in the
/// CP from the MPI FT layer (-1 if failed)
void FTNotificationHandler::CPFinish(int version) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  cp_state = CPState::NOT_RUNNING;
  if (!cp_new.canceled && version != -1) {
    if (cp_new.dev_map[0].cp_version != -1) {
      cp_last = cp_new;
      for (size_t i = 0; i < cp_last.saved_tasks.size(); i++)
        schedule_map->at(cp_last.saved_tasks[i]).state = TaskState::SAVED;
      cp_next_version++;
    }
  } else {
    DP("[FT-Handler] - Checkpoint canceled!\n");
  }
  CPContext dummy;
  cp_new = dummy;
}

bool FTNotificationHandler::CPCheck() {
  if (cp_state == CPState::NOT_RUNNING || cp_new.canceled)
    return false;

  int32_t task_id = TaskGetCurrentTargetID();
  int32_t device_id = -1;
  if (task_id > -1)
    device_id = DevicesTranslate(schedule_map->at(task_id).rank - 1);

  do {
    // Create a scope just to manipulate the queue
    int32_t cp_device;
    {
      std::lock_guard<std::mutex> lock(cp_queue_mtx);
      if (!cp_queue.empty()) {
        cp_device = cp_queue.front();
        cp_queue.pop();
      } else {
        if (execution_state != ExecutionState::RUNNING ||
            cp_new.dev_state[device_id] != CPDevState::DONE ||
            !TaskIsReady(task_id))
          return true;
        else
          return false;
      }
    }
    {
      std::lock_guard<std::mutex> lock(cp_internal_mtx);
      cp_new.dev_state[cp_device] = CPDevState::IN_PROGRESS;
    }
    CPExec(cp_device);
    {
      std::lock_guard<std::mutex> lock(cp_internal_mtx);
      cp_new.dev_state[cp_device] = CPDevState::DONE;
    }
  } while (execution_state != ExecutionState::RUNNING);
  return true;
}

// === Restart related procedures
// ===-----------------------------------------------------------------------===
/// Creates a list of the tasks that needs to be restarted. This procedure looks
/// for the tasks related to the failed task(s) and mark them to restart, it
/// also marks the related buffers to be restored.
void FTNotificationHandler::RestartCreateTaskList(const kmp_target_task_t *task,
                                                  int32_t call_task_id) {
  // Skip already marked tasks
  auto already_marked = std::find(restart_task_list.begin(),
                                  restart_task_list.end(), task->task_id);
  if (already_marked != restart_task_list.end())
    return;

  if (task->task_id < 0 || task->task_id > max_task_id) {
    // TODO: In future, the task history will disappear. An other way to
    // manipulate this tasks will be needed
    DP("[FT-Handler] - A non-target task needs re-scheduling - Searching in "
       "the history for matches\n");
    int count = 0;
    auto entry = task_interface_map.find(task->task_id);
    if (entry == task_interface_map.end())
      assertm(false, "FT Handler is not aware about a failed task\n");

    count++;
    restart_task_list.push_back(entry->first);
  } else {
    auto task_state = schedule_map->at(task->task_id).state;
    // Look for tasks that are already executed, but not saved
    if (task_state >= TaskState::DISPATCHED &&
        task_state <= TaskState::FAILED) {
      restart_task_list.push_back(task->task_id);
      // As we are going to reexecute, mark as failed task too
      schedule_map->at(task->task_id).state = TaskState::FAILED;
    } else {
      return;
    }
  }

  // verify predecessors
  for (const auto *pred = task->predecessors; pred != nullptr;
       pred = pred->next) {
    if (pred->task->task_id != call_task_id) {
      DP("[FT-Handler] - Calling pred task with id = %d from task %d \n",
         pred->task->task_id, task->task_id);
      RestartCreateTaskList(pred->task, task->task_id);
    }
  }
  // verify successors
  for (const auto *succ = task->successors; succ != nullptr;
       succ = succ->next) {
    if (succ->task->task_id != call_task_id) {
      DP("[FT-Handler] - Calling succ task with id = %d from task %d\n",
         succ->task->task_id, task->task_id);
      RestartCreateTaskList(succ->task, task->task_id);
    }
  }
}

void FTNotificationHandler::RestartTaskBuffer(std::vector<uintptr_t> hst_ptrs,
                                              int32_t device_id) {
  DeviceTy &first_device = *PM->Devices[device_id];
  AsyncInfoTy AsyncInfo(first_device);
  int32_t cp_version = cp_last.dev_map[0].cp_version;
  for (auto hst_ptr : hst_ptrs) {
    DP("[FT-Handler] - Starting recovery for buffer " DPxMOD "\n",
       DPxPTR(hst_ptr));
    auto &entry = restart_ptrs[hst_ptr];
    void *raw_ptr = entry.args_base;
    int64_t ptr_size = entry.arg_sizes;

    // If at least one map exists to this buffer, remove everything
    if (proxy_device->hasProxy(raw_ptr, ptr_size)) {
      {
        DeviceTy::HDTTMapAccessorTy ProxyMapAccessor =
            proxy_device->HostDataToTargetMap.getExclusiveAccessor();
        LookupResult lr =
            proxy_device->lookupMapping(ProxyMapAccessor, raw_ptr, ptr_size);
        lr.Entry->resetRefCount(false);
        lr.Entry->decRefCount(false);
        lr.Entry->setDeleteThreadId();
        proxy_device->deallocTgtPtr(ProxyMapAccessor, lr, false);
      }
    }

    void *tgt_ptr;
    int32_t load_device_id = -1;
    int32_t CPID = -1, CPRank = -1, CPVer = -1;

    // See if we saved this buffer
    DP("[FT-Handler] - Trying to load buffer " DPxMOD "\n", DPxPTR(raw_ptr));
    if (cp_last.ptr_map.find(hst_ptr) != cp_last.ptr_map.end()) {
      assertm(cp_last.ptr_map[hst_ptr].cp_device_id != -1,
              "Buffer was marked but not saved in last CP");
      CPID = cp_last.ptr_map[hst_ptr].cp_buffer_id;
      CPRank = cp_last.ptr_map[hst_ptr].cp_device_id;
      CPVer = cp_version;

      if (CPRank == 0) {
        // If the buffer was saved by the host, load it on the host
        tgt_ptr = raw_ptr;
        load_device_id = first_device_idx - 1;
      } else {
        // If the buffer was saved by any worker. Create a new map in the
        // first device
        DataManagementParam dm_param;
        dm_param.FromDataMap = true;

        // Deactivate the arg_type flags that requires pre allocation or
        // requires data transfer (host -> device) since we will load the
        // buffer from CP
        int64_t new_arg_type =
            entry.arg_types &
            (~(OMP_TGT_MAPTYPE_TO | OMP_TGT_MAPTYPE_ALWAYS |
               OMP_TGT_MAPTYPE_CLOSE | OMP_TGT_MAPTYPE_PRESENT));
        targetDataBegin(nullptr, first_device, entry.arg_num, &entry.args_base,
                        &entry.args, &ptr_size, &new_arg_type, nullptr, nullptr,
                        dm_param, AsyncInfo);

        // Get the new Proxy reference to recovery
        DeviceTy::HDTTMapAccessorTy FirstDevMapAccessor =
            first_device.HostDataToTargetMap.getExclusiveAccessor();
        LookupResult lr =
            first_device.lookupMapping(FirstDevMapAccessor, raw_ptr, ptr_size);
        std::lock_guard<decltype(*lr.Entry)> first_dev_entry_lock(*lr.Entry);
        assertm(lr.Exists(), "Allocated map entry should exist");
        tgt_ptr = (void *)lr.Entry->TgtPtrBegin;
        load_device_id = device_id;
      }
    } else {
      assertm(false, "Buffer not saved on the last CP");
    }

    DP("[FT-Handler] - Loading buffer " DPxMOD
       " in the device %d (CPID = %d, RankCP = %d, CPVer = %d)\n",
       DPxPTR(raw_ptr), load_device_id + 1, CPID, CPRank, CPVer);
    assertm(CPID != -1 && CPVer != -1 && CPRank != -1,
            "Needed buffer has incomplete recovery information");
    first_device.data_recovery(load_device_id, tgt_ptr, ptr_size, CPID, CPRank,
                               CPVer, AsyncInfo);
  }
  first_device.synchronize(AsyncInfo);
}

/// Evaluates all buffers of a task, recovering them in the specified device
/// that will use.
void FTNotificationHandler::RestartEvalTask(int32_t task_id) {
  const kmp_target_task_t *task = schedule_map->at(task_id).task;
  int32_t device_id =
      DevicesTranslate(schedule_map->at(task->task_id).rank - 1);
  std::vector<uintptr_t> task_restart_buffers;
  DP("[FT-Handler] - Evaluating buffers for task %d to be executed in device "
     "%d\n",
     task->task_id, device_id);
  assertm(task->task_id != INVALID_TASK, "Invalid restart task id");
  // Select from task buffers what buffers we will load
  for (size_t i = 0; i < task->target_data.num_args; i++) {
    // Ignore private variables and arrays - there is no mapping for them.
    if ((task->target_data.arg_types[i] & OMP_TGT_MAPTYPE_LITERAL) ||
        (task->target_data.arg_types[i] & OMP_TGT_MAPTYPE_PRIVATE))
      continue;

    uintptr_t hst_ptr = (uintptr_t)task->target_data.args_base[i];

    // Only loads buffers present in restart_ptrs vector
    if (restart_ptrs.find(hst_ptr) == restart_ptrs.end())
      continue;

    if (!restart_ptrs[hst_ptr].recovered) {
      task_restart_buffers.push_back(hst_ptr);
      restart_ptrs[hst_ptr].recovered = true;
    }
  }
  DP("[FT-Handler] - Evaluating %lu buffers for task %d\n",
     task_restart_buffers.size(), task->task_id);
  RestartTaskBuffer(task_restart_buffers, device_id);
  DP("[FT-Handler] - Finished evaluating buffers for task %d\n", task->task_id);
}

/// Executes all the procedures related to restarting tasks. \p device_id
/// represents the failed device.
void FTNotificationHandler::RestartLaunch(int device_id) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  // This is the other main FT function, to interact with the rest of the
  // agnostic layer, we first acquire the scheduler mutex. This will prevent any
  // task of changing its state before the end of the Restart Setup end, also
  // preventing tasks from starting after we start the setup.
  BaseScheduler::LockGuard lock_scheduler(scheduler);

  PROF_BEGIN(PROF_LVL_ALL, "Wait for Tasks");
  execution_state = ExecutionState::FAILED;

  // Cancel checkpoint if in progress
  if (cp_state == CPState::RUNNING)
    CPCancel();

  // Check if there are tasks running and waits for execution
  DP("[FT-Handler] - Received a RESTART request - waiting for current tasks to "
     "finish - Tasks running: %d\n",
     static_cast<int>(running_tasks));
  // std::unique_lock<std::mutex> ift_lock(interface_ft_mtx);
  // interface_ft_cv.wait(ift_lock, [&] { return running_tasks == 0; });
  execution_state = ExecutionState::FAILED;
  PROF_END(PROF_LVL_ALL);

  DP("[FT-Handler] - Starting the RESTART request - Tasks running: %d\n",
     static_cast<int>(running_tasks));

  DP("[FT-Handler] - Handling Failed Tasks - Size %lu\n",
     restart_failed_tasks.size());

  int failed_device = device_id;

  restart_ptrs.clear();
  restart_task_counter = 0;

  std::unordered_map<int32_t, std::vector<uintptr_t>> tasks_to_execute;
  std::vector<int32_t> ready_tasks;

  // 1st step: Calculate which tasks we need to restart
  PROF_BEGIN(PROF_LVL_ALL, "Create Restart Task List");
  // Create restart list from the tasks that were being executed in the failed
  // device
  for (const auto task_id : restart_failed_tasks) {
    DP("[FT-Handler] - Creating restart list from %d\n", task_id);
    assertm(task_id > -1,
            "[FT-Handler] - Task with id <= -1 needs to restart\n");
    RestartCreateTaskList(schedule_map->at(task_id).task, -1);
  }

  // Separate the non executed tasks, the restartable tasks
  std::unordered_map<int32_t, TaskScheduleData> non_executed_tasks;
  std::unordered_map<int32_t, TaskScheduleData> restartable_tasks;
  for (auto &task : *schedule_map) {
    TaskState task_state = task.second.state;
    if (task_state == TaskState::SCHEDULED)
      non_executed_tasks[task.first] = task.second;
    else if (task_state >= TaskState::DISPATCHED &&
             task_state <= TaskState::FAILED)
      restartable_tasks[task.first] = task.second;
  }

  // See each buffer that was the updated version on the failed node. If it is
  // used by one of the non-executed tasks, mark to be recovered.
  std::vector<std::pair<uintptr_t, int64_t>> hold_updated_buffers;
  std::unordered_map<uintptr_t, int32_t> updated_buffers_rec_loc;
  {

    DeviceTy::HDTTMapAccessorTy ProxyMA =
        proxy_device->HostDataToTargetMap.getExclusiveAccessor();

    for (auto &entry_map : *ProxyMA) {
      HostDataToTargetTy &entry = *(entry_map.HDTT);
      std::lock_guard<decltype(entry)> first_dev_entry_lock(entry);
      std::lock_guard<decltype(*entry.getActiveEntries().back())>
          link_entry_map_lock(*entry.getActiveEntries().back());
      if (entry.getActiveEntries().back()->getOwner()->DeviceID ==
          failed_device) {
        int32_t task_using = -1;
        int64_t arg_type;
        for (auto &task : non_executed_tasks) {
          for (int arg_id = 0; arg_id < task.second.task->target_data.num_args;
               arg_id++)
            if (entry.HstPtrBase ==
                (uintptr_t)task.second.task->target_data.args_base[arg_id]) {
              task_using = task.first;
              arg_type = task.second.task->target_data.arg_types[arg_id];
            }
          if (task_using != -1)
            break;
        }
        if (task_using != -1) {
          hold_updated_buffers.push_back(
              std::pair<uintptr_t, int64_t>(entry.HstPtrBase, arg_type));
          updated_buffers_rec_loc[entry.HstPtrBase] = task_using;
        }
      }
    }
  }

  // Now look at each restartable task, if they use this buffer, creat a list to
  // restart it
  for (auto ptr_info : hold_updated_buffers) {
    uintptr_t ptr = ptr_info.first;
    int64_t ptr_type = ptr_info.second;
    int64_t ptr_size = cp_last.ptr_map[ptr].ptr_size;
    bool was_used = false;
    // See if any restartable task used this buffer
    for (auto &task : restartable_tasks) {
      for (int arg_id = 0; arg_id < task.second.task->target_data.num_args;
           arg_id++) {
        if (ptr == (uintptr_t)task.second.task->target_data.args_base[arg_id]) {
          RestartCreateTaskList(task.second.task, -1);
          was_used = true;
          break;
        }
      }
    }

    // Create an entry in the restart_ptrs buffer with immediate recover if this
    // was not used by any restartable task
    if (!was_used) {
      DP("[FT-Handler] - Registering buffer for immediate recover " DPxMOD "\n",
         DPxPTR(ptr));
      restart_ptrs[ptr] = {
          .arg_num = 1,
          .args_base = (void *)ptr,
          .args = (void *)ptr,
          .arg_sizes = ptr_size,
          .arg_types = ptr_type,
          .recovered = false,
          .related_task = updated_buffers_rec_loc[ptr],
      };
    }
  }

  restart_task_counter = static_cast<int32_t>(restart_task_list.size());

  for (auto failed_task_id : restart_task_list) {
    const auto &task = task_interface_map[failed_task_id];
    restarted_list[task.task_id] = false;

    // Register the buffers of the task to be saved
    for (int i = 0; i < task.arg_num; i++) {
      // Ignore private variables and arrays - there is no mapping for them.
      if ((task.arg_types[i] & OMP_TGT_MAPTYPE_LITERAL) ||
          (task.arg_types[i] & OMP_TGT_MAPTYPE_PRIVATE))
        continue;
      uintptr_t hst_ptr = (uintptr_t)task.args_base[i];
      if (cp_last.ptr_map.find(hst_ptr) != cp_last.ptr_map.end()) {
        void *ptr = task.args_base[i];
        int64_t ptr_size = cp_last.ptr_map[hst_ptr].ptr_size;
        restart_ptrs[hst_ptr] = {
            .arg_num = 1,
            .args_base = ptr,
            .args = ptr,
            .arg_sizes = ptr_size,
            .arg_types = task.arg_types[i],
            .recovered = false,
            .related_task = -1,
        };
        DP("[FT-Handler] - Restart task %d needs the buffer " DPxMOD
           " with size %lu\n",
           task.task_id, DPxPTR(ptr), ptr_size);
      } else {
        DP("[FT-Handler] - Haven't found the buffer " DPxMOD
           " in the buffers saved in the last valid checkpoint.\n",
           DPxPTR((void *)hst_ptr));
      }
    }

    // If the task is ready, add to the reexecute queue
    if (TaskIsReady(failed_task_id)) {
      DP("[FT-Handler] - Adding task %d to the Restart Queue as it is Ready\n",
         failed_task_id);
      ready_tasks.push_back(failed_task_id);
      restart_queue.push(task_interface_map.at(failed_task_id));
      restarted_list[task.task_id] = true;
    }
  }
  PROF_END(PROF_LVL_ALL);

  // 2.5 step: Set failed tasks ids in the MPI Plugin
  proxy_device->RTL->register_failed_tasks_ids(restart_task_counter,
                                               restart_task_list.data());

  // 3rd step: Re-schedule the graph. Here the tasks that failed and remaining
  // tasks will be scheduled again in the remaining devices
  {
    PROF_SCOPED(PROF_LVL_ALL, "Re-schedule TaskGraph");
    DevicesAddFailed(failed_device);
    reScheduleGraph(RTL);
  }

  // 4rd step: Evaluate each ready task deciding where each buffer will be
  // loaded. If multiple ready tasks uses the same buffer, the buffer will be
  // loaded for the first task in the list
  for (auto ready_task_id : ready_tasks) {
    DP("[FT-Handler] - Evaluating buffers for task %d\n", ready_task_id);
    RestartEvalTask(ready_task_id);
  }
  // Load immediate recover buffer
  for (auto ptr_info : hold_updated_buffers) {
    uintptr_t hst_ptr = ptr_info.first;
    int32_t dev_number = RTL->NumberOfDevices + 1;
    std::vector<std::vector<uintptr_t>> imm_recover_buffers;
    imm_recover_buffers.resize(dev_number);
    if (restart_ptrs[hst_ptr].related_task != -1) {
      DP("[FT-Handler] - Evaluating immediate recover for buffer " DPxMOD "\n",
         DPxPTR(hst_ptr));
      int32_t device_id = DevicesTranslate(
          schedule_map->at(updated_buffers_rec_loc[hst_ptr]).rank - 1);
      imm_recover_buffers[device_id].push_back(hst_ptr);
    }
    for (int32_t i = 0; i < dev_number; i++)
      if (imm_recover_buffers[i].size() > 0)
        RestartTaskBuffer(imm_recover_buffers[i], i);
  }

  if (restart_task_counter > 0)
    restart_state = RestartState::IN_PROGRESS;

  DP("[FT-Handler] - There is a total of %d to be re-executed\n",
     restart_task_counter.load());

  // Clear related structures
  restart_failed_tasks.clear();
  restart_task_list.clear();

  // Finish procedure and free interface threads
  DP("[FT-Handler] - Ending the RESTART request - Tasks running: %d\n",
     static_cast<int>(running_tasks));
  std::unique_lock<std::mutex> fti_lock(ft_interface_mtx);
  execution_state = ExecutionState::RUNNING;
  ft_interface_cv.notify_all();
  fti_lock.unlock();
}

/// Check if there are failed tasks in the re-execution queue and execute them
bool FTNotificationHandler::RestartCheck() {
  if (restart_task_counter <= 0)
    return false;
  // This way of restarting tasks prioritize the failed tasks, but permits that
  // other tasks execute if there are worker threads available
  TaskInterfaceArgs reexec_task;
  int32_t main_id = __kmpc_target_task_current_task_id(); // Id of main caller
  do {
    // Create a scope just to manipulate the queue, the call for the restart
    // does not need to hold the mutex
    {
      std::lock_guard<std::mutex> lock(restart_queue_mtx);
      if (!restart_queue.empty()) {
        reexec_task = restart_queue.front();
        restart_queue.pop();
      } else {
        // If there is a new FT operation, or if the main caller task isn't
        // ready yet
        if (execution_state != ExecutionState::RUNNING || !TaskIsReady(main_id))
          return true;
        else
          return false;
      }
    }
    DP("[FT-Handler] - Reexecuting task %d\n", reexec_task.task_id);
    int32_t thread_num = __kmpc_global_thread_num(nullptr);
    thread_task_map[thread_num] = reexec_task.task_id;
    {
      PROF_SCOPED(PROF_LVL_ALL, "Reexecute Task", [&]() -> std::string {
        std::stringstream details;
        details << "{";
        details << "\"task_id\":" << reexec_task.task_id << ",";
        details << "\"worker_thread\":" << thread_num;
        details << "}";
        return details.str();
      }());
      RestartExec(reexec_task, RESTART_DEVICE);
      RestartFinishTask(reexec_task.task_id);
    }
    thread_task_map[thread_num] = INVALID_TASK;
    restart_task_counter--;
    DP("[FT-Handler] - Finished re-executing task %d - Restart task counter "
       "%d\n",
       reexec_task.task_id, restart_task_counter.load());
    if (restart_task_counter == 0) {
      restart_state = RestartState::COMPLETED;
      proxy_device->RTL->register_failed_tasks_ids(0, nullptr);
    }
  } while (execution_state != ExecutionState::RUNNING);
  return true;
}

void FTNotificationHandler::RestartExec(TaskInterfaceArgs &task,
                                        int device_id) {
  switch (task.interface_function) {
  case InterfaceTaskFunction::itf_tgt_target_kernel:
    __tgt_target_kernel(task.loc, task.device_id, task.team_num,
                        task.thread_limit, task.host_ptr, &task.kernel_args);
    break;
  case InterfaceTaskFunction::itf_tgt_target_data_begin_mapper:
    __tgt_target_data_begin_mapper(
        task.loc, device_id, task.arg_num, task.args_base, task.args,
        task.arg_sizes, task.arg_types, task.arg_names, task.arg_mappers);
    break;
  case InterfaceTaskFunction::itf_tgt_target_data_end_mapper:
    __tgt_target_data_end_mapper(
        task.loc, device_id, task.arg_num, task.args_base, task.args,
        task.arg_sizes, task.arg_types, task.arg_names, task.arg_mappers);
    break;
  case InterfaceTaskFunction::itf_tgt_target_data_update_mapper:
    __tgt_target_data_update_mapper(
        task.loc, device_id, task.arg_num, task.args_base, task.args,
        task.arg_sizes, task.arg_types, task.arg_names, task.arg_mappers);
    break;
  }
}

void FTNotificationHandler::RestartFinishTask(const int task_id) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  const kmp_target_task_t *task = schedule_map->at(task_id).task;
  // Iterate over the successors
  for (const auto *succ = task->successors; succ != nullptr;
       succ = succ->next) {
    // If the task is not marked to reexecute, we do not need to check or add to
    // queue
    if (schedule_map->at(succ->task->task_id).state != TaskState::FAILED)
      continue;

    if (TaskIsReady(succ->task->task_id)) {
      std::lock_guard<std::mutex> lock(restart_queue_mtx);
      // Prevents a task from being added to the queue twice
      if (!restarted_list[succ->task->task_id]) {
        DP("[FT-Handler] - Adding task %d to the Restart Queue\n",
           succ->task->task_id);
        RestartEvalTask(succ->task->task_id);
        restart_queue.push(task_interface_map.at(succ->task->task_id));
        restarted_list[succ->task->task_id] = true;
      }
    }
  }
}

/// Checks if the task \p task_id is ready to execute. All dependencies should
/// be satisfied to a task be ready
bool FTNotificationHandler::TaskIsReady(int32_t task_id) {
  const kmp_target_task_t *task = schedule_map->at(task_id).task;
  if (cp_state == CPState::RUNNING) {
    for (int32_t i = 0; i < cp_new.dev_map.size(); i++) {
      if (cp_new.dev_state[i] == CPDevState::DONE)
        continue;

      for (auto &ptr_to_save : cp_new.dev_map[i].ptrs) {
        for (int32_t j = 0; j < task->target_data.num_args; j++) {
          if (ptr_to_save->hst_ptr == (uintptr_t)task->target_data.args_base[j])
            return false;
        }
      }
    }
    return true;
  }

  // Iterate over the predecessors, checking if all dependencies were satisfied
  for (const auto *pred = task->predecessors; pred != nullptr;
       pred = pred->next)
    if (schedule_map->at(pred->task->task_id).state != TaskState::EXECUTED &&
        schedule_map->at(pred->task->task_id).state != TaskState::SAVED)
      return false;

  return true;
}

// === Integration with OpenMP interface
// ===-----------------------------------------------------------------------===
/// Register the task arguments in the history, so we can re-execute in the
/// restart procedure.
int32_t FTNotificationHandler::TaskRegisterArgs(
    InterfaceTaskFunction function, ident_t *loc, int64_t device_id,
    void *host_ptr, int32_t arg_num, void **args_base, void **args,
    int64_t *arg_sizes, int64_t *arg_types, map_var_info_t *arg_names,
    void **arg_mappers, int32_t team_num, int32_t thread_limit,
    __tgt_kernel_arguments kernel_args) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  std::lock_guard<std::mutex> lock(internal_mtx);
  int32_t id = __kmpc_target_task_current_task_id();
  id = id < 0 ? custom_id_counter-- : id;
  task_interface_map[id] = (TaskInterfaceArgs){
      id,        function,    loc,      device_id,    host_ptr,
      arg_num,   args_base,   args,     arg_sizes,    arg_types,
      arg_names, arg_mappers, team_num, thread_limit, kernel_args};
  return id;
}

/// Decrements the running_tasks counter. If there is any FT procedure waiting
/// to start, and if this is the only task executing, notify the FT handler to
/// start the FT procedure
void FTNotificationHandler::TaskFinish(int32_t id) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  running_tasks--;
  DP("[FT-Handler] - Task %d finished - Running tasks: %d\n", id,
     running_tasks.load());

  // If there is a FT procedure waiting, notify the procedure to continue
  if (execution_state != ExecutionState::RUNNING && running_tasks == 0) {
    std::unique_lock<std::mutex> ift_lock(interface_ft_mtx);
    interface_ft_cv.notify_all();
    ift_lock.unlock();
  }

  if (cp_state == CPState::RUNNING) {
    CPEvalTask(id);
  }

  removeDeviceTaskMapEntry(id);
}

int32_t FTNotificationHandler::TaskGetCurrentTargetID() {
  int32_t thread_num = __kmpc_global_thread_num(nullptr);

  if (thread_task_map.size() <= thread_num ||
      thread_task_map[thread_num] == INVALID_TASK)
    return __kmpc_target_task_current_task_id();
  else
    return thread_task_map[thread_num];
}
