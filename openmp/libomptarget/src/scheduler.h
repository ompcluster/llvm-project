//===----------------- scheduler.h - Target-Task scheduler ----------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Scheduler used to manage multiple devices of the same type.
//
//===----------------------------------------------------------------------===//

#ifndef _OMPTARGET_SCHEDULER_H
#define _OMPTARGET_SCHEDULER_H

#include <cstdint>
#include <set>

#include <SourceInfo.h>

struct RTLInfoTy;
struct DeviceTy;

/// Allocate the scheduler.
void InitScheduler();

/// Deallocate the scheduler if it exists.
void DeinitScheduler();

// Schedule a target region among the many devices of the same type as DeviceID.
// Currently only used by the MPI plugin.
int Schedule(ident_t *Loc, int DeviceId);

std::set<int> getDataTaskSuccessors(int32_t task_id);

/// Pins the current task at a specific device.
///
/// \note this function does not exclusively allocate the device.
void PinTask(ident_t *Loc, int DeviceId);

// Mark the target task as executed and free the allocated device.
void MarkExecutedTask(int device_id);

// Mark the target task as failed and set ft handlers.
void MarkFailedTask(int32_t task_id, const DeviceTy &Device);

/// Marks a pinned task as executed.
///
/// \note since pinned tasks does not allocate devices, no free is needed.
void MarkExecutedPinnedTask(const DeviceTy &Device);

bool HasOutDep(int32_t task, int64_t addr);

int32_t GetCurrentTargetTaskID();

void reScheduleGraph(RTLInfoTy *RTL);

/// Get the outlined function ID of the current task.
const void *GetCurrentOutlinedFnID();

#endif // _OMPTARGET_SCHEDULER_H
