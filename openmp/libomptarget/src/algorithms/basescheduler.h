//===--------------- basescheduler.h - Target-Task scheduler --------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Definition of the base abstract scheduler.
//
//===----------------------------------------------------------------------===//

#ifndef _OMPTARGET_BASESCHEDULER_H
#define _OMPTARGET_BASESCHEDULER_H

#include "device.h"
#include "private.h"
#include "rtl.h"

#include <cassert>
#include <cstdint>
#include <cstdio>

#include <atomic>
#include <mutex>
#include <string>
#include <unordered_map>

#define assertm(exp, msg) assert(((void)msg, exp));

// Declaration of needed kmp.h structures and functions
// KEEP IN SYNC WITH ITS ORIGINAL DECLARATIONS
// =============================================================================
#ifdef __cplusplus
extern "C" {
#endif

typedef struct kmp_target_task_list_node kmp_target_task_list_node_t;
typedef struct kmp_target_task kmp_target_task_t;
typedef struct kmp_target_task_map kmp_target_task_map_t;
typedef struct kmp_target_dep kmp_target_dep_t;
typedef struct kmp_target_task_data kmp_target_task_data_t;

struct kmp_target_task_data {
  int64_t device_id;
  void *outlined_fn_id;
  int32_t num_args;
  void **args_base;
  void **args;
  int64_t *arg_sizes;
  int64_t *arg_types;
};

struct kmp_target_task_list_node {
  kmp_target_task_t *task;
  kmp_target_task_list_node_t *next;
};

struct kmp_target_dep {
  int64_t addr;
  struct {
    bool in : 1;
    bool out : 1;
  } dep_type;
};

struct kmp_target_task {
  int32_t task_id;
  bool is_target_task;
  kmp_target_task_data_t target_data;

  int32_t num_deps;
  kmp_target_dep_t *deps;

  kmp_target_task_list_node_t *successors;
  kmp_target_task_list_node_t *predecessors;
  int32_t num_predecessors;
};

struct kmp_target_task_map {
  kmp_target_task_t *tasks;
  int32_t num_tasks;

  kmp_target_task_t **roots;
  int32_t num_roots;
};

kmp_target_task_map_t *__kmpc_target_task_map_create() __attribute__((weak));

void __kmpc_target_task_map_free(kmp_target_task_map_t *task_map)
    __attribute__((weak));

kmp_target_task_t *__kmpc_target_task_map_get(kmp_target_task_map_t *task_map,
                                              int32_t task_id)
    __attribute__((weak));

bool __kmpc_is_graph_builder_tasking() __attribute__((weak));

int32_t __kmpc_global_thread_num(void *) __attribute__((weak));

int32_t __kmpc_omp_taskyield(void *loc_ref, int32_t gtid, int end_part)
    __attribute__((weak));

int32_t __kmpc_target_task_current_task_id() __attribute__((weak));

#ifdef __cplusplus
} // extern "C"
#endif

enum class NonTaskScheduler {
  STATIC, // Default strategy
  ROUNDROBIN
};

// OmpCluster Task Graph implementation
// =============================================================================
enum class TaskState {
  UNDEFINED = -1,
  CREATED,
  DEFERRED, // Only used by HEFTScheduler
  SCHEDULED,
  DISPATCHED,
  EXECUTED,
  FAILED,
  SAVED
};

struct TaskScheduleData {
  TaskState state;
  int rank;
  int thread;
  int translated_rank;
  const kmp_target_task_t *task;
  std::string source_location;
};

// Maps a task ID to its associated schedule data
using ScheduleMap = std::unordered_map<int32_t, TaskScheduleData>;

class BaseScheduler {
private:
  int remaining_target_tasks;

  // Info for non-nowait target round-robin scheduling.
  std::atomic<size_t> non_nowait_counter;

  NonTaskScheduler non_task_scheduler;

  // Internal scheduler mutex
  std::mutex scheduler_mtx;

protected:
  // Info for task-graph dumping.
  std::string graph_dump_dir_path;
  int graph_counter;

  ScheduleMap schedule_map;
  kmp_target_task_map_t *task_map;

public:
  BaseScheduler();
  virtual ~BaseScheduler();

  int32_t scheduleTask(int32_t task_id, const RTLInfoTy *device);
  void pinTask(int32_t task_id, int DeviceId);
  void setTaskState(int32_t task_id, TaskState new_state);
  void setTaskState(int32_t task_id, int device_id, TaskState new_state);
  void setTaskSourceLocation(int32_t task_id, ident_t *loc);

  kmp_target_dep getDep(int32_t task_id, int64_t addr);
  TaskScheduleData getTask(int task_id);

  void reScheduleGraph(const RTLInfoTy *device);

private:
  /// Wait for OpenMP to create its graph
  void waitOmpGraphCreation();
  /// Replace the current task graph with the newest one from OpenMP runtime.
  void acquireNewGraph();

public:
  /// Dump the task graph to a graphviz "dot" file.
  virtual void dumpTaskGraph();

private:
  /// Let subclasses define custom behavior after a new graph is acquired.
  virtual void acquireNewGraphPost() = 0;

  /// Schedule a task graph.
  virtual void scheduleGraph(const RTLInfoTy *device) = 0;

  /// Let subclasses define custom behavior before a task schedule is acquired.
  ///
  /// This function can be useful for schedulers that do not schedule the whole
  /// graph prior to the tasks execution or for the ones that modify the graph
  /// scheduling at task execution time.
  virtual void preScheduleTask(int32_t task_schedule,
                               const RTLInfoTy *device) = 0;

public:
  struct LockGuard {
    BaseScheduler *sch;

  public:
    LockGuard(BaseScheduler *sch) : sch(sch) { sch->scheduler_mtx.lock(); }
    ~LockGuard() { sch->scheduler_mtx.unlock(); }
  };
};

using TaskPtr = const kmp_target_task_t *;

namespace detail {

/// Return a string indicating which color an edge should be in the task graph
/// dump.
static inline const char *getEdgeColor(int32_t Rank1, int32_t Rank2) {
  if (Rank1 == -1 || Rank2 == -1)
    return "black";
  if (Rank1 == Rank2)
    return "blue";
  return "red";
}

} // namespace detail

// Task Utilitaries
// =============================================================================

static inline int32_t NumSuccessors(const kmp_target_task_t &task) {
  int32_t count = 0;
  for (auto *ptr = task.successors; ptr != nullptr; ptr = ptr->next)
    count++;
  return count;
}

static inline int32_t NumSuccessors(const kmp_target_task_t *task) {
  assert(task != nullptr);
  return NumSuccessors(*task);
}

static inline int32_t NumPredecessors(const kmp_target_task_t &task) {
  return task.num_predecessors;
}

static inline int32_t NumPredecessors(const kmp_target_task_t *task) {
  assert(task != nullptr);
  return NumPredecessors(*task);
}

// Task Predicates
// =============================================================================

/// Predicate that indicates if the task is a root task
static inline bool isRootTask(const kmp_target_task_t &task) {
  return task.predecessors == nullptr;
}

/// Predicate that indicates if the task is a root task
static inline bool isRootTask(TaskPtr task) {
  assert(task != nullptr);
  return isRootTask(*task);
}

/// Predicate that indicates if the task is an exit task
static inline bool isExitTask(const kmp_target_task_t &task) {
  return task.successors == nullptr;
}

/// Predicate that indicates if the task is an exit task
static inline bool isExitTask(TaskPtr task) {
  assert(task != nullptr);
  return isExitTask(*task);
}

/// Predicate that indicates if the task is a target data task
///
/// These tasks are inserted during codegen to manipulate data
/// (alloc/dealloc/copy) but does not properly execute an outlined function.
static inline bool isTargetDataTask(const kmp_target_task_t &task) {
  return task.is_target_task && task.target_data.outlined_fn_id == nullptr;
}

/// Predicate that indicates if the task is a target data task
static inline bool isTargetDataTask(TaskPtr task) {
  assert(task != nullptr);
  return isTargetDataTask(*task);
}

/// Predicate that indicates if the task is a classical task
static inline bool isClassicalTask(const kmp_target_task_t &task) {
  return !task.is_target_task;
}

/// Predicate that indicates if the task is a classical task
static inline bool isClassicalTask(TaskPtr task) {
  assert(task != nullptr);
  return isClassicalTask(*task);
}

/// Predicate that indicates if the task has an user-specified device
static inline bool hasSpecifiedDevice(const kmp_target_task_t &task) {
  return task.target_data.device_id >= 0;
}

/// Predicate that indicates if the task has an user-specified device
static inline bool hasSpecifiedDevice(TaskPtr task) {
  assert(task != nullptr);
  return hasSpecifiedDevice(*task);
}

// Task Iterators
// =============================================================================

/// A simple iterator for plain arrays.
///
/// This iterator works with both const and non-const pointers.
template <typename T> class RangeIterator {
public:
  RangeIterator(T *ptr, size_t size) : ptr(ptr), size(size) {}

  T *begin() { return &ptr[0]; }
  T *end() { return &ptr[size]; }

  const T *cbegin() const { return &ptr[0]; }
  const T *cend() const { return &ptr[size]; }

public:
  T *ptr;
  size_t size;
};

// Type aliases
using TaskIterator = RangeIterator<kmp_target_task_t>;
using RootTaskIterator = RangeIterator<kmp_target_task_t *>;

/// Create an iterator for the tasks in a task map.
static inline TaskIterator Tasks(kmp_target_task_map_t &TaskMap) {
  return {TaskMap.tasks, (size_t)TaskMap.num_tasks};
}

/// Create an iterator for the tasks in a task map.
static inline TaskIterator Tasks(kmp_target_task_map_t *TaskMap) {
  assert(TaskMap != nullptr);
  return Tasks(*TaskMap);
}

/// Create an iterator for the root tasks in a task map.
static inline RootTaskIterator Roots(kmp_target_task_map_t &TaskMap) {
  return {TaskMap.roots, (size_t)TaskMap.num_roots};
}

/// Create an iterator for the root tasks in a task map.
static inline RootTaskIterator Roots(kmp_target_task_map_t *TaskMap) {
  assert(TaskMap != nullptr);
  return Roots(*TaskMap);
}

/// An iterator for a linked list of tasks.
///
/// This class is just a front-end for the actual Iterator that is implemented
/// as a nested class. This class will be useful for iterating over
/// successors/predecessors of a task.
///
/// FIXME: Is there a way of not duplicating Iterator/ConstIterator?
class TaskListIterator {
public:
  TaskListIterator(kmp_target_task_list_node_t *node) : m_RootNode(node) {}

  // Foward declarations
  class Iterator;
  class ConstIterator;

  Iterator begin() { return {m_RootNode}; }
  Iterator end() { return {nullptr}; }

  ConstIterator cbegin() const { return {m_RootNode}; }
  ConstIterator cend() const { return {nullptr}; }

  /// Iterator for mutable list elements
  class Iterator {
  public:
    Iterator(kmp_target_task_list_node_t *node) : m_CurrentNode(node) {}

    /// Advance iterator to next element in the list
    Iterator &operator++() {
      if (m_CurrentNode)
        m_CurrentNode = m_CurrentNode->next;
      return *this;
    }

    /// Compare iterators
    bool operator!=(const Iterator &iter) {
      return m_CurrentNode != iter.m_CurrentNode;
    }

    /// Dereference iterator to obtain list element
    kmp_target_task_t &operator*() { return *m_CurrentNode->task; }

  private:
    /// Current node in the list
    kmp_target_task_list_node_t *m_CurrentNode;
  };

  /// Iterator for immutable list elements
  class ConstIterator {
  public:
    ConstIterator(kmp_target_task_list_node_t const *node)
        : m_CurrentNode(node) {}

    /// Advance iterator to next element in the list
    ConstIterator &operator++() {
      if (m_CurrentNode)
        m_CurrentNode = m_CurrentNode->next;
      return *this;
    }

    /// Compare iterators
    bool operator!=(const ConstIterator &iter) {
      return m_CurrentNode != iter.m_CurrentNode;
    }

    /// Dereference iterator to obtain list element
    const kmp_target_task_t &operator*() { return *m_CurrentNode->task; }

  private:
    /// Current node in the list
    kmp_target_task_list_node_t const *m_CurrentNode;
  };

private:
  /// Root node (first) of the list
  kmp_target_task_list_node_t *m_RootNode;
};

/// Create an iterator for the successors of a task.
static inline TaskListIterator Successors(const kmp_target_task_t &Task) {
  return TaskListIterator{Task.successors};
}

/// Create an iterator for the successors of a task.
static inline TaskListIterator Successors(const kmp_target_task_t *Task) {
  assert(Task != nullptr);
  return Successors(*Task);
}

/// Create an iterator for the predecessors of a task.
static inline TaskListIterator Predecessors(const kmp_target_task_t &Task) {
  return TaskListIterator{Task.predecessors};
}

/// Create an iterator for the predecessors of a task.
static inline TaskListIterator Predecessors(const kmp_target_task_t *Task) {
  assert(Task != nullptr);
  return Predecessors(*Task);
}

/// Sort the tasks in topological order and return a list.
///
/// \note This is an implementation of Kahn's algorithm.
/// \see https://en.wikipedia.org/wiki/Topological_sorting
std::list<TaskPtr> TopologicalOrder(kmp_target_task_map_t &TaskMap);

/// Sort the tasks in topological order and return a list.
std::list<TaskPtr> TopologicalOrder(kmp_target_task_map_t *TaskMap);

/// Sort the tasks in reverse topological order and return a list.
///
/// \see TopologicalOrder
std::list<TaskPtr> ReverseTopologicalOrder(kmp_target_task_map_t &TaskMap);

/// Sort the tasks in reverse topological order and return a list.
///
/// \see TopologicalOrder
std::list<TaskPtr> ReverseTopologicalOrder(kmp_target_task_map_t *TaskMap);

#endif /* _OMPTARGET_BASESCHEDULER_H */
