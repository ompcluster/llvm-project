//===------- BumpMemoryManager.h - Target independent memory manager ------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Target independent memory manager.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_OPENMP_LIBOMPTARGET_PLUGINS_COMMON_MEMORYMANAGER_BUMPMEMORYMANAGER_H
#define LLVM_OPENMP_LIBOMPTARGET_PLUGINS_COMMON_MEMORYMANAGER_BUMPMEMORYMANAGER_H

#include <map>
#include <mutex>
#include <set>
#include <unordered_map>
#include <vector>
#include <memory>

#include "MemoryManager.h"

struct SlabTy {
  void *PtrBegin;
  void *PtrEnd;
  void *Cur;
  const size_t Size;
  size_t RefCount;

  SlabTy(void *TgtPtr, size_t Size)
      : PtrBegin(TgtPtr), PtrEnd(reinterpret_cast<char *>(PtrBegin) + Size),
        Cur(PtrEnd), Size(Size), RefCount(0) {}
};

constexpr const size_t Alignment = 8;

class BumpMemoryManagerTy {
  using SlabPtrTy = std::shared_ptr<SlabTy>;
  using SlabTableTy = std::map<void *, SlabPtrTy>;

  /// A table mapping from the target pointer to its slab. The target pointer
  /// must be the \p PtrBegin of each slab.
  SlabTableTy PtrToSlabTable;
  /// The mutex for the map table
  std::mutex MapTableMtx;

  /// The reference to a device allocator
  DeviceAllocatorTy &DeviceAllocator;

  /// The threshold to manage memory using memory manager. If the request size
  /// is larger than \p SlabSize, the allocation will not be managed by the
  /// memory manager. By default it is 8MB.
  size_t SlabSize = 1U << 23;

  /// Request memory from target device
  void *allocateOnDevice(size_t Size, void *HstPtr) const {
    return DeviceAllocator.allocate(Size, HstPtr, TARGET_ALLOC_DEVICE);
  }

  /// Deallocate data on device
  int deleteOnDevice(void *Ptr) const { return DeviceAllocator.free(Ptr); }

  /// Create a new slab of size \p Size
  SlabPtrTy createNewSlab(size_t Size) {
    DP("BumpMemoryManagerTy::createNewSlab: alloc size %zu.\n", Size);
    void *TgtPtr = allocateOnDevice(Size, nullptr);
    if (TgtPtr == nullptr)
      return nullptr;
    return std::make_shared<SlabTy>(TgtPtr, Size);
  }

public:
  /// Constructor. If \p Threshold is non-zero, then the default threshold will
  /// be overwritten by \p Threshold.
  BumpMemoryManagerTy(DeviceAllocatorTy &DeviceAllocator, size_t Threshold = 0)
      : DeviceAllocator(DeviceAllocator) {
    if (Threshold)
      SlabSize = Threshold;
  }

  /// Destructor
  ~BumpMemoryManagerTy() {
    for (auto Slab : PtrToSlabTable)
      deleteOnDevice(Slab.first);
  }

  /// Allocate memory of size \p Size from target device. \p HstPtr is used to
  /// assist the allocation.
  void *allocate(size_t Size, void *HstPtr) {
    DP("BumpMemoryManagerTy::allocate: size %zu with host pointer " DPxMOD
       ".\n",
       Size, DPxPTR(HstPtr));

    // If the size is larger than SlabSize, we will allocate it from device
    // directly.
    // TODO: We might want something similar to the CustomSizedSlabs in
    // BumpPtrAllocatorImpl in LLVM. Or, we could use it as another slab when it
    // is freed.
    if (Size > SlabSize) {
      DP("%zu is greater than the SlabSize %zu. Allocate it directly from "
         "device\n",
         Size, SlabSize);
      return allocateOnDevice(Size, HstPtr);
    }

    // Add padding if needed
    Size += Size % Alignment;

    // Find an available slab
    {
      DP("Look for an existing slab\n");
      std::lock_guard<std::mutex> G(MapTableMtx);
      for (auto [Ptr, Slab] : PtrToSlabTable) {
        // If the size left in a slab is less than Size, we move to the next one
        if ((size_t)(reinterpret_cast<char *>(Slab->Cur) -
                     reinterpret_cast<char *>(Slab->PtrBegin)) < Size)
          continue;

        // Now we find one. Allocate it from the slab.
        Slab->Cur = reinterpret_cast<char *>(Slab->Cur) - Size;
        Slab->RefCount += 1;
        return Slab->Cur;
      }
    }

    // We cannot find a fitted slab. Create a new one
    SlabPtrTy Slab = createNewSlab(SlabSize);

    // Failed to create a new slab. Return nullptr directly.
    if (Slab == nullptr) {
      DP("Failed to create a new slab\n");
      return nullptr;
    }

    // We could manipulate it w/o any lock because it has not been inserted to
    // the table yet
    Slab->Cur = reinterpret_cast<char *>(Slab->Cur) - Size;
    Slab->RefCount += 1;

    void *TgtPtr = Slab->Cur;

    // Add the slab into map table
    {
      std::lock_guard<std::mutex> G(MapTableMtx);
      PtrToSlabTable[Slab->PtrBegin] = Slab;
    }

    return TgtPtr;
  }

  /// Deallocate memory pointed by \p TgtPtr
  int free(void *TgtPtr) {
    DP("BumpMemoryManagerTy::free: target memory " DPxMOD ".\n",
       DPxPTR(TgtPtr));
    // Find which slab Ptr is from
    {
      std::lock_guard<std::mutex> G(MapTableMtx);
      SlabTableTy::iterator Itr = PtrToSlabTable.upper_bound(TgtPtr);
      // PtrToSlabTable.upper_bound returns the first element that IS greater
      // than TgtPtr. Thus, the only time when the query is invalid is when it
      // returns the beginning of the container, since we cannot call std::prev
      // in this case to get the correct Slab. Besides, empty containers are
      // already covered by the check, since begin == end on those cases.
      if (Itr != PtrToSlabTable.begin()) {
        Itr = std::prev(Itr);
        SlabPtrTy Slab = Itr->second;

        // If Ptr is not in the range, it is not allocated from the slab
        if (TgtPtr >= Slab->PtrBegin && TgtPtr < Slab->PtrEnd) {
          size_t &RC = Slab->RefCount;
          RC -= 1;

          // This is not the last piece of the slab. Return directly.
          if (RC != 0)
            return OFFLOAD_SUCCESS;

          DP("The slab is now empty, let's remove it\n");

          // The last piece of the slab. Remove the slab from the map table.
          PtrToSlabTable.erase(Itr);
          // Set Ptr to the beginning of the slab.
          TgtPtr = Slab->PtrBegin;
        }
      }
    }

    // There are only two cases that we can reach this point:
    // 1. Ptr is not allocated from a slab;
    // 2. Ptr has been "free" in the slab, and it is the last piece in the slab,
    // so we need to free the slab. In this case, Ptr has already been set to
    // the beginning of the slab in the code block above.
    return deleteOnDevice(TgtPtr);
  }
};

#endif // LLVM_OPENMP_LIBOMPTARGET_PLUGINS_COMMON_MEMORYMANAGER_BUMPMEMORYMANAGER_H
