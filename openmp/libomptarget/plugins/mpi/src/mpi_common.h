//===--- mpi_common.h - Common MPI constants and functions --------------*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the declarations of all library macros, types,
// and functions.
//
//===----------------------------------------------------------------------===//

#ifndef __OMPTARGET_MPI_COMMON_H
#define __OMPTARGET_MPI_COMMON_H

#ifndef TARGET_NAME
#define TARGET_NAME MPI
#endif

#ifndef DEBUG_PREFIX
#define DEBUG_PREFIX "Target " GETNAME(TARGET_NAME) " RTL"
#endif

#include "Debug.h"

int WMPI_Init_thread(int *argc, char ***argv, int required);

// Debuggin
#ifdef OMPTARGET_DEBUG

#include <stdio.h>

struct DebugData {
  int debug_level;
  int world_rank;

  static DebugData &get();

  DebugData(DebugData &) = delete;
  void operator=(DebugData &) = delete;

private:
  DebugData();
};

#define DEBUG_SET_WORLD_RANK(rank)                                             \
  { DebugData::get().world_rank = rank; }

#define OMPC_DPP(prefix, ...)                                                  \
  {                                                                            \
    if (DebugData::get().debug_level) {                                        \
      fprintf(stderr, "%s --> ", prefix);                                      \
      fprintf(stderr, __VA_ARGS__);                                            \
    }                                                                          \
  }

#define OMPC_DP(...)                                                           \
  do {                                                                         \
    if (DebugData::get().world_rank == 0) {                                    \
      OMPC_DPP(GETNAME(DEBUG_PREFIX) " (Host)", __VA_ARGS__);                  \
    } else {                                                                   \
      OMPC_DPP(GETNAME(DEBUG_PREFIX) " (Device)", __VA_ARGS__);                \
    }                                                                          \
  } while (false)

#else // OMPTARGET_DEBUG

#define DEBUG_SET_WORLD_RANK(rank)                                             \
  {}

#define OMPC_DPP(prefix, ...)                                                  \
  {}

#define OMPC_DP(...)                                                                \
  {}

#endif // OMPTARGET_DEBUG

#endif // __OMPTARGET_MPI_COMMON_H
