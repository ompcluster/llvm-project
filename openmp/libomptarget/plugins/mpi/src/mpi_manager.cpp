//===------RTLs/mpi/src/rtl.cpp - Target RTLs Implementation - C++ ------*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// RTL for MPI machine
//
//===----------------------------------------------------------------------===//

#include "mpi_manager.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <dlfcn.h>
#include <ffi.h>
#include <gelf.h>
#include <link.h>
#include <list>
#include <numeric>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unistd.h>
#include <vector>

#include "llvm/Object/OffloadBinary.h"

#include "event_system.h"
#include "ft.h"
#include "omptargetplugin.h"
#include "profiler.h"

#ifndef TARGET_ELF_ID
#define TARGET_ELF_ID 0
#endif

#include "../../common/elf_common/elf_common.h"

#define OFFLOADSECTIONNAME "omp_offloading_entries"

// Check if broadcast events should be used by the MPI manager.
static bool isBcastEventEnabled() {
  constexpr std::array<const char *, 4> strategies = {
      "disabled", // Broadcast is disabled. Global data distribution is done
                  // solely by the memory management system. Default value.
      "p2p",      // Broadcast is enabled and implemented through p2p
                  // communications (SubmitEvent).
      "mpibcast", // Broadcast is enabled and implemented through MPI broadcast
                  // (BcastEvent).
      "dynamicbcast" // DynamicBcast
  };
  constexpr const char *default_strategy = "disabled";

  const char *env_str = getenv("OMPCLUSTER_BCAST_STRATEGY");
  std::string selected_mode = env_str != nullptr ? env_str : default_strategy;

  if (selected_mode == "disabled") {
    OMPC_DP("Broadcast disabled: data on synchronous target data map regions "
            "will be sent to each device when needed by the memory management "
            "system.\n");
  } else if (selected_mode == "p2p") {
    OMPC_DP("P2P Broadcast enabled: data on synchronous target data map "
            "regions will be sequentially sent to every device through "
            "peer-to-peer communication.\n");
  } else if (selected_mode == "mpibcast") {
    OMPC_DP("MPI Broadcast enabled: data on synchronous target data map "
            "regions will be sent to every device through MPI_Bcast.\n");
  } else if (selected_mode == "dynamicbcast") {
    OMPC_DP("Dynamic Broadcast enabled.\n");
  } else {
    selected_mode = default_strategy;

    std::string strats_str = "";
    for (const auto &strategy : strategies) {
      strats_str += " ";
      strats_str += strategy;
    }

    OMPC_DP("Invalid broadcast strategy. Defaulting to %s. Possible strategies "
            "are:%s.\n",
            default_strategy, strats_str.c_str());
  }

  // Broadcast events should be enabled only if "bcast" strategy is chosen.
  int value = (selected_mode == "mpibcast" || selected_mode == "dynamicbcast");
  return value;
}

void *MpiDeviceAllocatorTy::allocate(size_t size, void *hst_ptr,
                                     TargetAllocTy Kind) {
  if (size == 0)
    return nullptr;

  OMPC_DP("Allocating\n");

  int device_rank = device_id + 1; // rank 0 is the host

  uintptr_t device_ptr;
  auto event = event_system->createEvent<event_system::AllocEvent>(
      {device_rank}, size, &device_ptr);
  event->wait();

  if (event->getEventState() == event_system::EventState::FAILED)
    return hst_ptr;

  return (void *)device_ptr;
}

int MpiDeviceAllocatorTy::free(void *tgt_ptr) {
  int device_rank = device_id + 1; // rank 0 is the host

  auto event = event_system->createEvent<event_system::DeleteEvent>(
      {device_rank}, reinterpret_cast<uintptr_t>(tgt_ptr));
  event->wait();

  if (event->getEventState() == event_system::EventState::FAILED)
    return OFFLOAD_EVENT_FAILED;

  return OFFLOAD_SUCCESS;
}

RTLDeviceInfoTy::RTLDeviceInfoTy() {
  // Check and initialize MPI with desired thread support level
  WMPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE);

  target_comm = MPI_COMM_WORLD;

  // Get the rank of the process
  MPI_Comm_rank(target_comm, &world_rank);

  // Initialize profiler
  PROF_INIT_PROC(world_rank ? (std::string("Worker Process ") +
                               std::to_string(world_rank - 1))
                            : "Head Process",
                 world_rank);
  PROF_INIT_THD(world_rank ? "Gate Thread" : "Control Thread");

  // Initialize fault tolerance and event systems
  ft_handler = new ft::FaultTolerance(ft::DEFAULT_HB_TIME,
                                      ft::DEFAULT_HB_TIMEOUT, target_comm);

  event_system = new event_system::EventSystem(target_comm, this);

  fi_handler = new ft::FailureInjector(ft::DEFAULT_INJECTOR_INTERVAL, this);

  // Make FT visible to event system
  event_system->assignFT(ft_handler);

  if (world_rank == 0) {
    int world_size;
    MPI_Comm_size(target_comm, &world_size);
    number_workers = world_size - 1;

    func_gbl_entries.resize(number_workers);

    // Check if broadcast events are enabled
    use_bcast_event = isBcastEventEnabled();
  } else {
    // Second level of MPI offloading is not supported"
    number_workers = 0;

    func_gbl_entries.resize(1);
  }

  for (int i = 0; i < number_workers; ++i)
    device_allocators.emplace_back(i, event_system);

  // Get the size threshold from environment variable
  std::pair<size_t, bool> res = MemoryManagerTy::getSizeThresholdFromEnv();
  use_memory_manager = res.second;
  size_t memory_manager_threshold = res.first;

  if (use_memory_manager)
    for (int i = 0; i < number_workers; ++i)
      memory_managers.emplace_back(std::make_unique<BumpMemoryManagerTy>(
          device_allocators[i], memory_manager_threshold));
}

RTLDeviceInfoTy::~RTLDeviceInfoTy() {
  // Close dynamic libraries
  for (auto &lib : dyn_libs) {
    if (lib.handle) {
      dlclose(lib.handle);
      remove(lib.file_name);
    }
  }

  // Destruct memory managers
  for (auto &m : memory_managers)
    m.release();

  // Only send exit events from the host side
  if (world_rank == 0 && number_workers > 0) {
    OMPC_DP("Send an exit event to every child process\n");

    // TODO: refactor this event?
    std::vector<event_system::EventPtr> exit_events(number_workers);
    for (int rank = 1; rank <= number_workers; rank++) {
      exit_events[rank - 1] =
          event_system->createEvent<event_system::ExitEvent>({rank});
      exit_events[rank - 1]->progress();
    }

    for (int rank = 1; rank <= number_workers; rank++) {
      exit_events[rank - 1]->wait();
    }
  }

  // MPI_Barrier tries to repair to comm, if it succeed, the function will
  // return ft::FT_SUCCESS_NEW_COMM, meaning we should get the new comm.
  if (MPI_Barrier(target_comm) == ft::FT_SUCCESS_NEW_COMM) {
    if (ft_handler) {
      target_comm = ft_handler->getMainComm();
      if (event_system) {
        event_system->destroyLocalMPIContext();
        event_system->createLocalMPIContext(target_comm);
      }
    }
  }

  if (event_system)
    delete event_system;
  if (ft_handler)
    delete ft_handler;
  if (fi_handler)
    delete fi_handler;

  MPI_Finalize();

  PROF_DUMP_TRACE();
}

// Record entry point associated with device.
void RTLDeviceInfoTy::createOffloadTable(int32_t device_id,
                                         __tgt_offload_entry *begin,
                                         __tgt_offload_entry *end) {
  assert(device_id < (int32_t)func_gbl_entries.size() &&
         "Unexpected device id!");
  func_gbl_entries[device_id].emplace_back();
  FuncOrGblEntryTy &E = func_gbl_entries[device_id].back();

  E.table.EntriesBegin = begin;
  E.table.EntriesEnd = end;
}

// Return true if the entry is associated with device.
bool RTLDeviceInfoTy::findOffloadEntry(int32_t device_id, void *addr) {
  assert(device_id < (int32_t)func_gbl_entries.size() &&
         "Unexpected device id!");
  FuncOrGblEntryTy &E = func_gbl_entries[device_id].back();

  for (__tgt_offload_entry *i = E.table.EntriesBegin, *e = E.table.EntriesEnd;
       i < e; ++i) {
    if (i->addr == addr)
      return true;
  }

  return false;
}

// Return the pointer to the target entries table.
__tgt_target_table *RTLDeviceInfoTy::getOffloadEntriesTable(int32_t device_id) {
  assert(device_id < (int32_t)func_gbl_entries.size() &&
         "Unexpected device id!");
  FuncOrGblEntryTy &E = func_gbl_entries[device_id].back();
  return &E.table;
}

// Return the pointer to the target entries table.
__tgt_target_table *RTLDeviceInfoTy::getOffloadEntriesTableOnDevice() {
  return getOffloadEntriesTable(0);
}

// Check whether a given DeviceId is valid
bool RTLDeviceInfoTy::isValidDeviceId(const int device_id) const {
  return device_id >= 0 && device_id < number_workers;
}

int RTLDeviceInfoTy::getNumOfDevices() const { return number_workers; }

////////////////////////////////////////////////////////////////////////////////
/// Check whether is given binary is valid for the device
int32_t RTLDeviceInfoTy::isValidBinary(__tgt_device_image *image) {
// If we don't have a valid ELF ID we can just fail.
#if TARGET_ELF_ID < 1
  return 0;
#else
  return elf_check_machine(image, TARGET_ELF_ID);
#endif
}

static __tgt_device_image getExecutableImage(__tgt_device_image *Image) {
  llvm::StringRef ImageStr(static_cast<char *>(Image->ImageStart),
                           static_cast<char *>(Image->ImageEnd) -
                               static_cast<char *>(Image->ImageStart));
  auto BinaryOrErr =
      llvm::object::OffloadBinary::create(llvm::MemoryBufferRef(ImageStr, ""));
  if (!BinaryOrErr) {
    llvm::consumeError(BinaryOrErr.takeError());
    return *Image;
  }

  void *Begin = const_cast<void *>(
      static_cast<const void *>((*BinaryOrErr)->getImage().bytes_begin()));
  void *End = const_cast<void *>(
      static_cast<const void *>((*BinaryOrErr)->getImage().bytes_end()));

  return {Begin, End, Image->EntriesBegin, Image->EntriesEnd};
}

__tgt_target_table *
RTLDeviceInfoTy::loadBinary(const int device_id,
                            const __tgt_device_image *image) {
  size_t ImageSize = (size_t)image->ImageEnd - (size_t)image->ImageStart;
  size_t NumEntries = (size_t)(image->EntriesEnd - image->EntriesBegin);
  OMPC_DP("Expecting to have %zd entries defined.\n", NumEntries);

  // Is the library version incompatible with the header file?
  if (elf_version(EV_CURRENT) == EV_NONE) {
    OMPC_DP("Incompatible ELF library!\n");
    return NULL;
  }

  // Obtain elf handler
  Elf *e = elf_memory((char *)image->ImageStart, ImageSize);
  if (!e) {
    OMPC_DP("Unable to get ELF handle: %s!\n", elf_errmsg(-1));
    return NULL;
  }

  if (elf_kind(e) != ELF_K_ELF) {
    OMPC_DP("Invalid Elf kind!\n");
    elf_end(e);
    return NULL;
  }

  // Find the entries section offset
  Elf_Scn *section = 0;
  Elf64_Off entries_offset = 0;

  size_t shstrndx;

  if (elf_getshdrstrndx(e, &shstrndx)) {
    OMPC_DP("Unable to get ELF strings index!\n");
    elf_end(e);
    return NULL;
  }

  while ((section = elf_nextscn(e, section))) {
    GElf_Shdr hdr;
    gelf_getshdr(section, &hdr);

    if (!strcmp(elf_strptr(e, shstrndx, hdr.sh_name), OFFLOADSECTIONNAME)) {
      entries_offset = hdr.sh_addr;
      break;
    }
  }

  if (!entries_offset) {
    OMPC_DP("Entries Section Offset Not Found\n");
    elf_end(e);
    return NULL;
  }

  OMPC_DP("Offset of entries section is (" DPxMOD ").\n",
          DPxPTR(entries_offset));

  // load dynamic library and get the entry points. We use the dl library
  // to do the loading of the library, but we could do it directly to avoid
  // the dump to the temporary file.
  //
  // 1) Create tmp file with the library contents.
  // 2) Use dlopen to load the file and dlsym to retrieve the symbols.
  char tmp_name[] = "/tmp/tmpfile_XXXXXX";
  int tmp_fd = mkstemp(tmp_name);

  if (tmp_fd == -1) {
    elf_end(e);
    return NULL;
  }

  FILE *ftmp = fdopen(tmp_fd, "wb");

  if (!ftmp) {
    elf_end(e);
    return NULL;
  }

  fwrite(image->ImageStart, ImageSize, 1, ftmp);
  fclose(ftmp);

  DynLibTy Lib = {"", dlopen(tmp_name, RTLD_LAZY)};
  assert(sizeof(tmp_name) <= sizeof(Lib.file_name));
  strcpy(Lib.file_name, tmp_name);

  if (!Lib.handle) {
    OMPC_DP("Target library loading error: %s\n", dlerror());
    elf_end(e);
    return NULL;
  }

  dyn_libs.push_back(Lib);

  struct link_map *libInfo = (struct link_map *)Lib.handle;

  // The place where the entries info is loaded is the library base address
  // plus the offset determined from the ELF file.
  Elf64_Addr entries_addr = libInfo->l_addr + entries_offset;

  OMPC_DP("Pointer to first entry to be loaded is (" DPxMOD ").\n",
          DPxPTR(entries_addr));

  // Table of pointers to all the entries in the target.
  __tgt_offload_entry *entries_table = (__tgt_offload_entry *)entries_addr;

  __tgt_offload_entry *entries_begin = &entries_table[0];
  __tgt_offload_entry *entries_end = entries_begin + NumEntries;

  if (!entries_begin) {
    OMPC_DP("Can't obtain entries begin\n");
    elf_end(e);
    return NULL;
  }

  OMPC_DP("Entries table range is (" DPxMOD ")->(" DPxMOD ")\n",
          DPxPTR(entries_begin), DPxPTR(entries_end));
  createOffloadTable(device_id, entries_begin, entries_end);

  elf_end(e);

  return getOffloadEntriesTable(device_id);
}

__tgt_target_table *
RTLDeviceInfoTy::loadBinaryOnDevice(const __tgt_device_image *Image) {
  return loadBinary(0, Image);
}

////////////////////////////////////////////////////////////////////////////////
/// Register the shared library to the current device
void RTLDeviceInfoTy::registerLib(__tgt_bin_desc *desc) {
  // Register the images with the RTLs that understand them, if any.
  for (int32_t i = 0; i < desc->NumDeviceImages; ++i) {
    // Obtain the image.
    __tgt_device_image img = getExecutableImage(&desc->DeviceImages[i]);

    if (!isValidBinary(&img)) {
      OMPC_DP("Image " DPxMOD " is NOT compatible with this MPI device!\n",
              DPxPTR(img.ImageStart));
      continue;
    }

    OMPC_DP("Image " DPxMOD " is compatible with this MPI device!\n",
            DPxPTR(img.ImageStart));

    // 2) load image into the target table.
    loadBinaryOnDevice(&img);
  }

  OMPC_DP("Done registering entries!\n");
}

EventQueue *RTLDeviceInfoTy::getEventQueue(__tgt_async_info *async_info) {
  assert(async_info != nullptr);

  if (async_info->Queue == nullptr) {
    async_info->Queue = reinterpret_cast<void *>(new EventQueue);
  }

  return reinterpret_cast<EventQueue *>(async_info->Queue);
}

bool RTLDeviceInfoTy::addPtrDynBcast(int device_rank, int32_t device_id,
                                     void *tgt_ptr, void *hst_ptr,
                                     int64_t size) {
  // if there is a map, add the new device to the pointer
  // if there is no map, return false
  if (dynbcast_map.find(hst_ptr) != dynbcast_map.end()) {
    auto dyn_bcast = std::static_pointer_cast<event_system::DynBcastEvent>(
        dynbcast_map[hst_ptr]);
    dyn_bcast->addTgtPtr(device_rank, tgt_ptr);
    return true;
  } else {
    return false;
  }
}

bool RTLDeviceInfoTy::addPtrMPIBcast(int device_rank, int32_t device_id,
                                     void *tgt_ptr, void *hst_ptr,
                                     int64_t size) {
  if (mpibcast_map.find(hst_ptr) != mpibcast_map.end()) {
    auto mpi_bcast = std::static_pointer_cast<event_system::BcastEvent>(
        mpibcast_map[hst_ptr]);
    mpi_bcast->addTgtPtr(device_rank, tgt_ptr);
    return true;

  } else {
    return false;
  }
}

void RTLDeviceInfoTy::pushNewEvent(const event_system::EventPtr &event,
                                   int32_t device_id,
                                   __tgt_async_info *async_info) {
  EventQueue *queue = getEventQueue(async_info);

  PROF_SCOPED(PROF_LVL_USER, __FUNCTION__, [&]() -> std::string {
    std::stringstream details;
    details << "{";
    details << "\"mpi_tag\":" << event->mpi_tag << ",";
    details << "\"event_type\":\"" << toString(event->event_type) << "\",";
    details << "\"device_id\":" << device_id;
    details << "}";
    return details.str();
  }());

  // Syntax sugar to check if we can add a packable event to an existing
  // PackedEvent in queue. This can ben done if there is a PackedEvent in the
  // front of the queue with compatible event type and same destination rank.
  const auto canPackToLastEvent = [&] {
    if (queue->empty())
      return false;
    auto last_event = queue->back();

    if (last_event->dest_ranks != event->dest_ranks)
      return false;

    if (!event->is_packable)
      return false;

    auto packed_event =
        std::static_pointer_cast<event_system::PackedEvent>(last_event);

    if (packed_event->packedType() != event->event_type)
      return false;

    return true;
  };

  // If event is not packable, immediately add it to queue
  if (!event->is_packable || !event_system::PackedEvent::isPackingEnabled()) {
    queue->push_back(event);
  } else if (canPackToLastEvent()) {
    // Add to existing packed event if possible. Else create new packed event.
    // Safe to static cast since we already know that we can add this new event
    // to the previously added PackedEvent
    auto packed_event =
        std::static_pointer_cast<event_system::PackedEvent>(queue->back());
    packed_event->packEvent(event);
  } else {
    // create packed event
    auto packed_event = event_system->createPackedEvent(event);
    queue->push_back(packed_event);
  }
}

////////////////////////////////////////////////////////////////////////////////
/// Data management
void *RTLDeviceInfoTy::dataAlloc(int32_t device_id, int64_t size,
                                 void *hst_ptr) {
  if (use_memory_manager)
    return memory_managers[device_id]->allocate(size, hst_ptr);

  int device_rank = device_id + 1; // rank 0 is the host

  uintptr_t tgt_ptr;
  auto event = event_system->createEvent<event_system::AllocEvent>(
      {device_rank}, size, &tgt_ptr);
  event->wait();

  // In case of failed event, return the hst_ptr. Even if it is not valid on the
  // Device, the tasks related to this allocation will be skipped and restarted
  if (event->getEventState() == event_system::EventState::FAILED)
    return hst_ptr;

  return (void *)tgt_ptr;
}

int32_t RTLDeviceInfoTy::dataDelete(int32_t device_id, void *tgt_ptr) {
  int device_rank = device_id + 1; // rank 0 is the host

  if (ft_handler->getProcessState(device_rank) == ft::ProcessState::DEAD)
    return OFFLOAD_SUCCESS;

  if (use_memory_manager)
    return memory_managers[device_id]->free(tgt_ptr);

  auto event = event_system->createEvent<event_system::DeleteEvent>(
      {device_rank}, reinterpret_cast<uintptr_t>(tgt_ptr));
  event->wait();

  if (event->getEventState() == event_system::EventState::FAILED)
    return OFFLOAD_EVENT_FAILED;

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::dataRecovery(int32_t device_id, void *tgt_ptr,
                                      int64_t size, int32_t buffer_id,
                                      int32_t cp_rank, int32_t cp_ver) {
  __tgt_async_info async;

  int32_t rc = OFFLOAD_SUCCESS;
  rc |= dataRecoveryAsync(device_id, tgt_ptr, size, buffer_id, cp_rank, cp_ver,
                          &async);
  rc |= synchronize(device_id == -1 ? number_workers : device_id, &async);

  return rc;
}

int32_t RTLDeviceInfoTy::dataRecoveryAsync(int32_t device_id, void *tgt_ptr,
                                           int64_t size, int32_t buffer_id,
                                           int32_t cp_rank, int32_t cp_ver,
                                           __tgt_async_info *async_info) {
  if (async_info == nullptr) {
    return dataRecovery(device_id, tgt_ptr, size, buffer_id, cp_rank, cp_ver);
  }

  int device_rank = device_id + 1; // rank 0 is the host

  auto event = event_system->createEvent<event_system::RecoveryEvent>(
      {device_rank}, reinterpret_cast<uintptr_t>(tgt_ptr), size, buffer_id,
      cp_rank, cp_ver);
  pushNewEvent(event, device_id == -1 ? number_workers : device_id, async_info);

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::dataSubmit(int32_t device_id, void *tgt_ptr,
                                    void *hst_ptr, int64_t size) {
  __tgt_async_info async;

  int32_t rc = OFFLOAD_SUCCESS;
  rc |= dataSubmitAsync(device_id, tgt_ptr, hst_ptr, size, &async);
  rc |= synchronize(device_id, &async);

  return rc;
}

int32_t RTLDeviceInfoTy::dataSubmitAsync(int32_t device_id, void *tgt_ptr,
                                         void *hst_ptr, int64_t size,
                                         __tgt_async_info *async_info) {
  bool isBroadcast = [=]() {
    if (!use_bcast_event)
      return false;

    bool found = (std::find(hst_bcast_ptrs.begin(), hst_bcast_ptrs.end(),
                            hst_ptr) != hst_bcast_ptrs.end());

    OMPC_DP("Host pointer %p %s in broadcast list\n", hst_ptr,
            found ? "is" : "is not");

    return found;
  }();

  if (async_info == nullptr) {
    return dataSubmit(device_id, tgt_ptr, hst_ptr, size);
  }

  int device_rank = device_id + 1; // rank 0 is the host

  if (!isBroadcast) {
    auto event = event_system->createEvent<event_system::SubmitEvent>(
        {device_rank}, hst_ptr, tgt_ptr, size);
    pushNewEvent(event, device_id, async_info);

    return OFFLOAD_SUCCESS;
  }

  // Check if we are using dynbcast
  const std::string env_str = getenv("OMPCLUSTER_BCAST_STRATEGY");

  // DYNBCAST event
  if (env_str == "dynamicbcast") {
    // Quick hack to skip duplicates
    if (!addPtrDynBcast(device_rank, device_id, tgt_ptr, hst_ptr, size)) {
      auto event = event_system->createEvent<event_system::DynBcastEvent>(
          {device_rank}, hst_ptr, tgt_ptr, size);
      auto ev = std::static_pointer_cast<event_system::DynBcastEvent>(event);
      ev->addTgtPtr(device_rank, tgt_ptr);
      dynbcast_map[hst_ptr] = event;
      pushNewEvent(event, device_id, async_info);
    }

    return OFFLOAD_SUCCESS;
  }

  if (env_str == "mpibcast") {
    if (!addPtrMPIBcast(device_rank, device_id, tgt_ptr, hst_ptr, size)) {
      // MPI Broadcast have all process as destination
      int world_size = 0;
      MPI_Comm_size(MPI_COMM_WORLD, &world_size);
      std::vector<int> bcast_dests(world_size - 1);
      std::iota(bcast_dests.begin(), bcast_dests.end(), 1);

      auto event = event_system->createEvent<event_system::BcastEvent>(
          bcast_dests, hst_ptr, tgt_ptr, size);
      auto ev = std::static_pointer_cast<event_system::BcastEvent>(event);
      ev->addTgtPtr(device_rank, tgt_ptr);
      mpibcast_map[hst_ptr] = event;
      pushNewEvent(event, device_id, async_info);
    }
    return OFFLOAD_SUCCESS;
  }

  return OFFLOAD_FAIL;
}

int32_t RTLDeviceInfoTy::registerBcastPtr(int32_t arg_num, void **args_base,
                                          void **args, int64_t *arg_sizes,
                                          int64_t *arg_types, bool AddToList) {
  // If broadcast events are disabled, do nothing
  if (!use_bcast_event)
    return OFFLOAD_SUCCESS;

  for (int i = 0; i < arg_num; i++) {
    void *hst_ptr = args[i];
    if (AddToList) {
      OMPC_DP("Adding address %p to broadcast list\n", hst_ptr);
      hst_bcast_ptrs.insert(hst_bcast_ptrs.begin(), hst_ptr);
    } else {
      OMPC_DP("Removing address %p from broadcast list\n", hst_ptr);
      hst_bcast_ptrs.remove(hst_ptr);
    }
  }

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::dataRetrieve(int32_t device_id, void *hst_ptr,
                                      void *tgt_ptr, int64_t size) {
  __tgt_async_info async;

  int32_t rc = OFFLOAD_SUCCESS;
  rc |= dataRetrieveAsync(device_id, hst_ptr, tgt_ptr, size, &async);
  rc |= synchronize(device_id, &async);

  return rc;
}

int32_t RTLDeviceInfoTy::dataRetrieveAsync(int32_t device_id, void *hst_ptr,
                                           void *tgt_ptr, int64_t size,
                                           __tgt_async_info *async_info) {
  if (async_info == nullptr) {
    return dataRetrieve(device_id, hst_ptr, tgt_ptr, size);
  }

  int device_rank = device_id + 1; // rank 0 is the host

  auto event = event_system->createEvent<event_system::RetrieveEvent>(
      {device_rank}, hst_ptr, tgt_ptr, size);
  pushNewEvent(event, device_id, async_info);

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::dataExchange(int32_t src_id, void *src_ptr,
                                      int32_t dst_id, void *dst_ptr,
                                      int64_t size) {
  __tgt_async_info async;

  int32_t rc = OFFLOAD_SUCCESS;
  rc |= dataExchangeAsync(src_id, src_ptr, dst_id, dst_ptr, size, &async);
  rc |= synchronize(src_id, &async);

  return rc;
}

int32_t RTLDeviceInfoTy::dataExchangeAsync(int32_t src_id, void *src_ptr,
                                           int32_t dst_id, void *dst_ptr,
                                           int64_t size,
                                           __tgt_async_info *async_info) {
  if (async_info == nullptr) {
    return dataExchange(src_id, src_ptr, dst_id, dst_ptr, size);
  }

  // Add 1 since rank/device id 0 is the host.
  int32_t device_src_rank = src_id + 1;
  int32_t device_dst_rank = dst_id + 1;

  auto event = event_system->createEvent<event_system::ExchangeEvent>(
      {device_dst_rank, device_src_rank}, device_src_rank, device_dst_rank,
      src_ptr, dst_ptr, size);
  pushNewEvent(event, dst_id, async_info);

  return OFFLOAD_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////
/// Target execution
int32_t RTLDeviceInfoTy::runTargetTeamRegion(
    int32_t device_id, void *tgt_entry_ptr, void **tgt_args,
    ptrdiff_t *tgt_offsets, int32_t arg_num, int32_t team_num,
    int32_t thread_limit, uint64_t loop_tripcount /*not used*/) {
  __tgt_async_info async;

  int32_t rc = OFFLOAD_SUCCESS;
  rc |= runTargetTeamRegionAsync(device_id, tgt_entry_ptr, tgt_args,
                                 tgt_offsets, arg_num, team_num, thread_limit,
                                 loop_tripcount, &async);
  rc |= synchronize(device_id, &async);

  return rc;

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::runTargetTeamRegionAsync(
    int32_t device_id, void *tgt_entry_ptr, void **tgt_args,
    ptrdiff_t *tgt_offsets, int32_t arg_num, int32_t team_num,
    int32_t thread_limit, uint64_t loop_tripcount /*not used*/,
    __tgt_async_info *async_info) {
  if (async_info == nullptr) {
    return runTargetTeamRegion(device_id, tgt_entry_ptr, tgt_args, tgt_offsets,
                               arg_num, team_num, thread_limit, loop_tripcount);
  }

  // All args are references.
  std::vector<void *> ptrs(arg_num);

  PROF_BEGIN(PROF_LVL_ALL, "Preparing Pointers");
  // FIXME: Maybe be we should do this on the device
  for (int32_t i = 0; i < arg_num; ++i) {
    ptrs[i] = (void *)((intptr_t)tgt_args[i] + tgt_offsets[i]);
  }

  OMPC_DP("[MPI host] Running entry point at " DPxMOD "...\n",
          DPxPTR(tgt_entry_ptr));

  // get the translation table (which contains all the good info).
  __tgt_target_table *TargetTable = getOffloadEntriesTable(device_id);
  // iterate over all the host table entries to see if we can locate the
  // host_ptr.
  __tgt_offload_entry *begin = TargetTable->EntriesBegin;
  __tgt_offload_entry *end = TargetTable->EntriesEnd;
  __tgt_offload_entry *cur = begin;

  uint32_t tgt_entry_idx;

  for (uint32_t i = 0; cur < end; ++cur, ++i) {
    if (cur->addr != tgt_entry_ptr)
      continue;
    // we got a match, now fill the HostPtrToTableMap so that we
    // may avoid this search next time.
    OMPC_DP("[MPI host] Running kernel called %s...\n", cur->name);
    tgt_entry_idx = i;
    break;
  }
  PROF_END(PROF_LVL_ALL);

  PROF_BEGIN(PROF_LVL_ALL, "Event Creation");
  int device_rank = device_id + 1; // rank 0 is the host

  auto event = event_system->createEvent<event_system::ExecuteEvent>(
      {device_rank}, arg_num, ptrs.data(), tgt_entry_idx);
  pushNewEvent(event, device_id, async_info);
  PROF_END(PROF_LVL_ALL);

  return OFFLOAD_SUCCESS;
}

// Auxiliary function to manage DynBcast and PackedDynBcast events before
// execution
void DynBcastBookkeeping(
    EventQueue *queue, std::map<void *, event_system::EventPtr> dynbcast_map) {
  // Schedule every DynBcast in queue
  for (auto event : *queue) {
    // Run scheduleBcast for every DynBcast event in queue
    if (event->event_type == event_system::EventType::DYNBCAST) {
      auto dynBcast =
          std::static_pointer_cast<event_system::DynBcastEvent>(event);
      dynBcast->scheduleBcast();

      // Update DynBcast send list before execution
      dynBcast->dest_ranks =
          dynBcast->getSendList(dynBcast->fwd_list, dynBcast->orig_rank);

    } else if (event->event_type == event_system::EventType::PACKED_DYNBCAST) {
      auto dynBcast =
          std::static_pointer_cast<event_system::PackedDynBcastEvent>(event);
      dynBcast->updateSendList();
    }
  }

  // Clear DynBcast pointer map, since we are at an syncronization point
  dynbcast_map.clear();
}

int32_t RTLDeviceInfoTy::synchronize(int32_t device_id,
                                     __tgt_async_info *async_info,
                                     int32_t task_id) {

  // If no async info is given, just return successfully.
  if (async_info == nullptr || async_info->Queue == nullptr) {
    return OFFLOAD_SUCCESS;
  }

  // Acquire the async context.
  EventQueue *queue = getEventQueue(async_info);

  // Schedule dynbcast propagation for events in queue
  DynBcastBookkeeping(queue, dynbcast_map);
  mpibcast_map.clear();

  DP("Event queue (Task %d):\n", task_id);
  for (auto item : *queue) {
    DP("---- EventType = %s\n", event_system::toString(item->event_type));
  }

  int result = OFFLOAD_SUCCESS;
  for (auto &event : *queue) {
    // Check if the associated task was canceled due to re-execution
    if (failed_tasks_ids.size() > 0 && task_id != -1) {
      if (std::find(failed_tasks_ids.begin(), failed_tasks_ids.end(),
                    task_id) != failed_tasks_ids.end()) {
        result = OFFLOAD_EVENT_FAILED;
        DP("Task %d was canceled due to re-execution.\n", task_id);
        break;
      }
    }

    event->wait();

    // Check if the event failed
    if (event->getEventState() == event_system::EventState::FAILED) {
      result = OFFLOAD_EVENT_FAILED;
      DP("An event in the event queue failed. Canceling next Events.\n");
      break;
    }
  }

  // Delete the current async_info context. Further use of the same async_info
  // object must create a new context.
  delete queue;
  async_info->Queue = nullptr;

  return result;
}

////////////////////////////////////////////////////////////////////////////////
/// Start device main for worker ranks
int32_t RTLDeviceInfoTy::startDeviceMain(__tgt_bin_desc *desc) {
  // Check whether it is a device or not and if so run its initialization
  if (world_rank <= 0) {
    return OFFLOAD_SUCCESS;
  }

  OMPC_DP("Run main function of device\n");

  registerLib(desc);

  event_system->runGateThread();

  OMPC_DP("Do exit\n");

  // Will call the destructor of all static objects, thus all the MPI
  // finalization code will be called.
  exit(0);
}

// Synchronization event management
// ===========================================================================
int32_t RTLDeviceInfoTy::createEvent(int32_t ID, void **Event) {
  if (Event == nullptr) {
    REPORT("Received an invalid event pointer on createEvent\n");
    return OFFLOAD_FAIL;
  }

  auto e = new event_system::EventPtr;
  if (e == nullptr) {
    REPORT("Could not allocate a new synchronization event\n");
    return OFFLOAD_FAIL;
  }

  *Event = reinterpret_cast<void *>(e);

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::destroyEvent(int32_t ID, void *Event) {
  if (Event == nullptr) {
    REPORT("Received an invalid event pointer on destroyEvent\n");
    return OFFLOAD_FAIL;
  }

  delete reinterpret_cast<event_system::EventPtr *>(Event);

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::recordEvent(int32_t ID, void *Event,
                                     __tgt_async_info *AsyncInfo) {
  if (Event == nullptr) {
    REPORT("Received an invalid event pointer on recordEvent\n");
    return OFFLOAD_FAIL;
  }

  if (AsyncInfo == nullptr || AsyncInfo->Queue == nullptr) {
    REPORT("Received an invalid async queue on recordEvent\n");
    return OFFLOAD_FAIL;
  }

  EventQueue *queue = getEventQueue(AsyncInfo);
  if (queue->empty()) {
    OMPC_DP("Tried to record an event for an empty event queue\n");
    return OFFLOAD_SUCCESS;
  }

  // Copy the last event in the queue to the event handle.
  auto &e = *reinterpret_cast<event_system::EventPtr *>(Event);
  e = queue->back();

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::waitEvent(int32_t ID, void *Event,
                                   __tgt_async_info *AsyncInfo) {
  if (Event == nullptr) {
    REPORT("Received an invalid event pointer on waitEvent\n");
    return OFFLOAD_FAIL;
  }

  if (AsyncInfo == nullptr) {
    REPORT("Received an invalid async info on waitEvent\n");
    return OFFLOAD_FAIL;
  }

  auto &e = *reinterpret_cast<event_system::EventPtr *>(Event);
  if (!e) {
    OMPC_DP("Tried to wait an empty event\n");
    return OFFLOAD_SUCCESS;
  }

  // Create a wait event that waits for `Event` to be completed and add it to
  // the event queue. This ensures that the whole event queue where `Event`
  // originated is completed up to the `Event` itself. Directly waiting on
  // `Event` would execute it instead of waiting for its predecessors in its
  // original event queue.
  EventQueue *queue = getEventQueue(AsyncInfo);
  queue->push_back(std::make_shared<event_system::SyncEvent>(e));
  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::syncEvent(int32_t ID, void *Event) {
  if (Event == nullptr) {
    REPORT("Received an invalid event pointer on syncEvent\n");
    return OFFLOAD_FAIL;
  }

  auto &e = *reinterpret_cast<event_system::EventPtr *>(Event);
  if (!e) {
    OMPC_DP("Tried to synchronize an empty event\n");
    return OFFLOAD_SUCCESS;
  }

  // Create a wait event that waits for `Event` to be completed and executes it.
  // This ensures that the whole event queue where `Event` originated is
  // completed up to the `Event` itself. Directly waiting on `Event` would
  // execute it instead of waiting for its predecessors in its original event
  // queue.
  auto wait_event = std::make_shared<event_system::SyncEvent>(e);
  wait_event->wait();

  return OFFLOAD_SUCCESS;
}

// Fault tolerance management
// ===========================================================================
int32_t RTLDeviceInfoTy::registerFTCallback(void *callback) {
  DP("Registering FT notification systems\n");
  ft_handler->registerNotifyCallback((ft::FtCallbackTy)callback);

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::registerCPPtrs(int32_t device_id,
                                        uintptr_t *ptrs_to_register,
                                        int64_t *ptrs_size, int32_t num_ptrs) {
  __tgt_async_info async;

  int32_t rc = OFFLOAD_SUCCESS;
  rc |= registerCPPtrsAsync(device_id, ptrs_to_register, ptrs_size, num_ptrs,
                            &async);
  rc |= synchronize(device_id == -1 ? number_workers : device_id, &async);

  return rc;
}

int32_t RTLDeviceInfoTy::registerCPPtrsAsync(int32_t device_id,
                                             uintptr_t *ptrs_to_register,
                                             int64_t *ptrs_size,
                                             int32_t num_ptrs,
                                             __tgt_async_info *async_info) {
  if (async_info == nullptr) {
    return registerCPPtrs(device_id, ptrs_to_register, ptrs_size, num_ptrs);
  }

  int device_rank = device_id + 1;

  DP("Registering %d buffers to be saved by rank %d\n", num_ptrs, device_rank);

  auto event = event_system->createEvent<event_system::RegisterCPPtrs>(
      {device_rank}, ptrs_to_register, ptrs_size, num_ptrs);
  pushNewEvent(event, device_id == -1 ? number_workers : device_id, async_info);

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::requestCheckpoint(int32_t device_id,
                                           int32_t *version) {
  __tgt_async_info async;

  int32_t rc = OFFLOAD_SUCCESS;
  rc |= requestCheckpointAsync(device_id, version, &async);
  rc |= synchronize(device_id == -1 ? number_workers : device_id, &async);

  return rc;
}

int32_t RTLDeviceInfoTy::requestCheckpointAsync(int32_t device_id,
                                                int32_t *version,
                                                __tgt_async_info *async_info) {
  int device_rank = device_id + 1;

  // If it is a fake call, only call once in the host
  if (*version == -2) {
    if (device_rank == 0 /* host */)
      ft_handler->cpSaveCheckpoint(*version);
    return OFFLOAD_SUCCESS;
  }

  if (async_info == nullptr) {
    return requestCheckpoint(device_id, version);
  }

  // Add an event to execute checkpoint in every process
  auto event = event_system->createEvent<event_system::CheckpointEvent>(
      {device_rank}, version);
  pushNewEvent(event, device_id == -1 ? number_workers : device_id, async_info);

  return OFFLOAD_SUCCESS;
}

int32_t RTLDeviceInfoTy::registerFailedTasksId(int32_t number_of_tasks,
                                               int32_t *tasks_ids) {
  failed_tasks_ids.clear();
  failed_tasks_ids.resize(number_of_tasks);

  if (number_of_tasks != 0)
    std::copy_n(tasks_ids, number_of_tasks, failed_tasks_ids.begin());

  return OFFLOAD_SUCCESS;
}