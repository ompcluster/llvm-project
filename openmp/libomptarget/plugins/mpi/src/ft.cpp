//===------ ft.cpp - Common MPI fault tolerance implementations----------*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the implementation of heartbeat and functions related to
// fault tolerance.
//
// Heartbeat algorithm is based on the paper:
// Bosilca, George, et al. "A failure detector for HPC platforms." The
// International Journal of High Performance Computing Applications 32.1 (2018):
// 139-158.
//
//===----------------------------------------------------------------------===//

#include "ft.h"

#include <cassert>
#include <pthread.h>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <limits>
#include <random>
#include <sstream>

#include "mpi_common.h"
#include "mpi_manager.h"
#include "profiler.h"
#if COMPILE_WITH_VELOC
#include "veloc.h"
#endif

// Debug macros
#define FTDEBUG(msg_literal, ...)                                              \
  OMPC_DP("[Fault Tolerance] -> " msg_literal, ##__VA_ARGS__)

#define assertm(expr, msg) assert(((void)msg, expr));

// Static definitions for fault tolerance
// ===----------------------------------------------------------------------===
static ft::FaultTolerance *ft_handler = nullptr;
static RTLDeviceInfoTy *rtl_handler = nullptr;
static bool disable_ft = false;
static bool using_veloc = false;
static bool failed = false;
// TODO: Since asserts are temporary, the disabling option will be removed when
// checkpointing is integrated
static bool disable_wrappers = false;
static bool disable_wrappers_asserts = false;

// MPI wrappers
// These wrappers use --wrap compiler linkage option that redefines the MPI
// functions, calling a MPI function that was wrapped execute this version
// instead of MPI distribution version. Calling __real_MPI_...() will call the
// original function
// Asserts in case of result equals to ft::FT_ERROR are temporary
// ===----------------------------------------------------------------------===
/// MPI_Iwait replaces the MPI_Wait functions, as well as the regular MPI_Wait,
/// it waits until the /p request is complete. The additional parameter /p proc
/// represents the other side of the communication and is used to verify FT
/// parameters, it can be the rank of a process, or comm size if it is a
/// collective operation request. If /p proc equals to ft::FT_WAIT_LEGACY, it
/// will be executing regular MPI_Wait.
int MPI_Iwait(MPI_Request *request, MPI_Status *status, int proc) {
  if (disable_ft || ft_handler == nullptr || proc == ft::FT_WAIT_LEGACY || disable_wrappers) {
    return __real_MPI_Wait(request, status);
  }
  assertm((proc >= ft::FT_MPI_COLLECTIVE) && (proc < ft_handler->getSize()),
          "Waiting for request with invalid rank participating.");
  int test_flag = 0;
  while (!test_flag) {
    __real_MPI_Test(request, &test_flag, status);
    // Since some MPI distribution have some error related to failed processes
    // we always check if the other communication part failed
    if (proc == ft::FT_MPI_COLLECTIVE) {
      // If it is a collective call
      if (ft_handler->getCommState() != ft::CommState::VALID) {
        MPI_Request_free(request);
        if (!disable_wrappers_asserts)
          assertm(false, "MPI_Iwait could not complete a collective call");
        return ft::FT_ERROR;
      }
    } else {
      // If it is a point-to-point call
      if (ft_handler->getProcessState(proc) != ft::ProcessState::ALIVE) {
        MPI_Request_free(request);
        if (!disable_wrappers_asserts)
          assertm(false, "MPI_Iwait could not complete a p2p call");
        return ft::FT_ERROR;
      }
    }
  }
  return ft::FT_SUCCESS;
}

/// Warns user about the safety (in terms of fault tolerance) of using MPI_Wait
/// and redirects to the use of MPI_Iwait version
int __wrap_MPI_Wait(MPI_Request *request, MPI_Status *status) {
  if (disable_ft || ft_handler == nullptr || disable_wrappers) {
    return __real_MPI_Wait(request, status);
  }
  assertm(
      false,
      "Using MPI_Wait(MPI_Request *, MPI_Status *) is unsafe in terms of FT. "
      "Please use the MPI_Iwait(MPI_Request *, MPI_Status *, int) version with "
      "the last argument being the rank of the other side of communication or "
      "FT_MPI_COLLECTIVE if it is collective. If still wanting to call the "
      "original unsafe MPI_Wait use MPI_Iwait(request, status, "
      "ft::FT_WAIT_LEGACY)");
  return ft::FT_ERROR;
}

/// MPI_Itest replaces MPI_Test. The objective of this function is to return an
/// error if the other side of comm died. It is intended to be uses when the \p
/// flag is used as a loop condition. Which simulates the use of MPI_Wait
/// function.
int MPI_Itest(MPI_Request *request, int *flag, MPI_Status *status, int proc) {
  if (disable_ft || ft_handler == nullptr || proc == ft::FT_TEST_LEGACY || disable_wrappers) {
    return __real_MPI_Test(request, flag, status);
  }
  assertm((proc >= ft::FT_MPI_COLLECTIVE) && (proc < ft_handler->getSize()),
          "Waiting for request with invalid rank participating.");

  if (proc == ft::FT_MPI_COLLECTIVE) {
    // If it is a collective call
    if (ft_handler->getCommState() != ft::CommState::VALID) {
      MPI_Request_free(request);
      if (!disable_wrappers_asserts)
        assertm(false, "MPI_Iwait could not complete a collective call");
      return ft::FT_ERROR;
    }
  } else {
    // If it is a point-to-point call
    if (ft_handler->getProcessState(proc) != ft::ProcessState::ALIVE) {
      MPI_Request_free(request);
      if (!disable_wrappers_asserts)
        assertm(false, "MPI_Iwait could not complete a p2p call");
      return ft::FT_ERROR;
    }
  }

  // If there is no errors associated to the processes. return the real call
  __real_MPI_Test(request, flag, status);
  return ft::FT_SUCCESS;
}

/// Warns user about the safety (in terms of fault tolerance) of using MPI_Test
/// in case of using MPI_Test on loops
int __wrap_MPI_Test(MPI_Request *request, int *flag, MPI_Status *status) {
  if (disable_ft || ft_handler == nullptr || disable_wrappers) {
    return __real_MPI_Test(request, flag, status);
  }
  assertm(
      false,
      "Using MPI_Test(MPI_Request *, int *, MPI_Status *)  is unsafe in terms "
      "of FT if one is using MPI_Test on a waiting loop, since the destination "
      "can fail and not be reached, this function could not return from the "
      "loop if the loop condition is the flag (simulating a call to MPI_Wait). "
      "Please use the MPI_Itest(MPI_Request *, int *, MPI_Status *, int) "
      "version with the last argument being the rank of the other side of "
      "communication or FT_MPI_COLLECTIVE if it is collective. If still "
      "wanting to call the original not using its flag as a loop condition use "
      "MPI_Iwait(request, flag, status, ft::FT_TEST_LEGACY)");
  return ft::FT_ERROR;
}

/// Wraps MPI_Barrier by replacing the MPI_Barrier by the use of MPI_Ibarrier
/// with MPI_Iwait. Also tries to repair the communicator if it is invalid
int __wrap_MPI_Barrier(MPI_Comm comm) {
  if (failed) {
    return ft::FT_ERROR;
  }
  if (disable_ft || ft_handler == nullptr || disable_wrappers) {
    return __real_MPI_Barrier(comm);
  }
  int result = ft::FT_ERROR;
  // Assuming comm holds all processes
  MPI_Request barrier_req;
  if (ft_handler->getCommState() == ft::CommState::VALID) {
    // Although comm is valid, it is still possible that a failure occur, using
    // MPI_Ibarrier and MPI_Iwait prevents it from deadlocking if failure occur
    // in the middle of Barrier call
    MPI_Ibarrier(comm, &barrier_req);
    result = MPI_Iwait(&barrier_req, MPI_STATUS_IGNORE, ft::FT_MPI_COLLECTIVE);
  }
  // If comm is invalid or previous call to MPI_Ibarrier failed
  while (result == ft::FT_ERROR) {
    // If comm is not valid, let us return an error, but try to fix the comm and
    // call MPI_Ibarrier
    comm = ft_handler->requestCommRepair();
    assertm(comm != MPI_COMM_NULL,
            "MPI_Ibarrier was called within an invalid MPI Comm. FT library "
            "could not the reestablish comm.");
    MPI_Ibarrier(comm, &barrier_req);
    result = MPI_Iwait(&barrier_req, MPI_STATUS_IGNORE, ft::FT_MPI_COLLECTIVE);
    if (result == ft::FT_SUCCESS) {
      result = ft::FT_SUCCESS_NEW_COMM;
      if (ft_handler->getRank() == 0)
        FTDEBUG("[Rank %d FT] - MPI_Barrier was called within an invalid MPI "
                "Comm. Executing in a repaired comm\n",
                ft_handler->getRank());
    }
  }
  // At this point, user should check for the return value, and if it equals
  // to FT_ERROR, the user should call getMainComm() to get the repaired comm
  if (!disable_wrappers_asserts)
    assertm(result != ft::FT_ERROR, "MPI_Barrier could not complete");
  return result;
}

/// Verifies if it is possible (comm is valid) to execute MPI_Comm_free,
/// otherwise, it does not execute and return an error
int __wrap_MPI_Comm_free(MPI_Comm *comm) {
  if (disable_ft || ft_handler == nullptr || disable_wrappers) {
    return __real_MPI_Comm_free(comm);
  }
  // Assuming comm holds all processes
  if (ft_handler->getCommState() == ft::CommState::VALID && !failed) {
    __real_MPI_Comm_free(comm);
    return ft::FT_SUCCESS;
  } else {
    // Cannot free a comm without all processes in it but no need to repair it
    // either.
    if (ft_handler->getRank() == 0)
      FTDEBUG(
          "[Rank %d FT] - Could not free communicator with a failed process\n",
          ft_handler->getRank());
    return ft::FT_ERROR;
  }
}

/// Replaces the blocking send by Isend and wait for its completion, returns
/// error if the other side of communication failed or success if it completes
int __wrap_MPI_Send(const void *buf, int count, MPI_Datatype datatype, int dest,
                    int tag, MPI_Comm comm) {
  if (disable_ft || ft_handler == nullptr || disable_wrappers) {
    // If FT is disabled
    return __real_MPI_Send(buf, count, datatype, dest, tag, comm);
  }
  // Replaces regular Send by Isend
  MPI_Request w_send_request;
  MPI_Isend(buf, count, datatype, dest, tag, comm, &w_send_request);
  int result = MPI_Iwait(&w_send_request, MPI_STATUS_IGNORE, dest);
  if (result == ft::FT_ERROR) {
    MPI_Request_free(&w_send_request);
    if (!disable_wrappers_asserts)
      assertm(false, "MPI_Send could not complete");
  }
  return result;
}

/// Replaces the blocking Recv by a Probe-Recv function using the Mprobe wrapper
int __wrap_MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source,
                    int tag, MPI_Comm comm, MPI_Status *status) {
  if (disable_ft || ft_handler == nullptr || disable_wrappers) {
    // If FT is disabled
    return __real_MPI_Recv(buf, count, datatype, source, tag, comm, status);
  }
  // Keep probing until the message is received and stored in msg
  int probe_flag = 0;
  MPI_Message msg;
  MPI_Improbe(source, tag, comm, &probe_flag, &msg, status);
  while (!probe_flag) {
    if (ft_handler->getProcessState(source) != ft::ProcessState::ALIVE) {
      if (!disable_wrappers_asserts)
        assertm(false, "MPI_Recv could not complete the operation");
      return ft::FT_ERROR;
    }
    MPI_Improbe(source, tag, comm, &probe_flag, &msg, status);
  }
  
  MPI_Mrecv(buf, count, datatype, &msg, MPI_STATUS_IGNORE);
  return ft::FT_SUCCESS;
}

namespace ft {

/// Triggers whenever a MPI error occurs
void errorHandlerFunction(MPI_Comm *pcomm, int *perr, ...) {
  int len, ec;
  char errstr[MPI_MAX_ERROR_STRING];
  MPI_Error_string(*perr, errstr, &len);
  MPI_Error_class(*perr, &ec);
  FTDEBUG("Unhandled error!\n");
  FTDEBUG("Error: %s\n", errstr);
  FTDEBUG("Errorclass: %d\n", ec);
}

/// Init with \p global_comm as main comm, \p hb_time as heartbeat period and \p
/// susp_time as heartbeat suspect time
FaultTolerance::FaultTolerance(int hb_time, int susp_time,
                               MPI_Comm global_comm) {
  disable_ft = false;
  if (char *env_str = getenv("OMPCLUSTER_FT_DISABLE")) {
    if (std::stoi(env_str) == 1)
      disable_ft = true;
    if (disable_ft) {
      FTDEBUG("Disabling Fault Tolerance feature\n");
      return;
    }
  }

  main_comm = global_comm;

  MPI_Comm_dup(main_comm, &hb_comm);
  MPI_Comm_rank(hb_comm, &rank);
  MPI_Comm_size(hb_comm, &size);
  MPI_Comm_size(main_comm, &app_size);

  ft_handler = this;

  eta = hb_time / DEFAULT_HB_TIME_STEP;
  delta = susp_time / DEFAULT_HB_TIME_STEP;
  time_step = DEFAULT_HB_TIME_STEP;
  hb_done = false;

  setErrorHandling();
  exec_state = ExecState::RUNNING;

  // Checkpointing start procedures, check if using veloc
  if (char *env_str = getenv("OMPCLUSTER_CP_USEVELOC")) {
    if (std::stoi(env_str) == 1) {
      using_veloc = true;
      // OMPCLUSTER_CP_EXECCFG represents application cfg file location
      if (char *exec_file = getenv("OMPCLUSTER_CP_EXECCFG")) {
        cp_cfg_file_path = std::string(exec_file);
        cpInit(exec_file);
      } else {
        // OMPCLUSTER_CP_TESTCFG is the cfg file location for llvm lit test
        if (char *test_file = getenv("OMPCLUSTER_CP_TESTCFG")) {
          cp_cfg_file_path = std::string(test_file);
          cpInit(test_file);
        } else {
          cp_cfg_file_path = std::string("veloc.cfg");
          cpInit("veloc.cfg");
        }
      }
    }
  }

  if (char *env_str = getenv("OMPCLUSTER_FT_WRAPPERS_DISABLE")) {
    if (std::stoi(env_str) == 1) {
      disable_wrappers = true;
      FTDEBUG("Disabling Fault Tolerance wrappers\n");
    }
  }

  if (char *env_str = getenv("OMPCLUSTER_FT_WRAPPERS_ASSERTS")) {
    if (std::stoi(env_str) == 0) {
      disable_wrappers_asserts = true;
      FTDEBUG("Disabling Fault Tolerance Wrappers asserts\n");
    }
  }

  // Start heartbeat threads only if there are more than one process
  if (size > 1) {
    hb_started_mutex.lock();
    hb_started = false;
    hb_started_mutex.unlock();
    heartbeat = std::thread(&FaultTolerance::hbMain, this);
    // Wait for HB to be completely started
    std::unique_lock<std::mutex> lock(hb_started_mutex);
    hb_started_cv.wait(lock, [&] { return hb_started; });
    hb_started_mutex.unlock();
    // Start failure handling thread
    failure_thread = std::thread(&FaultTolerance::failureHandler, this);
  } else {
    // In case of not using HB, start structures so wrappers can use
    c_state = ft::CommState::VALID;
    if (size > 0) {
      p_states = new std::atomic<ProcessState>[size];
      p_states[0] = ft::ProcessState::ALIVE; // there is only one process
    }
  }
}

/// Finishes the main loop of heartbeat thread and synchronize with main thread
FaultTolerance::~FaultTolerance() {
  if (disable_ft)
    return;

  // Finish heartbeat
  std::unique_lock<std::mutex> lock_hb(hb_finished_mutex);
  hb_done = true;
  hb_finished_cv.notify_all();
  lock_hb.unlock();
  if (heartbeat.joinable()) 
    heartbeat.join();

  // End failure handling thread. Ending the program while recovering from
  // failure is not possible
  std::unique_lock<std::mutex> lock_fh(fh_mutex);
  fh_instruction = FHInstruction::FINISH;
  fh_cv.notify_all();
  lock_fh.unlock();
  if (failure_thread.joinable())
    failure_thread.join();
  DP("[Rank %d] - Finished all HB related threads\n", rank);

  // End checkpoint operations
  cpEnd();
}

// MPI related functions
// ===----------------------------------------------------------------------===
/// Non-blocking MPI wrapper for probe and receive specific message
template <typename T, typename S, typename R>
int FaultTolerance::MPIw_IProbeRecv(T *buffer, int size, S type, int source,
                                    int tag, MPI_Comm comm, R status) {
  msg_recv_flag = 0;
  MPI_Improbe(source, tag, comm, &msg_recv_flag, &msg_recv, MPI_STATUS_IGNORE);
  if (msg_recv_flag) {
    MPI_Mrecv(buffer, size, type, &msg_recv, status);
    return 0;
  }
  return 1;
}

// Heartbeat related functions
// ===----------------------------------------------------------------------===
/// Configures all necessary variables to start the heartbeat
void FaultTolerance::hbInit() {
  hb_need_repair = false;
  c_state = CommState::VALID;

  // Check if there are FT environment variables
  if (char *env_str = getenv("OMPCLUSTER_HB_TIMESTEP")) {
    time_step = std::stoi(env_str);
    FTDEBUG(
        "[Rank %d FT] Using HB timestep defined in environment variables: %d\n",
        rank, time_step);
  }
  // The timeout represents the suspect time, must be multiple of TIMESTEP
  if (char *env_str = getenv("OMPCLUSTER_HB_TIMEOUT")) {
    assertm((std::stoi(env_str) % time_step) == 0,
            "Timeout must be multiple of Timestep.");
    delta = std::stoi(env_str) / time_step;
    FTDEBUG(
        "[Rank %d FT] Using HB timeout defined in environment variables: %d\n",
        rank, std::stoi(env_str));
  }
  // The period represents heartbeat period, must be multiple of TIMESTEP
  if (char *env_str = getenv("OMPCLUSTER_HB_PERIOD")) {
    assertm((std::stoi(env_str) % time_step) == 0,
            "Period must be multiple of Timestep.");
    eta = std::stoi(env_str) / time_step;
    FTDEBUG(
        "[Rank %d FT] Using HB period defined in environment variables: %d\n",
        rank, std::stoi(env_str));
  }

  // Start heartbeat period and suspect time counters
  delta_to = delta;
  eta_to = eta;

  // Set all nodes as neighbors alive state
  p_states = new std::atomic<ProcessState>[size];
  for (int i = 0; i < size; i++) {
    neighbors.push_back(i);
    p_states[i] = ProcessState::ALIVE;
  }

  // Shuffle and find emitter and observer
  shuffle(neighbors.begin(), neighbors.end(),
          std::default_random_engine(size * size));
  for (std::size_t i = 0; i < neighbors.size(); i++) {
    if (rank == neighbors[i]) {
      n_pos = i;
      emitter = neighbors[(i == 0) ? size - 1 : i - 1];
      observer = neighbors[(i + 1) % size];
      break;
    }
  }
  // Extend delta_to 5 times if the rank observed is 0, since rank 0 has no
  // recovery procedures.
  if (emitter == 0)
    delta_to *= 5;

  // Init CP structures
  cp_completed_ranks.resize(size, false);
  cp_started_ranks.resize(size, false);

  FTDEBUG(
      "[Rank %d FT] Heartbeat initialized with emitter %d and observer %d\n",
      rank, emitter, observer);

  // Notifies the constructor the heartbeat finished starting
  std::unique_lock<std::mutex> lock(hb_started_mutex);
  hb_started = true;
  hb_started_cv.notify_all();
  lock.unlock();
}

/// Heartbeat thread main loop function
void FaultTolerance::hbMain() {
  PROF_INIT_THD("Heartbeat Thread", std::numeric_limits<short>::max() + 1);
  thread_id = std::this_thread::get_id();
  FTDEBUG("[Rank %d FT] Started rank %d of %d heartbeat thread\n", rank, rank,
          size - 1);
  hbInit();
  int flag;
  MPI_Status status;
  while (!hb_done) {
    if (hb_need_repair) {
      // Waits for the user thread notifies that repair is complete
      std::unique_lock<std::mutex> lock(hb_need_repair_mutex);
      hb_need_repair_cv.wait(lock, [&] { return !hb_need_repair; });
      hb_need_repair_mutex.unlock();
    }

    // Send an alive message to observer if heartbeat period has passed
    if (eta_to <= 0)
      hbSendAlive();
    else
      eta_to--;

    // If received an alive message from emitter, reset the suspect timeout
    flag = MPIw_IProbeRecv(&hb_message, 1, MPI_INT, MPI_ANY_SOURCE, TAG_HB_ALIVE,
                           hb_comm, &status);
    if (!flag) {
      if (hb_message == emitter) {
        hbResetObsTimeout();
      } else {
        // If sender isn't in neighbors, means that is a false positive
        auto known = std::find(neighbors.begin(), neighbors.end(), hb_message);
        if (known == neighbors.end()) {
          // Gives extra time before suspecting the emitter
          delta_to = 2 * delta;
          // Resets current emitter observer
          new_obs_msg = hb_message;
          MPI_Isend(&new_obs_msg, 1, MPI_INT, emitter, TAG_HB_NEWOBS, hb_comm,
                    &send_request);
          MPI_Request_free(&send_request);
          // Resets current emitter to the old one
          emitter = hb_message;
          // Broadcast the inclusion of the false positive
          hbBroadcast(HB_BC_ALIVE, emitter, n_pos, 0);
        }
      }
      FTDEBUG("[Rank %d FT] Received alive message from %d\n", rank, hb_message);
    }

    // If no alive messages were received before suspect time, it is a failure
    if (delta_to <= 0)
      hbFindDeadNode();
    else
      delta_to--;

    // Check if there is a new observer, occurs when the current observer fails
    flag = MPIw_IProbeRecv(&hb_message, 1, MPI_INT, MPI_ANY_SOURCE,
                           TAG_HB_NEWOBS, hb_comm, &status);
    if (!flag) {
      hbSetNewObs(hb_message);
      FTDEBUG("[Rank %d FT] %d is the new observer\n", rank, hb_message);
    }

    // Check if received a broadcast of failed node
    flag = MPIw_IProbeRecv(bc_message_recv, 4, MPI_INT, MPI_ANY_SOURCE,
                           TAG_HB_BCAST, hb_comm, &status);
    if (!flag) {
      auto valid_source =
          std::find(neighbors.begin(), neighbors.end(), status.MPI_SOURCE);
      switch (bc_message_recv[0]) {
      case HB_BC_FAILURE: {
        auto dead_process =
            std::find(neighbors.begin(), neighbors.end(), bc_message_recv[1]);
        // Only replicates if failure is unknown and from valid source
        if (dead_process != neighbors.end() &&
            valid_source != neighbors.end()) {
          hbBroadcast(HB_BC_FAILURE, bc_message_recv[1], bc_message_recv[2], 0);
          FTDEBUG("[Rank %d FT] Received broadcast with failed node %d at "
                  "position %d\n",
                  rank, bc_message_recv[1], bc_message_recv[2]);
        }
      } break;
      case HB_BC_ALIVE: {
        auto alive_process =
            std::find(neighbors.begin(), neighbors.end(), bc_message_recv[1]);
        // Only replicates if process is not added in neighbors yet
        if (alive_process == neighbors.end() &&
            valid_source != neighbors.end()) {
          hbBroadcast(HB_BC_ALIVE, bc_message_recv[1], bc_message_recv[2], 0);
          FTDEBUG("[Rank %d FT] Received broadcast of false positive %d to be "
                  "added in position %d\n",
                  rank, bc_message_recv[1], bc_message_recv[1]);
        }
      } break;
      case HB_BC_REPAIR: {
        if (c_state == CommState::INVALID && valid_source != neighbors.end()) {
          if (hb_need_repair == false) {
            hbBroadcast(HB_BC_REPAIR, bc_message_recv[1], bc_message_recv[2],
                        0);
            FTDEBUG("[Rank %d FT] Received broadcast of repair operation. "
                    "Check sum is %d and current size is %d\n",
                    rank, bc_message_recv[1], bc_message_recv[1]);
          }
        }
      } break;
      case CP_START: {
        if (exec_state != ExecState::RESTARTING &&
            cp_started_ranks[bc_message_recv[1]] == false &&
            bc_message_recv[2] > cp_count) {
          hbBroadcast(CP_START, bc_message_recv[1], bc_message_recv[2], 0);
          FTDEBUG("[Rank %d FT] Received broadcast of checkpoint start: %d\n",
                  rank, bc_message_recv[1]);
        }
      } break;
      case CP_COMPLETED: {
        if (exec_state == ExecState::CHECKPOINTING &&
            cp_completed_ranks[bc_message_recv[1]] == false &&
            bc_message_recv[2] > cp_count) {
          hbBroadcast(CP_COMPLETED, bc_message_recv[1], bc_message_recv[2],
                      bc_message_recv[3]);
          FTDEBUG(
              "[Rank %d FT] Received broadcast of checkpoint completion: %d\n",
              rank, bc_message_recv[1]);
        }
      } break;
      case CP_CANCELED: {
        if (bc_message_recv[2] > cp_count) {
          hbBroadcast(CP_CANCELED, bc_message_recv[1], bc_message_recv[2], 0);
          FTDEBUG(
              "[Rank %d FT] Received broadcast of checkpoint cancelation: %d\n",
              rank, bc_message_recv[1]);
        }
      } break;
      default:
        FTDEBUG("[Rank %d FT] Ignoring unknown broadcast\n", rank);
        break;
      }
    }
    // Heartbeat thread time step
    std::unique_lock<std::mutex> hb_lock(hb_finished_mutex);
    if (hb_finished_cv.wait_for(hb_lock, std::chrono::milliseconds(time_step),
                                [&] { return hb_done == true; }))
      break;
  }
}

/// Sends a message to the observer saying it is alive
void FaultTolerance::hbSendAlive() {
  eta_to = eta;
  alive_msg = rank;
  MPI_Isend(&alive_msg, 1, MPI_INT, observer, TAG_HB_ALIVE, hb_comm,
            &send_request);
  MPI_Request_free(&send_request);
}

/// Resets suspect time upon receiving alive messages from emitter
void FaultTolerance::hbResetObsTimeout() {
  delta_to = delta;
  // Extend delta_to 5 times if the rank observed is 0, since rank 0 has no
  // recovery procedures.
  if (emitter == 0)
    delta_to *= 5;
}

/// Do all the necessary procedures after noticing the emitter failed
void FaultTolerance::hbFindDeadNode() {
  // Gives extra time before suspecting the emitter
  delta_to = 2 * delta;

  assertm(emitter != 0, "Fatal Error: Head process failed");
  FTDEBUG("[Rank %d FT] Found a failed process: %d\n", rank, emitter);

  // Broadcast failure to other nodes
  int d_pos = (n_pos == 0) ? size - 1 : n_pos - 1;
  hbBroadcast(HB_BC_FAILURE, emitter, d_pos, 0);

  // Find a new emitter and send a message saying its new observer
  emitter = hbFindEmitter();
  new_obs_msg = rank;
  MPI_Isend(&new_obs_msg, 1, MPI_INT, emitter, TAG_HB_NEWOBS, hb_comm,
            &send_request);
  MPI_Request_free(&send_request);
  // Extend delta_to 5 times if the emitter is 0, since rank 0 has no recovery
  // procedures.
  if (emitter == 0)
    delta_to *= 5;
  FTDEBUG("[Rank %d FT] New emitter is: %d\n", rank, emitter);
}

/// Look backwards in the ring to find a new emitter
int FaultTolerance::hbFindEmitter() {
  // Since failed node was already removed from neighbors
  int new_emitter = (n_pos == 0) ? size - 1 : n_pos - 1;
  return neighbors[new_emitter];
}

/// Set the new observer after the previous one failed
void FaultTolerance::hbSetNewObs(int new_obs) {
  observer = new_obs;
  // Sends an alive message to new observer in the next heartbeat iteration
  eta_to = 0;
  FTDEBUG("[Rank %d FT] New observer is: %d\n", rank, observer);
}

/// Internal broadcast for heartbeat
void FaultTolerance::hbBroadcast(int type, int first_value, int second_value,
                                 int third_value) {
  bc_message[0] = type;
  bc_message[1] = first_value;
  bc_message[2] = second_value;
  bc_message[3] = third_value;

  switch (type) {
  case HB_BC_FAILURE: {
    // Updates states of the process and MPI communicators
    c_state = CommState::INVALID;
    p_states[first_value] = ProcessState::DEAD;
    cp_completed_ranks[first_value] = true;
    cp_started_ranks[first_value] = true;
    // Tell Failure Handling thread about the FAILURE
    std::unique_lock<std::mutex> lock(fh_mutex);
    failed_rank = first_value;
    fh_instruction = FHInstruction::FAILURE;
    fh_cv.notify_all();
    lock.unlock();
    // Removes failed node from neighbors
    if (second_value < neighbors.size()) {
      neighbors.erase(neighbors.begin() + second_value);
      // Update current position if necessary
      if (second_value < n_pos)
        n_pos--;
      // Update size of the ring
      size--;
    }
    FTDEBUG("[Rank %d FT] Broadcasting failure\n", rank);
  } break;
  case HB_BC_ALIVE: {
    p_states[first_value] = ProcessState::ALIVE;
    cp_completed_ranks[first_value] = false;
    cp_started_ranks[first_value] = false;
    // Validate comm again if the false positive was the only dead process
    c_state = CommState::VALID;
    for (int i = 0; i < size; i++) {
      if (p_states[i] == ProcessState::DEAD) {
        c_state = CommState::INVALID;
        break;
      }
    }
    // Tell FailureHandling thread about the False Positive
    std::unique_lock<std::mutex> lock(fh_mutex);
    fh_instruction = FHInstruction::FALSE_POSITIVE;
    failed_rank = first_value;
    fh_cv.notify_all();
    lock.unlock();

    // Adds false positive to neighbors again and fix current position
    neighbors.insert(neighbors.begin() + second_value, first_value);
    if (second_value <= n_pos)
      n_pos++;
    size++;
    FTDEBUG("[Rank %d FT] Broadcasting false positive\n", rank);
  } break;
  case HB_BC_REPAIR: {
    int sum = 0;
    for (std::size_t i = 0; i < neighbors.size(); i += 2)
      sum += neighbors[i] * ((i < neighbors.size()) ? neighbors[i + 1] : 1);

    // Sum and size must be the same as received in broadcast
    assertm((sum == first_value && (int)neighbors.size() == second_value),
            "Inconsistency between alive groups, can't repair comm.\n");

    // This notifies the requestCommRepair waiting cv
    std::unique_lock<std::mutex> lock(hb_need_repair_mutex);
    hb_need_repair = true;
    hb_need_repair_cv.notify_all();
    lock.unlock();

    FTDEBUG("[Rank %d FT] Broadcasting repair operation\n", rank);
  } break;
  case CP_START: {
    cp_started_ranks[first_value] = true;

    if (not std::all_of(cp_started_ranks.begin(), cp_started_ranks.end(),
                        [&](bool started) { return started == true; }))
      break;

    FTDEBUG("[Rank %d FT] - Started CP (version: %d)\n", rank, second_value);
    // Tell CP thread to start checkpointing
    std::unique_lock<std::mutex> lock_cp(cp_mutex);
    cp_instruction = CPInstruction::EXEC_CP;
    cp_cv.notify_all();
    lock_cp.unlock();
  } break;
  case CP_COMPLETED: {
    cp_completed_ranks[first_value] = true;
    cp_cost_sum = cp_cost_sum > third_value ? cp_cost_sum : third_value;

    if (not std::all_of(cp_completed_ranks.begin(), cp_completed_ranks.end(),
                        [&](bool finished) { return finished == true; }))
      break;

    FTDEBUG("[Rank %d FT] - Finished CP (version %d)\n", rank, second_value);

    // Redefine cp structures and notify the callbacks
    cpUnregisterPointers();
    for (size_t i = 0; i < cp_completed_ranks.size(); i++) {
      if (getProcessState(i) != ProcessState::DEAD) {
        cp_completed_ranks[i] = false;
        cp_started_ranks[i] = false;
      }
    }

    exec_state = ExecState::RUNNING;
    cp_count++;
    // Tell CP thread to complete checkpointing
    std::unique_lock<std::mutex> lock_cp(cp_mutex);
    cp_instruction = CPInstruction::COMPLETE_CP;
    cp_cv.notify_all();
    lock_cp.unlock();

    // Notify the CP Interval calculation
    std::unique_lock<std::mutex> lock_ih(ih_mutex);
    ih_instruction = IHInstruction::CALC_INTERVAL;
    ih_cv.notify_all();
    lock_ih.unlock();
  } break;
  case CP_CANCELED: {
    FTDEBUG("[Rank %d FT] - Canceled CP (version: %d)\n", rank, second_value);

    // Redefine cp structures and notify the callbacks
    for (size_t i = 0; i < cp_completed_ranks.size(); i++) {
      if (getProcessState(i) != ProcessState::DEAD) {
        cp_completed_ranks[i] = false;
        cp_started_ranks[i] = false;
      }
    }

    exec_state = ExecState::RUNNING;
    cp_count++;
    // Tell CP thread to cancel current checkpoint
    if (cp_instruction == CPInstruction::EXEC_CP) {
      // If the thread that calls this procedure is executing CP, only changes
      // the instruction
      cp_instruction = CPInstruction::CANCEL_CP;
    } else {
      std::unique_lock<std::mutex> lock_cp(cp_mutex);
      cp_instruction = CPInstruction::CANCEL_CP;
      cp_cv.notify_all();
      lock_cp.unlock();
    }

    // Notify the CP Interval calculation
    cp_cost_sum = cp_last_sum; // As the CP was canceled, use last cost
    std::unique_lock<std::mutex> lock_ih(ih_mutex);
    ih_instruction = IHInstruction::CALC_INTERVAL;
    ih_cv.notify_all();
    lock_ih.unlock();
  } break;
  default: {
    FTDEBUG("[Rank %d FT] Ignoring unknown broadcast\n", rank);
  } break;
  }

  // Does the broadcast
  for (std::size_t i = 1; i < neighbors.size(); i *= 2) {
    int index = (n_pos + i) % neighbors.size();
    if (neighbors[index] != rank) {
      MPI_Isend(bc_message, 4, MPI_INT, neighbors[index], TAG_HB_BCAST, hb_comm,
                &send_request);
      MPI_Request_free(&send_request);
    }
  }
}

/// Failure handling function
void FaultTolerance::failureHandler() {
  PROF_INIT_THD("Failure Handler Thread",
                std::numeric_limits<short>::max() + 2);

  bool loop_condition = true;
  std::unique_lock<std::mutex> lock(fh_mutex);
  do {
    // Resets and waits for an instruction
    fh_instruction = FHInstruction::NONE;
    fh_cv.wait(lock, [this] { return fh_instruction != FHInstruction::NONE; });

    switch (fh_instruction) {
    case FHInstruction::FAILURE: {
      // TODO: We might need a queue of failed processes here to handle multiple
      // failures
      exec_state = ExecState::RESTARTING;
      for (const auto &callback : notify_callbacks)
        callback({FTNotificationID::FAILURE, failed_rank});
      failed_rank = -1; // reset the value
      exec_state = ExecState::RUNNING;
    } break;
    case FHInstruction::FALSE_POSITIVE: {
      for (const auto &callback : notify_callbacks)
        callback({FTNotificationID::FALSE_POSITIVE, failed_rank});
      failed_rank = -1; // reset the value
    } break;
    case FHInstruction::FINISH: {
      loop_condition = false;
    } break;
    case FHInstruction::NONE:
      break;
    }
  } while (loop_condition);
}

// Checkpointing related functions
// ===----------------------------------------------------------------------===
// Default MTBF value
constexpr int DEFAULT_MTBF = 86400; // TODO: Change to reference
// Default write speed in MB/s
constexpr int DEFAULT_WRITE_SPEED = 10;

/// Initializes checkpoint related variables and VELOC passing \p loc as veloc
/// configuration file path
void FaultTolerance::cpInit(const char *loc) {
#if COMPILE_WITH_VELOC
  cp_reg_pointers.clear();
  cp_next_region_id = 0;
  cp_count = 0;

  // Mean time between failures should be in seconds
  cp_mtbf = DEFAULT_MTBF;
  if (char *env_str = getenv("OMPCLUSTER_CP_MTBF")) {
    cp_mtbf = std::stoi(env_str);
    FTDEBUG("[Rank %d FT] Using CP mtbf defined in environment variables: %d\n",
            rank, cp_mtbf);
  }
  // Write speed should be in MB/s
  cp_wspeed = DEFAULT_WRITE_SPEED;
  if (char *env_str = getenv("OMPCLUSTER_CP_WSPEED")) {
    cp_wspeed = std::stoi(env_str);
    FTDEBUG("[Rank %d FT] Using CP write speed defined in environment "
            "variables: %d\n",
            rank, cp_wspeed);
  }
  // Custom file name
  cp_file_name = "ckpt";
  if (char *env_str = getenv("SLURM_JOB_ID")) {
    cp_file_name = std::string(env_str);
    FTDEBUG("[Rank %d FT] Using CP file name as the slurm job id: %s\n", rank,
            cp_file_name.c_str());
  }

  cp_veloc_mutex.lock();
  if (VELOC_Init_single(rank, loc) != VELOC_SUCCESS) {
    using_veloc = false;
    return;
  }
  cp_veloc_mutex.unlock();

  // Start CP related threads
  interval_thread = std::thread(&FaultTolerance::intervalHandler, this);
  checkpoint_thread = std::thread(&FaultTolerance::checkpointHandler, this);
  wait_transfer_thread = std::thread(&FaultTolerance::transferHandler, this);
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
#endif
}

/// Clean up checkpoint directories upon successfully application conclusion
/// and ends checkpointing thread
void FaultTolerance::cpEnd() {
#if COMPILE_WITH_VELOC
  if (using_veloc == false)
    return;

  // Unregister pointers before finalizing
  cp_veloc_mutex.lock();
  for (std::size_t i = 0; i < cp_reg_pointers.size(); i++) {
    VELOC_Mem_unprotect(cp_reg_pointers[i].id);
  }

  VELOC_Finalize(0);
  cp_veloc_mutex.unlock();

  // End wait transfer thread
  std::unique_lock<std::mutex> lock_ih(ih_mutex);
  ih_instruction = IHInstruction::FINISH;
  ih_cv.notify_all();
  lock_ih.unlock();
  if (interval_thread.joinable())
    interval_thread.join();

  // End checkpoint thread
  std::unique_lock<std::mutex> lock_cp(cp_mutex);
  cp_instruction = CPInstruction::FINISH;
  cp_cv.notify_all();
  lock_cp.unlock();
  if (checkpoint_thread.joinable())
    checkpoint_thread.join();

  // End wait transfer thread
  std::unique_lock<std::mutex> lock_wt(wt_mutex);
  wt_instruction = WTInstruction::FINISH;
  wt_cv.notify_all();
  lock_wt.unlock();
  if (wait_transfer_thread.joinable())
    wait_transfer_thread.join();
  DP("[Rank %d] - Finished all CP related threads\n", rank);
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
#endif
}

/// Calculates the interval till next checkpoint and instantiate a new
/// checkpointing thread
void FaultTolerance::cpSetNextInterval() {
#if COMPILE_WITH_VELOC
  if (using_veloc == false)
    return;

  cp_last_sum = cp_cost_sum;
  double cp_cost = cp_cost_sum / cp_wspeed;
  cp_cost = (cp_cost > 0) ? cp_cost : 1;

  // Young's Formula. Source: John W. Young. 1974. A first order approximation
  // to the optimum checkpoint interval. Commun. ACM 17, 9 (Sept. 1974),
  // 530–531.
  cp_interval = floor(sqrt(2 * cp_cost * cp_mtbf));

  cp_cost_sum = 0;

  FTDEBUG("[Rank %d FT] Next checkpoint will be in %d seconds\n", rank,
          cp_interval);
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
#endif
}

void FaultTolerance::intervalHandler() {
#if COMPILE_WITH_VELOC
  PROF_INIT_THD("Checkpoint Thread", std::numeric_limits<short>::max() + 4);
  if (using_veloc == false)
    return;

  // Initialization
  cp_cost_sum = cp_wspeed;
  cpSetNextInterval();
  bool loop_condition = true;
  std::unique_lock<std::mutex> lock(ih_mutex);
  do {
    // Resets and waits for an instruction. This is an special case, since we
    // also have a timeout for this notification
    ih_instruction = IHInstruction::NONE;
    auto now = std::chrono::system_clock::now();
    if (!ih_cv.wait_until(
            lock, now + std::chrono::seconds(cp_interval),
            [this]() { return ih_instruction != IHInstruction::NONE; })) {
      // If the wait condition timed out, we have a CP to do
      ih_instruction = IHInstruction::START_CP;
    }

    switch (ih_instruction) {
    case IHInstruction::START_CP: {
      if (exec_state == ExecState::RUNNING) {
        exec_state = ExecState::CHECKPOINTING;
        hbBroadcast(CP_START, rank, cp_count + 1, 0);
        // Sets interval to the maximum possible temporary
        cp_interval = INT_MAX;
      } else {
        // Just calculate next interval e try again
        cp_cost_sum = cp_last_sum;
        cpSetNextInterval();
      }
    } break;
    case IHInstruction::CALC_INTERVAL: {
      cpSetNextInterval();
    } break;
    case IHInstruction::FINISH: {
      loop_condition = false;
    } break;
    case IHInstruction::NONE:
      break;
    }
  } while (loop_condition);
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
#endif
}

/// Waits until user calls the checkpointing save function and then execute the
/// procedures to save a new checkpoint
void FaultTolerance::checkpointHandler() {
#if COMPILE_WITH_VELOC
  PROF_INIT_THD("Checkpoint Thread", std::numeric_limits<short>::max() + 2);
  if (using_veloc == false)
    return;

  bool loop_condition = true;
  std::unique_lock<std::mutex> lock(cp_mutex);
  do {
    cp_instruction = CPInstruction::NONE;
    cp_cv.wait(lock, [this] { return cp_instruction != CPInstruction::NONE; });

    switch (cp_instruction) {
    case CPInstruction::EXEC_CP: {
      for (const auto &callback : notify_callbacks)
        callback({FTNotificationID::CHECKPOINT, rank});

      // Special case, if the CP is canceled before finishing, the procedure for
      // canceling would be called by this thread causing deadlock
      if (cp_instruction == CPInstruction::CANCEL_CP)
        for (const auto &callback : notify_callbacks)
          callback({FTNotificationID::CHECKPOINT_DONE, -1});
    } break;
    case CPInstruction::COMPLETE_CP: {
      for (const auto &callback : notify_callbacks)
        callback({FTNotificationID::CHECKPOINT_DONE, 0});
    } break;
    case CPInstruction::CANCEL_CP: {
      for (const auto &callback : notify_callbacks)
        callback({FTNotificationID::CHECKPOINT_DONE, -1});
    } break;
    case CPInstruction::FINISH: {
      loop_condition = false;
    } break;
    case CPInstruction::NONE:
      break;
    }
  } while (loop_condition);
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
#endif
}

/// User level function to call the procedures to save a new checkpoint
int FaultTolerance::cpSaveCheckpoint(int version) {
#if COMPILE_WITH_VELOC
  if (using_veloc == false)
    return FT_VELOC_ERROR;

  if (version < 0) {
    FTDEBUG("[Rank %d FT] - Canceling checkpoint\n", rank);
    hbBroadcast(CP_CANCELED, rank, cp_count + 1, 0);
    return version;
  }

  // Save the checkpoint
  if (cp_reg_pointers.size() > 0) {
    cp_veloc_mutex.lock();
    std::string cp_name = cp_file_name;
    if (VELOC_Checkpoint(cp_name.c_str(), version) != VELOC_SUCCESS) {
      FTDEBUG("[Rank %d FT] - Could not save the checkpoint\n", rank);
      hbBroadcast(CP_CANCELED, rank, cp_count + 1, 0);
      return FT_VELOC_ERROR;
    }
    cp_veloc_mutex.unlock();

    cp_local_cost = 0;
    for (size_t i = 0; i < cp_reg_pointers.size(); i++)
      cp_local_cost += cp_reg_pointers[i].size * cp_reg_pointers[i].base_size;

    // Transform in MB
    cp_local_cost = (cp_local_cost / 1024 / 1024);
  }
  // Wait for Veloc to send the checkpoint from scratch to persistent
  // directory in another thread to wait in background
  std::unique_lock<std::mutex> lock_wt(wt_mutex);
  wt_instruction = WTInstruction::TRANSFER;
  wt_cv.notify_all();
  lock_wt.unlock();

  return version;
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
  return -1;
#endif
}

void FaultTolerance::transferHandler() {
#if COMPILE_WITH_VELOC
  if (using_veloc == false)
    return;

  bool loop_condition = true;
  std::unique_lock<std::mutex> lock(wt_mutex);
  do {
    wt_instruction = WTInstruction::NONE;
    wt_cv.wait(lock, [this] { return wt_instruction != WTInstruction::NONE; });

    switch (wt_instruction) {
    case WTInstruction::TRANSFER: {
      // Wait only if saved anything (Veloc seems to fail if doing procedures
      // without registered pointers)
      int result = VELOC_SUCCESS;
      cp_veloc_mutex.lock();
      if (cp_reg_pointers.size() > 0)
        result = VELOC_Checkpoint_wait();
      cp_veloc_mutex.unlock();

      // If operation succeed, send checkpoint complete notification
      if (exec_state == ExecState::CHECKPOINTING) {
        if (result == VELOC_SUCCESS) {
          hbBroadcast(CP_COMPLETED, rank, cp_count + 1, cp_local_cost);
        } else {
          FTDEBUG("[Rank %d FT] - Veloc couldn't transfer checkpoint from "
                  "scratch to persistent directory, please check config "
                  "options and permissions, disabling checkpoint\n",
                  rank);
          hbBroadcast(CP_CANCELED, rank, cp_count + 1, 0);
        }
      }
    } break;
    case WTInstruction::FINISH: {
      loop_condition = false;
    } break;
    case WTInstruction::NONE:
      break;
    }
  } while (loop_condition);
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
#endif
}

/// Ends current Veloc context, to enable loading other's rank context
void FaultTolerance::cpLoadStart() {
#if COMPILE_WITH_VELOC
  if (using_veloc == false)
    return;

  // The load process should be done by calling cpLoadStart() one time, one or
  // multiple calls to cpLoadMem(), and one final call to cpLoadEnd(), so we
  // will lock the mutex in cpLoadStart and free it in cpLoadEnd().
  cp_veloc_mutex.lock();

  VELOC_Finalize(0);
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
#endif
}

// TODO: LoadMem function is currently implemented such that every memory region
// will start and finalize Veloc. In the future, we should be able to restore
// all regions that belong to the same checkpoint file.
/// Recovers a specific region of memory saved with \p id in the cp \p version
/// by the process of \p s_cp_rank
int FaultTolerance::cpLoadMem(int id, int s_cp_rank, int ver, size_t count,
                              size_t base_size, void *memregion) {
#if COMPILE_WITH_VELOC
  if (using_veloc == false)
    return FT_VELOC_ERROR;

  // Initialize Veloc on the s_cp_rank, so Veloc will route the right file.
  if (VELOC_Init_single(s_cp_rank, cp_cfg_file_path.c_str()) != VELOC_SUCCESS) {
    using_veloc = false;
    FTDEBUG("[Rank %d FT] - Could not start Veloc with rank %d - %s\n", rank,
            s_cp_rank, cp_cfg_file_path.c_str());
    return FT_VELOC_ERROR;
  }

  std::string cp_name = cp_file_name;
  // result of v should be equal to the version argument
  int v = VELOC_Restart_test(cp_name.c_str(), ver + 1);
  if (v >= ver) {
    // Protect the buffer argument with the id saved in checkpoint
    VELOC_Mem_protect(id, memregion, count, base_size);
    FTDEBUG("[Rank %d FT] - Recovering checkpoint version %d\n", rank, ver);
    // Load the memory region
    VELOC_Restart_begin(cp_name.c_str(), ver);
    int ids[1] = {id};
    int r = VELOC_Recover_selective(VELOC_RECOVER_SOME, ids, 1);
    VELOC_Restart_end(0);
    FTDEBUG("Result of restart: %s\n",
            (r == VELOC_FAILURE) ? "Failure" : "Success");
    // Ends this context of Veloc
    VELOC_Finalize(0);
    return FT_VELOC_SUCCESS;
  } else {
    FTDEBUG("[Rank %d FT] - Last valid CP and version doesn't match\n", rank);
    return FT_VELOC_ERROR;
  }
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
  return FT_VELOC_ERROR;
#endif
}

/// Restores the context of Veloc before loading memory regions
void FaultTolerance::cpLoadEnd() {
#if COMPILE_WITH_VELOC
  if (using_veloc == false)
    return;

  // Initialize Veloc on this rank again
  if (VELOC_Init_single(rank, cp_cfg_file_path.c_str()) != VELOC_SUCCESS) {
    using_veloc = false;
    return;
  }

  // Free the load process mutex
  cp_veloc_mutex.unlock();
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
#endif
}

/// Registers the regions present in \p ptrs vector
void FaultTolerance::cpRegisterPointers(std::vector<uintptr_t> ptrs,
                                        std::vector<int64_t> sizes,
                                        size_t base_size, int *id) {
#if COMPILE_WITH_VELOC
  if (using_veloc == false) {
    *id = -1;
    return;
  }

  cp_veloc_mutex.lock();
  for (size_t i = 0; i < ptrs.size(); i++) {
    *id = cp_next_region_id;
    void *ptr = (void *)ptrs[i];
    size_t count = sizes[i];
    cp_reg_pointers.push_back({cp_next_region_id, ptr, count, base_size});
    VELOC_Mem_protect(cp_next_region_id, ptr, count, base_size);
    cp_next_region_id++;
  }
  cp_veloc_mutex.unlock();
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
#endif
}

/// Unregisters every region registered before
void FaultTolerance::cpUnregisterPointers() {
#if COMPILE_WITH_VELOC
  if (using_veloc == false)
    return;

  cp_veloc_mutex.lock();
  for (size_t i = 0; i < cp_reg_pointers.size(); i++)
    VELOC_Mem_unprotect(cp_reg_pointers[i].id);

  cp_reg_pointers.clear();
  cp_next_region_id = 0;
  cp_veloc_mutex.unlock();
#else
  FTDEBUG("[Rank %d FT] - Checkpointing isn't enabled\n", rank);
#endif
}

// General fault tolerance functions
// ===----------------------------------------------------------------------===
/// Configure error handling for main and heartbeat communicators
void FaultTolerance::setErrorHandling() {
  MPI_Comm_create_errhandler(errorHandlerFunction, &main_error_handler);
  MPI_Comm_set_errhandler(main_comm, main_error_handler);
  MPI_Comm_create_errhandler(errorHandlerFunction, &hb_error_handler);
  MPI_Comm_set_errhandler(hb_comm, hb_error_handler);
}

/// Execute all procedures to make a new communicator with alive processes
void FaultTolerance::commRepair() {
  FTDEBUG("[Rank %d FT] Starting communicator repair process\n", rank);

  MPI_Group main_comm_group;
  MPI_Comm_group(main_comm, &main_comm_group);

  FTDEBUG("[Rank %d FT] Creating group with alive processes\n", rank);
  int n_size = neighbors.size();
  std::sort(neighbors.begin(), neighbors.end());
  int *alive_ranks = neighbors.data();

  MPI_Group alive_group;
  MPI_Group_incl(main_comm_group, n_size, (const int *)alive_ranks,
                 &alive_group);

  // Create a new Comm. Point of sync
  FTDEBUG("[Rank %d FT] Creating new communicator\n", rank);
  MPI_Comm new_comm;
  MPI_Comm_create_group(main_comm, alive_group, 0, &new_comm);

  assertm(new_comm != MPI_COMM_NULL, "Created invalid communicator");
  FTDEBUG("[Rank %d FT] Created new communicator\n", rank);

  int l_rank;
  MPI_Comm_rank(new_comm, &l_rank);
  MPI_Comm_size(new_comm, &size);
  FTDEBUG("[Rank %d FT] Is now rank %d of %d.\n", rank, l_rank, size);
  rank = l_rank;

  main_comm = new_comm;
  MPI_Comm_dup(main_comm, &hb_comm);
  setErrorHandling();

  c_state = CommState::VALID;
  delete p_states;
  neighbors.clear();
  hbInit();

  FTDEBUG("[Rank %d FT] Finished repair\n", rank);
  MPI_Barrier(hb_comm);
}

// User level functions
// ===----------------------------------------------------------------------===
/// Return current state of process with rank equals to \p id
ProcessState FaultTolerance::getProcessState(int id) {
  if (disable_ft)
    return ProcessState::DEAD;
  return p_states[id];
}

/// Return current state of MPI communicators
CommState FaultTolerance::getCommState() {
  if (disable_ft)
    return CommState::INVALID;
  return c_state;
}

/// Request by main thread to repair communicator
MPI_Comm FaultTolerance::requestCommRepair() {
  if (disable_ft)
    return MPI_COMM_NULL;
  if (c_state == CommState::INVALID) {
    if (rank == 0) {
      // Value and pos (BC parameters) are used to build an agree function
      int sum = 0;
      for (std::size_t i = 0; i < neighbors.size(); i += 2)
        sum += neighbors[i] * ((i < neighbors.size()) ? neighbors[i + 1] : 1);

      // Starts a request to se if all processes agree
      hbBroadcast(HB_BC_REPAIR, sum, neighbors.size(), 0);
    }
    // This waits for heartbeat thread to verify if comm repair is possible
    std::unique_lock<std::mutex> w_lock(hb_need_repair_mutex);
    hb_need_repair_cv.wait(w_lock, [&] { return hb_need_repair; });
    hb_need_repair_mutex.unlock();

    // Start repair, the HB thread can't repair since it could repair before
    // the main thread enters this function
    commRepair();

    // Free the HB thread - This notifies the heartbeat thread there is waiting
    std::unique_lock<std::mutex> n_lock(hb_need_repair_mutex);
    hb_need_repair = false;
    hb_need_repair_cv.notify_all();
    n_lock.unlock();
  }

  return main_comm;
}

/// Public gather function that returns MPI main communicator
MPI_Comm FaultTolerance::getMainComm() {
  if (disable_ft)
    return MPI_COMM_NULL;
  // Update application size
  MPI_Comm_size(main_comm, &app_size);
  return main_comm;
}

/// Register an notification callback
void FaultTolerance::registerNotifyCallback(
    std::function<void(FTNotification)> callback) {
  if (disable_ft)
    return;
  notify_callbacks.push_back(callback);
}

// Test purpose functions
// ===----------------------------------------------------------------------===
/// Test purpose function that returns the heartbeat thread id
uint64_t FaultTolerance::getID() {
  std::stringstream ss;
  ss << thread_id;
  uint64_t id = std::stoull(ss.str());
  return id;
}

/// Test purpose function that returns the heartbeat current emitter
int FaultTolerance::getEmitter() { return emitter; }

// Wrappers helping functions
// ===----------------------------------------------------------------------===
/// Returns the current MPI rank
int FaultTolerance::getRank() {
  if (disable_ft) {
    return -1;
  }
  return rank;
}

/// Returns the current size the application have. In case of commRepair shrink,
/// this size only will be updated if application calls getMainComm()
int FaultTolerance::getSize() {
  if (disable_ft) {
    return -1;
  }
  return app_size;
}

/// Disables asserts for testing
void FaultTolerance::disableAsserts() {
  if (disable_ft) {
    return;
  }
  disable_wrappers_asserts = true;
}

// ===----------------------------------------------------------------------===
// Failure injector
// ===----------------------------------------------------------------------===

FailureInjector::FailureInjector(int64_t time, RTLDeviceInfoTy *rtl) {
  fi_condition = false;
  rtl_handler = rtl;
  interval = time;

  // Cancel if not using veloc
  if (using_veloc == false)
    return;

  max_failures = INJECTOR_MAX_FAILURES;
  if (char *env_str = getenv("OMPCLUSTER_FI_MAX_FAILURES")) {
    max_failures = std::stoi(env_str);
  }

  if (char *env_str = getenv("OMPCLUSTER_FI_FAILURE_INTERVAL")) {
    interval = std::stoi(env_str);
  }

  if (char *env_str = getenv("OMPCLUSTER_FI_ENABLE")) {
    if (std::stoi(env_str) != 1)
      return;
  } else {
    return;
  }

  mpi_rank = ft_handler->getRank();
  injector = std::thread(&FailureInjector::injectFailure, this);
}

FailureInjector::~FailureInjector() {
  // End checkpoint thread
  std::unique_lock<std::mutex> lock(fi_mutex);
  fi_condition = true;
  fi_cv.notify_all();
  lock.unlock();
  if (injector.joinable())
    injector.join();
  DP("[Rank %d] - Failure injector finalized\n", mpi_rank);
}

void FailureInjector::injectFailure() {
  srand(9999);
  number_of_failures = 0;
  std::unique_lock<std::mutex> lock(fi_mutex);
  do {
    FTDEBUG("Injecting a failure in %ld seconds\n", interval);
    if (!fi_cv.wait_for(lock, std::chrono::seconds(interval),
                        [this]() { return fi_condition == true; })) {
      if (!ft_handler)
        break;

      // Sort a rank to fail
      int size = ft_handler->getSize();
      if (size > 4 && number_of_failures < max_failures) {
        int failure = rand() % size;
        // Guarantee that rank 0 or 1 does not fail
        failure = failure < 2 ? 2 : failure;

        if (mpi_rank == failure) {
          FTDEBUG("Injecting failure in rank %d\n", mpi_rank);
          failed = true;
          delete rtl_handler->ft_handler;
          rtl_handler->ft_handler = nullptr;
          rtl_handler->event_system->assignFT(nullptr);
          break;
        }

        number_of_failures++;
        if (number_of_failures >= max_failures) {
          break;
        }
      }
    } else {
      break;
    }
  } while (!fi_condition);
}

} // namespace ft
