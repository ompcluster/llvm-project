//===------RTLs/mpi/src/rtl.cpp - Target RTLs Implementation - C++ ------*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// RTL for MPI machine
//
//===----------------------------------------------------------------------===//

#include <cassert>
// TODO: Do we need this?
#include <unistd.h>

#include "mpi_common.h"
#include "mpi_manager.h"

#include "omptarget.h"
#include "omptargetplugin.h"
#include "profiler.h"

static RTLDeviceInfoTy DeviceInfo;

#ifdef __cplusplus
extern "C" {
#endif

int32_t __tgt_rtl_is_valid_binary(__tgt_device_image *image) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  return DeviceInfo.isValidBinary(image);
}

int32_t __tgt_rtl_number_of_devices() {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  return DeviceInfo.getNumOfDevices();
}

int32_t __tgt_rtl_init_device(int32_t device_id) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  return OFFLOAD_SUCCESS;
}

__tgt_target_table *__tgt_rtl_load_binary(int32_t device_id,
                                          __tgt_device_image *image) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);

  OMPC_DP("Dev %d: load binary from " DPxMOD " image\n", device_id,
          DPxPTR(image->ImageStart));

  assert(DeviceInfo.isValidDeviceId(device_id) && "bad dev id");

  return DeviceInfo.loadBinary(device_id, image);
}

void *__tgt_rtl_data_alloc(int32_t device_id, int64_t size, void *hst_ptr,
                           int32_t kind) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  OMPC_DP("__tgt_rtl_data_alloc\n");

  switch (kind) {
  case TARGET_ALLOC_DEVICE:
  case TARGET_ALLOC_DEFAULT:
    return DeviceInfo.dataAlloc(device_id, size, hst_ptr);
  default:
    OMPC_DP("Invalid target data allocation kind");
    return nullptr;
  }
}

int32_t __tgt_rtl_register_bcast_ptr(int32_t arg_num, void **args_base,
                                     void **args, int64_t *arg_sizes,
                                     int64_t *arg_types, bool AddToList) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  return DeviceInfo.registerBcastPtr(arg_num, args_base, args, arg_sizes,
                                     arg_types, AddToList);
}

int32_t __tgt_rtl_data_recovery(int32_t device_id, void *tgt_ptr, int64_t size,
                                int32_t buffer_id, int32_t cp_rank,
                                int32_t cp_ver) {
  DP("__tgt_rtl_data_recovery\n");
  return DeviceInfo.dataRecovery(device_id, tgt_ptr, size, buffer_id, cp_rank,
                                 cp_ver);
}

int32_t __tgt_rtl_data_recovery_async(int32_t device_id, void *tgt_ptr,
                                      int64_t size, int32_t buffer_id,
                                      int32_t cp_rank, int32_t cp_ver,
                                      __tgt_async_info *async_info) {
  DP("__tgt_rtl_data_recovery_async\n");
  return DeviceInfo.dataRecoveryAsync(device_id, tgt_ptr, size, buffer_id,
                                      cp_rank, cp_ver, async_info);
}

int32_t __tgt_rtl_data_submit(int32_t device_id, void *tgt_ptr, void *hst_ptr,
                              int64_t size) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  OMPC_DP("__tgt_rtl_data_submit\n");

  return DeviceInfo.dataSubmit(device_id, tgt_ptr, hst_ptr, size);
}

int32_t __tgt_rtl_data_submit_async(int32_t device_id, void *tgt_ptr,
                                    void *hst_ptr, int64_t size,
                                    __tgt_async_info *async_info) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  OMPC_DP("__tgt_rtl_data_submit_async\n");

  return DeviceInfo.dataSubmitAsync(device_id, tgt_ptr, hst_ptr, size,
                                    async_info);
}

int32_t __tgt_rtl_data_retrieve(int32_t device_id, void *hst_ptr, void *tgt_ptr,
                                int64_t size) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  OMPC_DP("__tgt_rtl_data_retrieve\n");

  return DeviceInfo.dataRetrieve(device_id, hst_ptr, tgt_ptr, size);
}

int32_t __tgt_rtl_data_retrieve_async(int32_t device_id, void *hst_ptr,
                                      void *tgt_ptr, int64_t size,
                                      __tgt_async_info *async_info) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  OMPC_DP("__tgt_rtl_data_retrieve_async\n");

  return DeviceInfo.dataRetrieveAsync(device_id, hst_ptr, tgt_ptr, size,
                                      async_info);
}

int32_t __tgt_rtl_is_data_exchangable(int32_t src_dev_id, int32_t dst_dev_id) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  if (DeviceInfo.isValidDeviceId(src_dev_id) &&
      DeviceInfo.isValidDeviceId(dst_dev_id)) {
    PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
    return 1;
  }

  return 0;
}

int32_t __tgt_rtl_data_exchange(int32_t src_id, void *src_ptr, int32_t dst_id,
                                void *dst_ptr, int64_t size) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  return DeviceInfo.dataExchange(src_id, src_ptr, dst_id, dst_ptr, size);
}

// Asynchronous version of __tgt_rtl_data_exchange
int32_t __tgt_rtl_data_exchange_async(int32_t src_id, void *src_ptr,
                                      int32_t dst_id, void *dst_ptr,
                                      int64_t size,
                                      __tgt_async_info *async_info_ptr) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  return DeviceInfo.dataExchangeAsync(src_id, src_ptr, dst_id, dst_ptr, size,
                                      async_info_ptr);
}

int32_t __tgt_rtl_data_delete(int32_t device_id, void *tgt_ptr) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  OMPC_DP("__tgt_rtl_data_delete\n");

  return DeviceInfo.dataDelete(device_id, tgt_ptr);
}

int32_t __tgt_rtl_run_target_team_region(int32_t device_id, void *tgt_entry_ptr,
                                         void **tgt_args,
                                         ptrdiff_t *tgt_offsets,
                                         int32_t arg_num, int32_t team_num,
                                         int32_t thread_limit,
                                         uint64_t loop_tripcount /*not used*/) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  // ignore team num and thread limit.
  OMPC_DP("__tgt_rtl_run_target_team_region\n");

  return DeviceInfo.runTargetTeamRegion(device_id, tgt_entry_ptr, tgt_args,
                                        tgt_offsets, arg_num, team_num,
                                        thread_limit, loop_tripcount);
}

int32_t __tgt_rtl_run_target_team_region_async(
    int32_t device_id, void *tgt_entry_ptr, void **tgt_args,
    ptrdiff_t *tgt_offsets, int32_t arg_num, int32_t team_num,
    int32_t thread_limit, uint64_t loop_tripcount /*not used*/,
    __tgt_async_info *async_info) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  // ignore team num and thread limit.
  OMPC_DP("__tgt_rtl_run_target_team_region_async\n");

  return DeviceInfo.runTargetTeamRegionAsync(
      device_id, tgt_entry_ptr, tgt_args, tgt_offsets, arg_num, team_num,
      thread_limit, loop_tripcount, async_info);
}

int32_t __tgt_rtl_run_target_region(int32_t device_id, void *tgt_entry_ptr,
                                    void **tgt_args, ptrdiff_t *tgt_offsets,
                                    int32_t arg_num) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  // use one team and one thread.
  return DeviceInfo.runTargetTeamRegion(device_id, tgt_entry_ptr, tgt_args,
                                        tgt_offsets, arg_num, 1, 1, 0);
}

int32_t __tgt_rtl_run_target_region_async(int32_t device_id,
                                          void *tgt_entry_ptr, void **tgt_args,
                                          ptrdiff_t *tgt_offsets,
                                          int32_t arg_num,
                                          __tgt_async_info *async_info) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  // use one team and one thread.
  return DeviceInfo.runTargetTeamRegionAsync(device_id, tgt_entry_ptr, tgt_args,
                                             tgt_offsets, arg_num, 1, 1, 0,
                                             async_info);
}

int32_t __tgt_rtl_synchronize(int32_t device_id, __tgt_async_info *async_info) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  return DeviceInfo.synchronize(device_id, async_info);
}

int32_t __tgt_rtl_synchronize_id(int32_t device_id, int32_t task_id,
                                 __tgt_async_info *async_info) {
  PROF_SCOPED(PROF_LVL_ALL, __FUNCTION__);
  return DeviceInfo.synchronize(device_id, async_info, task_id);
}

int32_t __tgt_rtl_start_devices(__tgt_bin_desc *desc) {
  return DeviceInfo.startDeviceMain(desc);
}

int32_t __tgt_rtl_create_event(int32_t ID, void **Event) {
  return DeviceInfo.createEvent(ID, Event);
}

int32_t __tgt_rtl_destroy_event(int32_t ID, void *Event) {
  return DeviceInfo.destroyEvent(ID, Event);
}

int32_t __tgt_rtl_record_event(int32_t ID, void *Event,
                               __tgt_async_info *AsyncInfo) {
  return DeviceInfo.recordEvent(ID, Event, AsyncInfo);
}

int32_t __tgt_rtl_wait_event(int32_t ID, void *Event,
                             __tgt_async_info *AsyncInfo) {
  return DeviceInfo.waitEvent(ID, Event, AsyncInfo);
}

int32_t __tgt_rtl_sync_event(int32_t ID, void *Event) {
  return DeviceInfo.syncEvent(ID, Event);
}

int32_t __tgt_rtl_register_ft_callback(void *callback) {
  return DeviceInfo.registerFTCallback(callback);
}

int32_t __tgt_rtl_register_cp_ptrs(int32_t device_id,
                                   uintptr_t *ptrs_to_register,
                                   int64_t *ptrs_size, int32_t num_ptrs) {
  return DeviceInfo.registerCPPtrs(device_id, ptrs_to_register, ptrs_size,
                                   num_ptrs);
}

int32_t __tgt_rtl_register_cp_ptrs_async(int32_t device_id,
                                         uintptr_t *ptrs_to_register,
                                         int64_t *ptrs_size, int32_t num_ptrs,
                                         __tgt_async_info *async_info) {
  return DeviceInfo.registerCPPtrsAsync(device_id, ptrs_to_register, ptrs_size,
                                        num_ptrs, async_info);
}

int32_t __tgt_rtl_request_checkpoint(int32_t device_id, int32_t *version) {
  return DeviceInfo.requestCheckpoint(device_id, version);
}

int32_t __tgt_rtl_request_checkpoint_async(int32_t device_id, int32_t *version,
                                           __tgt_async_info *AsyncInfoPtr) {
  return DeviceInfo.requestCheckpointAsync(device_id, version, AsyncInfoPtr);
}

int32_t __tgt_rtl_register_failed_tasks_ids(int32_t number_of_tasks,
                                            int32_t *tasks_ids) {
  return DeviceInfo.registerFailedTasksId(number_of_tasks, tasks_ids);
}

#ifdef __cplusplus
}
#endif
