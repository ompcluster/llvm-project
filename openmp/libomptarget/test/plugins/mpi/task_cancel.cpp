// RUN: %libomptarget-compile-generic && env OMP_CANCELLATION=true %libomptarget-run-generic | %fcheck-generic

#include <cstdio>

int main() {
  int counter = 0;
  constexpr int task_count = 10;

// Phase 1: Start task_count target-tasks, but only one should run.
#pragma omp parallel
#pragma omp single
#pragma omp taskgroup
  {
    for (int i = 0; i < task_count; ++i) {
#pragma omp target nowait map(tofrom : counter) depend(inout : counter)
      { counter++; }

#pragma omp task depend(inout : counter)
      {
#pragma omp cancel taskgroup
      }
    }
  }

  // CHECK: Counter 01
  printf("Counter %02d\n", counter);

// Phase 2: Start task_count target-tasks. They should run successfully, even
// after a taskgroup cancel operation.
#pragma omp parallel
#pragma omp single
  {
    for (int i = 0; i < task_count; ++i) {
#pragma omp target nowait map(tofrom : counter) depend(inout : counter)
      { counter++; }
    }
  }

  // CHECK: Counter 11
  printf("Counter %02d\n", counter);

  return 0;
}
