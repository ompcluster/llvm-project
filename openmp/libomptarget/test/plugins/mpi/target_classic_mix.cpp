// RUN: %libomptarget-compilexx-run-and-check-generic

// XFAIL: x86_64-pc-linux-gnu

// RUN: %libomptarget-compilexx-x86_64-pc-linux-gnu-ompc
// RUN: env OMPCLUSTER_SCHEDULER=heft \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc

// RUN: %libomptarget-compilexx-x86_64-pc-linux-gnu
// RUN: env OMPCLUSTER_SCHEDULER=roundrobin \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc

// RUN: %libomptarget-compilexx-x86_64-pc-linux-gnu-ompc
// RUN: env OMPCLUSTER_SCHEDULER=graph_roundrobin \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc


#include <cstdio>

int main() {
  int *buffer = new int[1];
  buffer[0] = 1;

  #pragma omp parallel
  #pragma omp single
  {
    #pragma omp target enter data map(to: buffer[:1]) nowait depend(inout: *buffer)

    #pragma omp task depend(in: *buffer)
    {
      // CHECK: Buffer 1
      printf("Buffer %d\n", buffer[0]);
    }

    #pragma omp target nowait depend(inout: *buffer)
    {
      buffer[0] = 2;
    }

    #pragma omp task depend(in: *buffer)
    {
      // CHECK: Buffer 1
      printf("Buffer %d\n", buffer[0]);
    }

    #pragma omp target exit data map(from: buffer[:1]) nowait depend(inout: *buffer)
  }

  // CHECK: Buffer 2
  printf("Buffer %d\n", buffer[0]);

  delete[] buffer;

  return 0;
}
