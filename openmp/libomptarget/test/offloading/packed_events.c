// RUN: %libomptarget-compile-x86_64-pc-linux-gnu-ompc
// RUN: %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc
// RUN: env OMPCLUSTER_ENABLE_PACKING=1  \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc
// RUN: env OMPCLUSTER_ENABLE_PACKING=1 OMPCLUSTER_BCAST_STRATEGY=mpibcast \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc

// REQUIRES: x86_64-pc-linux-gnu-ompc

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main(void) {
  int test_array_1[] = {0, 1, 2, 3};
  int test_array_2[] = {4, 5, 6, 7};
  int test_array_3[] = {8, 9, 10, 11};

#pragma omp parallel
#pragma omp single
  {
#pragma omp target nowait map(tofrom                                           \
                              : test_array_1[:4], test_array_2                 \
                              [:4]) depend(in                                  \
                                           : *test_array_1, *test_array_2)
    {
      // CHECK: test_array_1[0]=0
      printf("test_array_1[0]=%d\n", test_array_1[0]);
      // CHECK: test_array_1[1]=1
      printf("test_array_1[1]=%d\n", test_array_1[1]);
      // CHECK: test_array_1[2]=2
      printf("test_array_1[2]=%d\n", test_array_1[2]);
      // CHECK: test_array_1[3]=3
      printf("test_array_1[3]=%d\n", test_array_1[3]);

      // CHECK: test_array_2[0]=4
      printf("test_array_2[0]=%d\n", test_array_2[0]);
      // CHECK: test_array_2[1]=5
      printf("test_array_2[1]=%d\n", test_array_2[1]);
      // CHECK: test_array_2[2]=6
      printf("test_array_2[2]=%d\n", test_array_2[2]);
      // CHECK: test_array_2[3]=7
      printf("test_array_2[3]=%d\n", test_array_2[3]);

      test_array_1[0] = -1;
    }
  }

  // CHECK:  test_array_1[0]=-1
  printf("test_array_1[0]=%d\n", test_array_1[0]);

#pragma omp target data map(to : test_array_2[:4], test_array_3[:4])
  {
#pragma omp parallel
#pragma omp single
    {
#pragma omp target nowait depend(in : *test_array_2, *test_array_3)
      {
        // CHECK: test_array_2[3]=7
        printf("test_array_2[3]=%d\n", test_array_2[3]);
        // CHECK: test_array_3[0]=8
        printf("test_array_3[0]=%d\n", test_array_3[0]);
        // CHECK: test_array_3[1]=9
        printf("test_array_3[1]=%d\n", test_array_3[1]);
        // CHECK: test_array_3[2]=10
        printf("test_array_3[2]=%d\n", test_array_3[2]);
        // CHECK: test_array_3[3]=11
        printf("test_array_3[3]=%d\n", test_array_3[3]);
      }
    }
  }

  return 0;
}
