// RUN: %libomptarget-compile-x86_64-pc-linux-gnu-ompc
// RUN: env OMPCLUSTER_HEFT_COMP_COST="1000000000" OMPCLUSTER_BCAST_STRATEGY=disabled \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc
// RUN: env OMPCLUSTER_HEFT_COMP_COST="1000000000" OMPCLUSTER_BCAST_STRATEGY=p2p \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc
// RUN: env OMPCLUSTER_HEFT_COMP_COST="1000000000" OMPCLUSTER_BCAST_STRATEGY=mpibcast \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc
// RUN: env OMPCLUSTER_HEFT_COMP_COST="1000000000" OMPCLUSTER_PROFILE=profile \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc
// RUN: env OMPCLUSTER_HEFT_COMP_COST="1000000000" OMPCLUSTER_BCAST_STRATEGY=dynamicbcast \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc
// RUN: env OMPCLUSTER_HEFT_COMP_COST="1000000000" OMPCLUSTER_BCAST_STRATEGY=dynamicbcast OMPCLUSTER_ENABLE_PACKING=1 \
// RUN:   %libomptarget-run-x86_64-pc-linux-gnu-ompc | %fcheck-x86_64-pc-linux-gnu-ompc

// REQUIRES: x86_64-pc-linux-gnu-ompc

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main(void) {
  int test_array[] = {0, 1, 2, 3};
  long test_scalar = 128;

#pragma omp parallel
#pragma omp single nowait
  {
#pragma omp target enter data map(to                                           \
                                  : test_array[:], test_scalar) nowait         \
depend(inout                                                                   \
       : *test_array, test_scalar)

    for (int idx = 0; idx < 3; idx++) {
#pragma omp target nowait depend(in : *test_array, test_scalar)
      {
        // CHECK: test_array[0]=0
        printf("test_array[0]=%d\n", test_array[0]);

        // CHECK: test_array[1]=1
        printf("test_array[1]=%d\n", test_array[1]);

        // CHECK: test_array[2]=2
        printf("test_array[2]=%d\n", test_array[2]);

        // CHECK: test_array[3]=3
        printf("test_array[3]=%d\n", test_array[3]);

        // CHECK: test_scalar=128
        printf("test_scalar=%ld\n", test_scalar);
      }
    }

#pragma omp target exit data map(from                                          \
                                 : test_array[:], test_scalar) nowait          \
depend(inout                                                                   \
       : *test_array, test_scalar)
  }

  return 0;
}
