// RUN: %libomptarget-compilexx-run-and-check-x86_64-pc-linux-gnu-ompc
// REQUIRES: x86_64-pc-linux-gnu-ompc

#include <cstdint>
#include <cstdio>
#include <vector>

#include <omp.h>

int main() {

  constexpr char local_size = 10;
  std::vector<char> local_data(local_size, 0);

  for (char i = 0; i < local_size; ++i) {
    local_data[i] = i;
  }

  int host_dev = omp_get_initial_device();
  int rank_1_dev = 0;
  int rank_2_dev = 1;

  // CHECK: Number of devices 2
  printf("Number of devices %d\n", omp_get_num_devices());

  void *rank_1_addr = omp_target_alloc(local_size, rank_1_dev);
  void *rank_2_addr = omp_target_alloc(local_size, rank_2_dev);

  omp_target_memcpy(rank_1_addr, local_data.data(), local_size, 0, 0,
                    rank_1_dev, host_dev);
  omp_target_memcpy(rank_2_addr, rank_1_addr, local_size, 0, 0, rank_2_dev,
                    rank_1_dev);

  std::vector<char> remote_data(local_size, 0);
  omp_target_memcpy(remote_data.data(), rank_2_addr, local_size, 0, 0, host_dev,
                    rank_2_dev);

  omp_target_free(rank_1_addr, rank_1_dev);
  omp_target_free(rank_2_addr, rank_2_dev);

  for (char i = 0; i < local_size; ++i) {
    // CHECK: Next local and remote values are equal
    printf("Next local and remote values %s equal\n",
           local_data[i] == remote_data[i] ? "are" : "are not");
  }

  return 0;
}
