// RUN: %libomptarget-compilexx-run-and-check-generic
// XFAIL: nvptx64-nvidia-cuda
// XFAIL: nvptx64-nvidia-cuda-LTO

//This test should fail on any configuration with less than 2 devices
#include <omp.h>
#include <stdio.h>

#define N 1024
#define L 10

int A[N], B[N];
int main() {
  for (int i = 0; i < N; i++){
    A[i] = i;
    B[i] = i;
  }

  #pragma omp parallel
      {
  #pragma omp single
          {
              for (int i = 0; i < L; i++) {
  #pragma omp target nowait depend(inout:*A) map(tofrom:A[0:N]) device(1) 
                  {
                      for (int j = 0; j < N; j++) {
                          A[j]++;
                      }
                  }
  #pragma omp target nowait depend(inout:*B) map(tofrom:B[0:N]) device(2) 
                  {
                      for (int j = 0; j < N; j++) {
                          B[j]++;
                      }
                  }
              }
          }
      }
  int Sum = 0;
  for (int i = 0; i < N; i++) {
      Sum += A[i] + B[i];
      }
  // Sum is 2 * N * L + N * ( N - 1 )
  // CHECK: Sum = 1068032.
  printf("Sum = %d.\n", Sum);

  return 0;
}
